/*

  gmf2vtk is a tool to do conversions between Gamma Mesh Format (see libMeshb) files and VTK files (ParaView)

  Written by Matthieu Maunoury, INRIA, 2020
  email: Matthieu.Maunoury@inria.fr

*/

#ifndef VIZVTK_H
#define VIZVTK_H

#include "vtkCell.h"
#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkCellType.h"
#include "vtkDataArray.h"
#include "vtkDataSet.h"
#include "vtkDoubleArray.h"
#include "vtkInformation.h"
#include "vtkInformationStringKey.h"
#include "vtkInformationVector.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPointSet.h"
#include "vtkUnsignedCharArray.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridWriter.h"
#include <vtkCellTypes.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLUnstructuredGridWriter.h>

extern "C" {
#include "msh.h"
}

int viz_WriteVTK(VizObject* iObj, const char* OutMshNam, bool ascii, int ierrsol);
int viz_ReadVTK(VizObject* iObj, const char* OutMshNam, bool* solexist);

#endif