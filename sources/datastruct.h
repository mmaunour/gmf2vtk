#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#include <msh.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct viz_solution {
  int     vizAlloc;
  int     Dim;
  int     NbrSol;
  int     SolTyp;
  char    Nam[2048];
  double* Sol;
  int     Ite;
  int     Deg;
  int     NbrNod;
  double  Tim;

  struct viz_solution* nxt;

} solution;

struct viz_object {

  int1 Dim;

  int1 NbrVerMax, NbrCrnMax, NbrEdgMax, NbrTriMax, NbrQuaMax, NbrTetMax, NbrPyrMax, NbrPriMax, NbrHexMax;
  int1 NbrVer, NbrCrn, NbrEdg, NbrTri, NbrQua, NbrTet, NbrPyr, NbrPri, NbrHex;

  int1 NbrP2EdgMax, NbrP2TriMax, NbrQ2QuaMax, NbrP2TetMax, NbrQ2HexMax, NbrP2PyrMax, NbrP2PriMax;
  int1 NbrP2Edg, NbrP2Tri, NbrQ2Qua, NbrP2Tet, NbrQ2Hex, NbrP2Pyr, NbrP2Pri;

  int1 NbrP3EdgMax, NbrP3TriMax, NbrQ3QuaMax, NbrP3TetMax, NbrQ3HexMax, NbrP3PyrMax, NbrP3PriMax;
  int1 NbrP3Edg, NbrP3Tri, NbrQ3Qua, NbrP3Tet, NbrQ3Hex, NbrP3Pyr, NbrP3Pri;

  int1 NbrP4EdgMax, NbrP4TriMax, NbrQ4QuaMax, NbrP4TetMax, NbrQ4HexMax, NbrP4PyrMax, NbrP4PriMax;
  int1 NbrP4Edg, NbrP4Tri, NbrQ4Qua, NbrP4Tet, NbrQ4Hex, NbrP4Pyr, NbrP4Pri;

  // int1 NbrHoTriMax, NbrHoQuaMax, NbrHoTetMax, NbrHoHexMax;
  // int1 NbrHoTri   , NbrHoQua   , NbrHoTet   , NbrHoHex   ;

  int1 NbrNorMax, NbrNor;

  /* number of points/control points per High Order entities */
  // int1 NbrPoiHoTri, NbrPoiHoQua, NbrPoiHoTet, NbrPoiHoHex;

  float3*   Crd_f;
  float3*   Nor_f;
  double3*  Crd;
  int1*     CrdRef;
  solution* CrdSol;

  int1*     Crn;
  int1*     CrnRef;
  solution* CrnSol;

  int2*     Edg;
  int1*     EdgRef;
  solution* EdgSol;

  int3*     Tri;
  int1*     TriRef;
  solution* TriSol;

  int4*     Qua;
  int1*     QuaRef;
  solution* QuaSol;

  int4*     Tet;
  int1*     TetRef;
  solution* TetSol;

  int5*     Pyr;
  int1*     PyrRef;
  solution* PyrSol;

  int6*     Pri;
  int1*     PriRef;
  solution* PriSol;

  int8*     Hex;
  int1*     HexRef;
  solution* HexSol;

  /* intialization + reading mesh */
  int3*     P2Edg;
  int1*     P2EdgRef;
  solution* P2EdgSol;

  int4*     P3Edg;
  int1*     P3EdgRef;
  solution* P3EdgSol;

  int5*     P4Edg;
  int1*     P4EdgRef;
  solution* P4EdgSol;

  int6*     P2Tri;
  int1*     P2TriRef;
  solution* P2TriSol;

  int10*    P3Tri;
  int1*     P3TriRef;
  solution* P3TriSol;

  int15*    P4Tri;
  int1*     P4TriRef;
  solution* P4TriSol;

  int9*     Q2Qua;
  int1*     Q2QuaRef;
  solution* Q2QuaSol;

  int16*    Q3Qua;
  int1*     Q3QuaRef;
  solution* Q3QuaSol;

  int25*    Q4Qua;
  int1*     Q4QuaRef;
  solution* Q4QuaSol;

  int10*    P2Tet;
  int1*     P2TetRef;
  solution* P2TetSol;

  int20*    P3Tet;
  int1*     P3TetRef;
  solution* P3TetSol;

  int35*    P4Tet;
  int1*     P4TetRef;
  solution* P4TetSol;

  int27*    Q2Hex;
  int1*     Q2HexRef;
  solution* Q2HexSol;

  int64*    Q3Hex;
  int1*     Q3HexRef;
  solution* Q3HexSol;

  int125*   Q4Hex;
  int1*     Q4HexRef;
  solution* Q4HexSol;

  int14*    P2Pyr;
  int1*     P2PyrRef;
  solution* P2PyrSol;

  int30*    P3Pyr;
  int1*     P3PyrRef;
  solution* P3PyrSol;

  int55*    P4Pyr;
  int1*     P4PyrRef;
  solution* P4PyrSol;

  int18*    P2Pri;
  int1*     P2PriRef;
  solution* P2PriSol;

  int40*    P3Pri;
  int1*     P3PriRef;
  solution* P3PriSol;

  int75*    P4Pri;
  int1*     P4PriRef;
  solution* P4PriSol;

  // int1    **HoTri;
  // int1     *HoTriRef;
  // solution *HoTriSol;
  //
  // int1    **HoQua;
  // int1     *HoQuaRef;
  // solution *HoQuaSol;
  //
  // int1     **HoTet;
  // int1      *HoTetRef;
  // solution  *HoTetSol;
  //
  // int1    **HoHex;
  // int1     *HoHexRef;
  // solution *HoHexSol;

  /* geometry */
  int1  NbrRdgMax, NbrCorMax;
  int1  NbrRdg, NbrCor;
  int1* Rdg;
  int1* Cor;

  /* Entity-to ref Hash table */
  int MaxRefTyp[256];
  // long long HshRef;
  // long long HshTyp;
};

/* ---------------------------------------------- */
/*  Keyword types entities handled by the library */
/* ---------------------------------------------- */

void viz_freesol(solution* sol)
{
  // int iSol;
  if (!sol)
    return;
  solution* nxt = NULL;
  while (sol != NULL) {
    if (sol->vizAlloc && sol->Sol) {
      free(sol->Sol);
      sol->Sol = NULL;
    }
    free(sol->Nam);
    sol->Nam[0] = '\0';
    nxt         = sol->nxt;
    free(sol);
    sol = NULL;
    sol = nxt;
  }
  return;
}

solution* viz_allocsol()
{
  solution* sol = (solution*)malloc(sizeof(solution));
  if (!sol)
    return NULL;
  sol->Dim      = 0;
  sol->vizAlloc = 0;
  sol->NbrSol   = 0;
  sol->SolTyp   = 0;
  sol->Deg      = 0;
  sol->NbrNod   = 0;
  sol->Nam[0]   = '\0';
  sol->Sol      = NULL;
  sol->Tim      = 0.0;
  sol->Ite      = 0;
  sol->nxt      = NULL;
  return sol;
}

solution* viz_addsol(solution* sol)
{
  if (sol == NULL)
    return viz_allocsol();
  solution* news = viz_allocsol();
  /* create the link with previous solutions */
  sol->nxt = news;

  return news;
}

static int viz_IniObject(VizObject* Obj)
{
  // int ityp;

  if (!Obj)
    return 1;

  Obj->Dim = 0;

  Obj->NbrNorMax   = 0;
  Obj->NbrVerMax   = 0;
  Obj->NbrCrnMax   = 0;
  Obj->NbrEdgMax   = 0;
  Obj->NbrTriMax   = 0;
  Obj->NbrQuaMax   = 0;
  Obj->NbrTetMax   = 0;
  Obj->NbrPyrMax   = 0;
  Obj->NbrPriMax   = 0;
  Obj->NbrHexMax   = 0;
  Obj->NbrP2EdgMax = 0;
  Obj->NbrP3EdgMax = 0;
  Obj->NbrP4EdgMax = 0;
  Obj->NbrP2TriMax = 0;
  Obj->NbrP3TriMax = 0;
  Obj->NbrP4TriMax = 0;
  Obj->NbrQ2QuaMax = 0;
  Obj->NbrQ3QuaMax = 0;
  Obj->NbrQ4QuaMax = 0;
  Obj->NbrP2TetMax = 0;
  Obj->NbrP3TetMax = 0;
  Obj->NbrP4TetMax = 0;
  Obj->NbrQ2HexMax = 0;
  Obj->NbrQ3HexMax = 0;
  Obj->NbrQ4HexMax = 0;
  Obj->NbrP2PyrMax = 0;
  Obj->NbrP3PyrMax = 0;
  Obj->NbrP4PyrMax = 0;
  Obj->NbrP2PriMax = 0;
  Obj->NbrP3PriMax = 0;
  Obj->NbrP4PriMax = 0;

  Obj->NbrNor   = 0;
  Obj->NbrVer   = 0;
  Obj->NbrCrn   = 0;
  Obj->NbrEdg   = 0;
  Obj->NbrTri   = 0;
  Obj->NbrQua   = 0;
  Obj->NbrTet   = 0;
  Obj->NbrPyr   = 0;
  Obj->NbrPri   = 0;
  Obj->NbrHex   = 0;
  Obj->NbrP2Edg = 0;
  Obj->NbrP3Edg = 0;
  Obj->NbrP4Edg = 0;
  Obj->NbrP2Tri = 0;
  Obj->NbrP3Tri = 0;
  Obj->NbrP4Tri = 0;
  Obj->NbrQ2Qua = 0;
  Obj->NbrQ3Qua = 0;
  Obj->NbrQ4Qua = 0;
  Obj->NbrP2Tet = 0;
  Obj->NbrP3Tet = 0;
  Obj->NbrP4Tet = 0;
  Obj->NbrQ2Hex = 0;
  Obj->NbrQ3Hex = 0;
  Obj->NbrQ4Hex = 0;
  Obj->NbrP2Pyr = 0;
  Obj->NbrP3Pyr = 0;
  Obj->NbrP4Pyr = 0;
  Obj->NbrP2Pri = 0;
  Obj->NbrP3Pri = 0;
  Obj->NbrP4Pri = 0;

  Obj->Crd_f    = NULL;
  Obj->Nor_f    = NULL;
  Obj->Crd      = NULL;
  Obj->CrdRef   = NULL;
  Obj->CrdSol   = NULL;
  Obj->Crn      = NULL;
  Obj->CrnRef   = NULL;
  Obj->Edg      = NULL;
  Obj->EdgRef   = NULL;
  Obj->EdgSol   = NULL;
  Obj->Tri      = NULL;
  Obj->TriRef   = NULL;
  Obj->TriSol   = NULL;
  Obj->Qua      = NULL;
  Obj->QuaRef   = NULL;
  Obj->QuaSol   = NULL;
  Obj->Tet      = NULL;
  Obj->TetRef   = NULL;
  Obj->TetSol   = NULL;
  Obj->Pyr      = NULL;
  Obj->PyrRef   = NULL;
  Obj->PyrSol   = NULL;
  Obj->Pri      = NULL;
  Obj->PriRef   = NULL;
  Obj->PriSol   = NULL;
  Obj->Hex      = NULL;
  Obj->HexRef   = NULL;
  Obj->HexSol   = NULL;
  Obj->P2Edg    = NULL;
  Obj->P2EdgRef = NULL;
  Obj->P2EdgSol = NULL;
  Obj->P3Edg    = NULL;
  Obj->P3EdgRef = NULL;
  Obj->P3EdgSol = NULL;
  Obj->P4Edg    = NULL;
  Obj->P4EdgRef = NULL;
  Obj->P4EdgSol = NULL;
  Obj->P2Tri    = NULL;
  Obj->P2TriRef = NULL;
  Obj->P2TriSol = NULL;
  Obj->P3Tri    = NULL;
  Obj->P3TriRef = NULL;
  Obj->P3TriSol = NULL;
  Obj->P4Tri    = NULL;
  Obj->P4TriRef = NULL;
  Obj->P4TriSol = NULL;
  Obj->Q2Qua    = NULL;
  Obj->Q2QuaRef = NULL;
  Obj->Q2QuaSol = NULL;
  Obj->Q3Qua    = NULL;
  Obj->Q3QuaRef = NULL;
  Obj->Q3QuaSol = NULL;
  Obj->Q4Qua    = NULL;
  Obj->Q4QuaRef = NULL;
  Obj->Q4QuaSol = NULL;
  Obj->P2Tet    = NULL;
  Obj->P2TetRef = NULL;
  Obj->P2TetSol = NULL;
  Obj->P3Tet    = NULL;
  Obj->P3TetRef = NULL;
  Obj->P3TetSol = NULL;
  Obj->P4Tet    = NULL;
  Obj->P4TetRef = NULL;
  Obj->P4TetSol = NULL;
  Obj->Q2Hex    = NULL;
  Obj->Q2HexRef = NULL;
  Obj->Q2HexSol = NULL;
  Obj->Q3Hex    = NULL;
  Obj->Q3HexRef = NULL;
  Obj->Q3HexSol = NULL;
  Obj->Q4Hex    = NULL;
  Obj->Q4HexRef = NULL;
  Obj->Q4HexSol = NULL;
  Obj->P2Pyr    = NULL;
  Obj->P2PyrRef = NULL;
  Obj->P2PyrSol = NULL;
  Obj->P3Pyr    = NULL;
  Obj->P3PyrRef = NULL;
  Obj->P3PyrSol = NULL;
  Obj->P4Pyr    = NULL;
  Obj->P4PyrRef = NULL;
  Obj->P4PyrSol = NULL;
  Obj->P2Pri    = NULL;
  Obj->P2PriRef = NULL;
  Obj->P2PriSol = NULL;
  Obj->P3Pri    = NULL;
  Obj->P3PriRef = NULL;
  Obj->P3PriSol = NULL;
  Obj->P4Pri    = NULL;
  Obj->P4PriRef = NULL;
  Obj->P4PriSol = NULL;

  return 0;
}

static int viz_FreeObject(VizObject* Obj)
{
  if (!Obj)
    return 1;

  Obj->Dim = 0;

  if (Obj->Crd_f) {
    free(Obj->Crd_f);
    Obj->Crd_f = NULL;
  }
  if (Obj->Nor_f) {
    free(Obj->Nor_f);
    Obj->Nor_f = NULL;
  }
  if (Obj->Crd) {
    free(Obj->Crd);
    Obj->Crd = NULL;
  }
  if (Obj->CrdRef) {
    free(Obj->CrdRef);
    Obj->CrdRef = NULL;
  }
  if (Obj->CrdSol) {
    viz_freesol(Obj->CrdSol);
    Obj->CrdSol = NULL;
  }
  if (Obj->Crn) {
    free(Obj->Crn);
    Obj->Crn = NULL;
  }
  if (Obj->CrnRef) {
    free(Obj->CrnRef);
    Obj->CrnRef = NULL;
  }
  if (Obj->Edg) {
    free(Obj->Edg);
    Obj->Edg = NULL;
  }
  if (Obj->EdgRef) {
    free(Obj->EdgRef);
    Obj->EdgRef = NULL;
  }
  if (Obj->EdgSol) {
    viz_freesol(Obj->EdgSol);
    Obj->EdgSol = NULL;
  }
  if (Obj->Tri) {
    free(Obj->Tri);
    Obj->Tri = NULL;
  }
  if (Obj->TriRef) {
    free(Obj->TriRef);
    Obj->TriRef = NULL;
  }
  if (Obj->TriSol) {
    viz_freesol(Obj->TriSol);
    Obj->TriSol = NULL;
  }
  if (Obj->Qua) {
    free(Obj->Qua);
    Obj->Qua = NULL;
  }
  if (Obj->QuaRef) {
    free(Obj->QuaRef);
    Obj->QuaRef = NULL;
  }
  if (Obj->QuaSol) {
    viz_freesol(Obj->QuaSol);
    Obj->QuaSol = NULL;
  }
  if (Obj->Tet) {
    free(Obj->Tet);
    Obj->Tet = NULL;
  }
  if (Obj->TetRef) {
    free(Obj->TetRef);
    Obj->TetRef = NULL;
  }
  if (Obj->TetSol) {
    viz_freesol(Obj->TetSol);
    Obj->TetSol = NULL;
  }
  if (Obj->Pyr) {
    free(Obj->Pyr);
    Obj->Pyr = NULL;
  }
  if (Obj->PyrRef) {
    free(Obj->PyrRef);
    Obj->PyrRef = NULL;
  }
  if (Obj->PyrSol) {
    viz_freesol(Obj->PyrSol);
    Obj->PyrSol = NULL;
  }
  if (Obj->Pri) {
    free(Obj->Pri);
    Obj->Pri = NULL;
  }
  if (Obj->PriRef) {
    free(Obj->PriRef);
    Obj->PriRef = NULL;
  }
  if (Obj->PriSol) {
    viz_freesol(Obj->PriSol);
    Obj->PriSol = NULL;
  }
  if (Obj->Hex) {
    free(Obj->Hex);
    Obj->Hex = NULL;
  }
  if (Obj->HexRef) {
    free(Obj->HexRef);
    Obj->HexRef = NULL;
  }
  if (Obj->HexSol) {
    viz_freesol(Obj->HexSol);
    Obj->HexSol = NULL;
  }
  if (Obj->P2Edg) {
    free(Obj->P2Edg);
    Obj->P2Edg = NULL;
  }
  if (Obj->P2EdgRef) {
    free(Obj->P2EdgRef);
    Obj->P2EdgRef = NULL;
  }
  if (Obj->P2EdgSol) {
    viz_freesol(Obj->P2EdgSol);
    Obj->P2EdgSol = NULL;
  }
  if (Obj->P3Edg) {
    free(Obj->P3Edg);
    Obj->P3Edg = NULL;
  }
  if (Obj->P3EdgRef) {
    free(Obj->P3EdgRef);
    Obj->P3EdgRef = NULL;
  }
  if (Obj->P3EdgSol) {
    viz_freesol(Obj->P3EdgSol);
    Obj->P3EdgSol = NULL;
  }
  if (Obj->P4Edg) {
    free(Obj->P4Edg);
    Obj->P4Edg = NULL;
  }
  if (Obj->P4EdgRef) {
    free(Obj->P4EdgRef);
    Obj->P4EdgRef = NULL;
  }
  if (Obj->P4EdgSol) {
    viz_freesol(Obj->P4EdgSol);
    Obj->P4EdgSol = NULL;
  }
  if (Obj->P2Tri) {
    free(Obj->P2Tri);
    Obj->P2Tri = NULL;
  }
  if (Obj->P2TriRef) {
    free(Obj->P2TriRef);
    Obj->P2TriRef = NULL;
  }
  if (Obj->P2TriSol) {
    viz_freesol(Obj->P2TriSol);
    Obj->P2TriSol = NULL;
  }
  if (Obj->P3Tri) {
    free(Obj->P3Tri);
    Obj->P3Tri = NULL;
  }
  if (Obj->P3TriRef) {
    free(Obj->P3TriRef);
    Obj->P3TriRef = NULL;
  }
  if (Obj->P3TriSol) {
    viz_freesol(Obj->P3TriSol);
    Obj->P3TriSol = NULL;
  }
  if (Obj->P4Tri) {
    free(Obj->P4Tri);
    Obj->P4Tri = NULL;
  }
  if (Obj->P4TriRef) {
    free(Obj->P4TriRef);
    Obj->P4TriRef = NULL;
  }
  if (Obj->P4TriSol) {
    viz_freesol(Obj->P4TriSol);
    Obj->P4TriSol = NULL;
  }
  if (Obj->Q2Qua) {
    free(Obj->Q2Qua);
    Obj->Q2Qua = NULL;
  }
  if (Obj->Q2QuaRef) {
    free(Obj->Q2QuaRef);
    Obj->Q2QuaRef = NULL;
  }
  if (Obj->Q2QuaSol) {
    viz_freesol(Obj->Q2QuaSol);
    Obj->Q2QuaSol = NULL;
  }
  if (Obj->Q3Qua) {
    free(Obj->Q3Qua);
    Obj->Q3Qua = NULL;
  }
  if (Obj->Q3QuaRef) {
    free(Obj->Q3QuaRef);
    Obj->Q3QuaRef = NULL;
  }
  if (Obj->Q3QuaSol) {
    viz_freesol(Obj->Q3QuaSol);
    Obj->Q3QuaSol = NULL;
  }
  if (Obj->Q4Qua) {
    free(Obj->Q4Qua);
    Obj->Q4Qua = NULL;
  }
  if (Obj->Q4QuaRef) {
    free(Obj->Q4QuaRef);
    Obj->Q4QuaRef = NULL;
  }
  if (Obj->Q4QuaSol) {
    viz_freesol(Obj->Q4QuaSol);
    Obj->Q4QuaSol = NULL;
  }
  if (Obj->P2Tet) {
    free(Obj->P2Tet);
    Obj->P2Tet = NULL;
  }
  if (Obj->P2TetRef) {
    free(Obj->P2TetRef);
    Obj->P2TetRef = NULL;
  }
  if (Obj->P2TetSol) {
    viz_freesol(Obj->P2TetSol);
    Obj->P2TetSol = NULL;
  }
  if (Obj->P3Tet) {
    free(Obj->P3Tet);
    Obj->P3Tet = NULL;
  }
  if (Obj->P3TetRef) {
    free(Obj->P3TetRef);
    Obj->P3TetRef = NULL;
  }
  if (Obj->P3TetSol) {
    viz_freesol(Obj->P3TetSol);
    Obj->P3TetSol = NULL;
  }
  if (Obj->P4Tet) {
    free(Obj->P4Tet);
    Obj->P4Tet = NULL;
  }
  if (Obj->P4TetRef) {
    free(Obj->P4TetRef);
    Obj->P4TetRef = NULL;
  }
  if (Obj->P4TetSol) {
    viz_freesol(Obj->P4TetSol);
    Obj->P4TetSol = NULL;
  }
  if (Obj->Q2Hex) {
    free(Obj->Q2Hex);
    Obj->Q2Hex = NULL;
  }
  if (Obj->Q2HexRef) {
    free(Obj->Q2HexRef);
    Obj->Q2HexRef = NULL;
  }
  if (Obj->Q2HexSol) {
    viz_freesol(Obj->Q2HexSol);
    Obj->Q2HexSol = NULL;
  }
  if (Obj->Q3Hex) {
    free(Obj->Q3Hex);
    Obj->Q3Hex = NULL;
  }
  if (Obj->Q3HexRef) {
    free(Obj->Q3HexRef);
    Obj->Q3HexRef = NULL;
  }
  if (Obj->Q3HexSol) {
    viz_freesol(Obj->Q3HexSol);
    Obj->Q3HexSol = NULL;
  }
  if (Obj->Q4Hex) {
    free(Obj->Q4Hex);
    Obj->Q4Hex = NULL;
  }
  if (Obj->Q4HexRef) {
    free(Obj->Q4HexRef);
    Obj->Q4HexRef = NULL;
  }
  if (Obj->Q4HexSol) {
    viz_freesol(Obj->Q4HexSol);
    Obj->Q4HexSol = NULL;
  }
  if (Obj->P2Pyr) {
    free(Obj->P2Pyr);
    Obj->P2Pyr = NULL;
  }
  if (Obj->P2PyrRef) {
    free(Obj->P2PyrRef);
    Obj->P2PyrRef = NULL;
  }
  if (Obj->P2PyrSol) {
    viz_freesol(Obj->P2PyrSol);
    Obj->P2PyrSol = NULL;
  }
  if (Obj->P3Pyr) {
    free(Obj->P3Pyr);
    Obj->P3Pyr = NULL;
  }
  if (Obj->P3PyrRef) {
    free(Obj->P3PyrRef);
    Obj->P3PyrRef = NULL;
  }
  if (Obj->P3PyrSol) {
    viz_freesol(Obj->P3PyrSol);
    Obj->P3PyrSol = NULL;
  }
  if (Obj->P4Pyr) {
    free(Obj->P4Pyr);
    Obj->P4Pyr = NULL;
  }
  if (Obj->P4PyrRef) {
    free(Obj->P4PyrRef);
    Obj->P4PyrRef = NULL;
  }
  if (Obj->P4PyrSol) {
    viz_freesol(Obj->P4PyrSol);
    Obj->P4PyrSol = NULL;
  }
  if (Obj->P2Pri) {
    free(Obj->P2Pri);
    Obj->P2Pri = NULL;
  }
  if (Obj->P2PriRef) {
    free(Obj->P2PriRef);
    Obj->P2PriRef = NULL;
  }
  if (Obj->P2PriSol) {
    viz_freesol(Obj->P2PriSol);
    Obj->P2PriSol = NULL;
  }
  if (Obj->P3Pri) {
    free(Obj->P3Pri);
    Obj->P3Pri = NULL;
  }
  if (Obj->P3PriRef) {
    free(Obj->P3PriRef);
    Obj->P3PriRef = NULL;
  }
  if (Obj->P3PriSol) {
    viz_freesol(Obj->P3PriSol);
    Obj->P3PriSol = NULL;
  }
  if (Obj->P4Pri) {
    free(Obj->P4Pri);
    Obj->P4Pri = NULL;
  }
  if (Obj->P4PriRef) {
    free(Obj->P4PriRef);
    Obj->P4PriRef = NULL;
  }
  if (Obj->P4PriSol) {
    viz_freesol(Obj->P4PriSol);
    Obj->P4PriSol = NULL;
  }

  /* generic high-order */
  // if ( Obj->HoTri    ) { free(Obj->HoTri   );         Obj->HoTri    = NULL; }
  // if ( Obj->HoTriRef ) { free(Obj->HoTriRef);         Obj->HoTriRef = NULL; }
  // if ( Obj->HoTriSol ) { viz_freesol(Obj->HoTriSol);  Obj->HoTriSol = NULL; }
  // if ( Obj->HoQua    ) { free(Obj->HoQua   );         Obj->HoQua    = NULL; }
  // if ( Obj->HoQuaRef ) { free(Obj->HoQuaRef);         Obj->HoQuaRef = NULL; }
  // if ( Obj->HoQuaSol ) { viz_freesol(Obj->HoQuaSol);  Obj->HoQuaSol = NULL; }
  // if ( Obj->HoTet    ) { free(Obj->HoTet   );         Obj->HoTet    = NULL; }
  // if ( Obj->HoTetRef ) { free(Obj->HoTetRef);         Obj->HoTetRef = NULL; }
  // if ( Obj->HoTetSol ) { viz_freesol(Obj->HoTetSol);  Obj->HoTetSol = NULL; }
  // if ( Obj->HoHex    ) { free(Obj->HoHex   );         Obj->HoHex    = NULL; }
  // if ( Obj->HoHexRef ) { free(Obj->HoHexRef);         Obj->HoHexRef = NULL; }
  // if ( Obj->HoHexSol ) { viz_freesol(Obj->HoHexSol);  Obj->HoHexSol = NULL; }

  /* geometry */
  if (Obj->Rdg) {
    free(Obj->Rdg);
    Obj->Rdg = NULL;
  }
  if (Obj->Cor) {
    free(Obj->Cor);
    Obj->Cor = NULL;
  }

  return 0;
}

#endif
