/*

  gmf2vtk is a tool to do conversions between Gamma Mesh Format (see libMeshb) files and VTK files (ParaView)

  Written by Matthieu Maunoury, INRIA, 2020
  email: Matthieu.Maunoury@inria.fr

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
using namespace std;

extern "C" {
#include "msh.h"
}
#include "libmeshb7.h"
#include "viz_vtk.h"

int main(int argc, char** argv)
{
  printf(" \n");
  printf("- GMF2VTK, %s, Inria, Gamma Project Team\n \n", __DATE__);

  VizObject*   iObj = NULL;
  VizIntStatus status;
  int          ierro, ierrsol;
  char         OutMshNam[1024], OutSolNam[1024], bufNam[1024], noMshNam[1024];
  int          iin = 0, isol = 0, iout = 0;
  bool         solexist = false, ascii = false;

  double t0 = GetWallClock();

  if (argc < 3) {
    cout << "  ERROR: missing arguments" << endl;
    goto errormessage;

  errormessage: //-- error message
    printf("\n");
    cout << "  The format is: gmf2vtk -in xxx.mesh[b]/.vtu (-sol xxx.sol[b] -out xxx)" << endl;
    cout << "  where -in    is required to define the input mesh files" << endl;
    cout << "        -sol   is optional to define the input solution files" << endl;
    cout << "        -out   is optional to define the output mesh/sol files" << endl;
    cout << "        -ascii is optional to define ascii output (binary by default otherwise)" << endl;
    return 0;
  }

  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i], "-in") == 0)
      iin = i + 1;
    if (strcmp(argv[i], "-sol") == 0)
      isol = i + 1;
    if (strcmp(argv[i], "-out") == 0)
      iout = i + 1;
    if (strcmp(argv[i], "-ascii") == 0)
      ascii = true;
  }

  if (iin == 0) {
    cout << "  ERROR: -in is missing" << endl;
    return 0;
  }
  if (isol == 0)
    isol = iin; //-- no solution file given

  ierro = viz_NewInterface(&iObj);
  if (ierro != 0) {
    printf(" viz_NewInterface  status %d \n", ierro);
  }

  if (strstr(argv[iin], ".mesh") != NULL || strstr(argv[iin], ".meshb") != NULL) {
    //-- open a mesh[b] file and write with VTK library

    double t1 = GetWallClock();
    ierro     = viz_OpenMesh(iObj, argv[iin], &status);
    double t2 = GetWallClock();
    printf(" cpu time to open the mesh %lg (s) \n", t2 - t1);

    if (ierro != VIZINT_SUCCESS)
      printf(" viz_OpenMesh failed error %3d : %s\n", ierro, status.ErrMsg);
    else
      printf("  %%%% %s OPENED\n", status.InfoMsg);

    viz_PrintMeshInfo(iObj, &status);

    ierrsol = viz_OpenSolution(iObj, argv[isol], &status);

    if (ierro != VIZINT_SUCCESS)
      printf("  viz_OpenSolution failed error %3d : %s", ierro, status.ErrMsg);
    else
      printf("  %%%% %s OPENED\n", status.InfoMsg);

    viz_PrintSolutionInfo(iObj, &status);

    if (iout == 0) {
      sprintf(noMshNam, "%s", argv[iin]);
      if (strstr(argv[iin], ".mesh") != NULL)
        noMshNam[strstr(argv[iin], ".mesh") - argv[iin]] = '\0'; // check if it has the extension .mesh[b] and remove it if it is the case
      sprintf(bufNam, "%s", noMshNam);
      strcat(bufNam, ".vtu");

      sprintf(OutMshNam, "%s", bufNam);
    }
    else {
      if (strstr(argv[iout], ".vtu") != NULL)
        sprintf(OutMshNam, "%s", argv[iout]);
      else {
        sprintf(bufNam, "%s", argv[iout]);
        strcat(bufNam, ".vtu");
        sprintf(OutMshNam, "%s", bufNam);
      }
    }

    viz_WriteVTK(iObj, OutMshNam, ascii, ierrsol);
  }
  else if (strstr(argv[iin], ".vtu") != NULL || strstr(argv[iin], ".vtk") != NULL) {
    //-- open a mesh with VTK library and write with it in meshb
    viz_ReadVTK(iObj, argv[iin], &solexist);

    if (iout == 0) {
      sprintf(noMshNam, "%s", argv[iin]);
      if (strstr(argv[iin], ".vtk") != NULL)
        noMshNam[strstr(argv[iin], ".vtk") - argv[iin]] = '\0'; // check if it has the extension vtk and remove it if it is the case
      if (strstr(argv[iin], ".vtu") != NULL)
        noMshNam[strstr(argv[iin], ".vtu") - argv[iin]] = '\0'; // check if it has the extension vtu and remove it if it is the case

      sprintf(OutMshNam, "%s", noMshNam);
      sprintf(OutSolNam, "%s", noMshNam);

      if (ascii) {
        strcat(OutMshNam, ".mesh");
        strcat(OutSolNam, ".sol");
      }
      else {
        strcat(OutMshNam, ".meshb");
        strcat(OutSolNam, ".solb");
      }
    }
    else {
      if (strstr(argv[iout], ".mesh") != NULL)
        sprintf(OutMshNam, "%s", argv[iout]);
      else {
        sprintf(bufNam, "%s", argv[iout]);
        if (ascii)
          strcat(bufNam, ".mesh");
        else
          strcat(bufNam, ".meshb");
        sprintf(OutMshNam, "%s", bufNam);
      }

      sprintf(bufNam, "%s", argv[iout]);
      if (strstr(argv[iout], ".mesh") != NULL)
        bufNam[strstr(argv[iout], ".mesh") - argv[iout]] = '\0'; // check if it has the extension .mesh[b] and remove it if it is the case
      if (strstr(argv[iout], ".meshb") != NULL)
        bufNam[strstr(argv[iout], ".meshb") - argv[iout]] = '\0'; // check if it has the extension .mesh[b] and remove it if it is the case

      if (ascii)
        strcat(bufNam, ".sol");
      else
        strcat(bufNam, ".solb");
      sprintf(OutSolNam, "%s", bufNam);
    }
    ierro = viz_WriteMesh(iObj, OutMshNam, &status);
    if (ierro != VIZINT_SUCCESS)
      printf(" viz_WriteMesh failed error %3d : %s\n", ierro, status.ErrMsg);
    else {
      printf("  %%%% Mesh written in %s\n", status.InfoMsg);
      viz_PrintMeshInfo(iObj, &status);
    }

    if (solexist) {
      ierro = viz_WriteSolution(iObj, OutSolNam, -1, &status);
      if (ierro != VIZINT_SUCCESS)
        printf(" viz_WriteSolution failed error %3d : %s\n", ierro, status.ErrMsg);
      else {
        printf("  %%%% Solution written in %s\n", status.InfoMsg);
      }
      viz_PrintSolutionInfo(iObj, &status);
    }
  }
  else {
    cout << "  Unkown input mesh format, should be mesh, meshb, vtk or vtu " << endl;

    cout << "\n  Thank you for using GMF2VTK" << endl;
    return 0;
  }

  double t2 = GetWallClock();
  printf("  Total cpu time %lg (s) \n", t2 - t0);

  cout << "\n  Thank you for using GMF2VTK" << endl;

  return 0;
}
