#ifdef WIN32
#else
#include <sys/time.h>
#endif

#include <datastruct.h>
#include <msh.h>

static int typeOf[]    = { 0, 0, 0, 0, GmfInt, 0, 0, 0, GmfLong };
static int typeOfTab[] = { 0, 0, 0, 0, GmfIntTab, 0, 0, 0, GmfLongTab };

#ifndef min
#define min(a, b) ((a) < (b) ? (a) : (b))
#endif
#ifndef max
#define max(a, b) ((a) > (b) ? (a) : (b))
#endif

static void viz_writestatus(VizIntStatus* status, char* msg, ...)
{
  va_list arg;
  if (status == NULL)
    return;
  va_start(arg, msg);
  vsnprintf(status->ErrMsg, 512, msg, arg);
  va_end(arg);
}

static void viz_writeinfo(VizIntStatus* status, char* msg, ...)
{
  va_list arg;
  if (status == NULL)
    return;
  va_start(arg, msg);
  vsnprintf(status->InfoMsg, 512, msg, arg);
  va_end(arg);
}

//-- error codes libmeshb
void readMshErr()
{
  fprintf(stderr, "\n  ## ERROR WHILE READING MESH DATA. PLEASE CHECK YOUR DATA\n");
  printf("  Thank you for using VIZIR 4\n");
  exit(1);
}

void readSolErr()
{
  fprintf(stderr, "\n  ## ERROR WHILE READING SOLUTION DATA. PLEASE CHECK YOUR DATA\n");
  printf("  Thank you for using VIZIR 4\n");
  exit(1);
}

static int viz_IsSolKwd(int SolAtEnt)
{
  if (SolAtEnt == GmfSolAtVertices || SolAtEnt == GmfSolAtEdges || SolAtEnt == GmfSolAtTriangles || SolAtEnt == GmfSolAtQuadrilaterals || SolAtEnt == GmfSolAtTetrahedra || SolAtEnt == GmfSolAtPyramids || SolAtEnt == GmfSolAtPrisms || SolAtEnt == GmfSolAtHexahedra)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP1 || SolAtEnt == GmfHOSolAtTrianglesP1 || SolAtEnt == GmfHOSolAtQuadrilateralsQ1 || SolAtEnt == GmfHOSolAtTetrahedraP1 || SolAtEnt == GmfHOSolAtPyramidsP1 || SolAtEnt == GmfHOSolAtPrismsP1 || SolAtEnt == GmfHOSolAtHexahedraQ1)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP2 || SolAtEnt == GmfHOSolAtTrianglesP2 || SolAtEnt == GmfHOSolAtQuadrilateralsQ2 || SolAtEnt == GmfHOSolAtTetrahedraP2 || SolAtEnt == GmfHOSolAtPyramidsP2 || SolAtEnt == GmfHOSolAtPrismsP2 || SolAtEnt == GmfHOSolAtHexahedraQ2)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP3 || SolAtEnt == GmfHOSolAtTrianglesP3 || SolAtEnt == GmfHOSolAtQuadrilateralsQ3 || SolAtEnt == GmfHOSolAtTetrahedraP3 || SolAtEnt == GmfHOSolAtPyramidsP3 || SolAtEnt == GmfHOSolAtPrismsP3 || SolAtEnt == GmfHOSolAtHexahedraQ3)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP4 || SolAtEnt == GmfHOSolAtTrianglesP4 || SolAtEnt == GmfHOSolAtQuadrilateralsQ4 || SolAtEnt == GmfHOSolAtTetrahedraP4 || SolAtEnt == GmfHOSolAtPyramidsP4 || SolAtEnt == GmfHOSolAtPrismsP4 || SolAtEnt == GmfHOSolAtHexahedraQ4)
    return 1;
  else if (SolAtEnt == GmfSolAtBoundaryPolygons || SolAtEnt == GmfSolAtPolyhedra)
    return 1;
  return 0;
}

int viz_NewInterface(VizObject** iObj)
{
  VizObject* Obj = (VizObject*)malloc(sizeof(VizObject));
  if (Obj == NULL)
    return 2;

  viz_IniObject(Obj);

  *iObj = Obj;

  return 0;
}

int viz_FreeInterface(VizObject** Obj)
{

  if (!Obj)
    return 2;
  if (!(*Obj))
    return 2;

  viz_FreeObject(*Obj);

  free(*Obj);

  *Obj = NULL;

  return 0;
}

static solution* viz_ReadSolutionLibMeshb(int64_t InpSol, solution* sol, int FilVer, int SolAt, int NbrFld, int SolSiz, int* SolTyp, int NbrEnt, int Deg, int NbrNod, int Dim, int Ite, double Tim, VizIntStatus* status)
{
  int iSol, i, j, iEnt, idx, BufSiz;

  if (SolSiz <= 0) {
    printf("Warning: the size of the solution is invalid \n");
    printf("Please check your solution file \n");
    printf("In particular, check that your type of each solution is good \n");
    printf("1 for a scalar, 2 for a vector, 3 for symmetric matrix and 4 for a full matrix");
    printf(" \n");
    return NULL;
  }

  solution** Sol = malloc(sizeof(solution*) * NbrFld);
  for (iSol = 0; iSol < NbrFld; iSol++) {
    Sol[iSol] = NULL;
  }

  double* solBuf = malloc(sizeof(double) * (NbrEnt + 1) * (SolSiz));
  int*    sizFld = malloc(sizeof(int) * (NbrFld));

  //-- check parameters
  BufSiz = 0;
  for (iSol = 0; iSol < NbrFld; iSol++) {
    switch (SolTyp[iSol]) {
      case GmfSca:
        sizFld[iSol] = 1;
        break;
      case GmfVec:
        sizFld[iSol] = (Dim == 2) ? 2 : 3;
        break;
      case GmfSymMat:
        sizFld[iSol] = (Dim == 2) ? 3 : 6;
        break;
      case GmfMat:
        sizFld[iSol] = (Dim == 2) ? 4 : 9;
        break;
      default:
        printf("Warning: the following solution type is unkown   %d \n", SolTyp[iSol]);
        break;
    }
    BufSiz += sizFld[iSol];
  }
  BufSiz *= NbrNod;
  if (BufSiz != SolSiz) {
    viz_writestatus(status, "Invalid size of solution for Type %s : %d and it should be %d \n", GmfKwdFmt[SolAt][0], BufSiz, SolSiz);
    return NULL;
  }

  for (iSol = 0; iSol < NbrFld; iSol++) {
    //-- for each field, alloc one solution
    Sol[iSol]           = viz_addsol(sol);
    Sol[iSol]->Dim      = Dim;
    Sol[iSol]->NbrSol   = NbrEnt;
    Sol[iSol]->SolTyp   = SolTyp[iSol];
    Sol[iSol]->Nam[0]   = '\0';
    Sol[iSol]->Sol      = malloc(NbrNod * sizFld[iSol] * (NbrEnt + 1) * sizeof(double));
    Sol[iSol]->Ite      = Ite;
    Sol[iSol]->Tim      = Tim;
    Sol[iSol]->Deg      = Deg;
    Sol[iSol]->NbrNod   = NbrNod;
    Sol[iSol]->vizAlloc = 1;
    sol                 = Sol[iSol];
  }

  GmfGetBlock(InpSol, SolAt, 1, NbrEnt, 0, NULL, NULL, GmfDoubleVec, SolSiz, &solBuf[1 * SolSiz], &solBuf[NbrEnt * SolSiz]);

  for (iEnt = 1; iEnt <= NbrEnt; iEnt++) {
    // copy solution in buffer (split)
    idx = 0;
    for (iSol = 0; iSol < NbrFld; iSol++) {
      for (i = 0; i < NbrNod; i++) {
        for (j = 0; j < sizFld[iSol]; j++)
          Sol[iSol]->Sol[iEnt * sizFld[iSol] * NbrNod + i * sizFld[iSol] + j] = solBuf[iEnt * SolSiz + i * SolSiz / NbrNod + j + idx];
      }
      idx += sizFld[iSol];
    }
  }

  // free buffers
  if (solBuf) {
    free(solBuf);
    solBuf = NULL;
  }

  solution* out_sol = sol;

  out_sol = Sol[0];

  // free the contigus array of solution
  free(Sol);
  Sol = NULL;

  return out_sol;
}

int viz_ReadSolutionsStrings(int64_t InpSol, VizObject* iObj)
{
  int  NbrStr, iStr, iFld, FldKwd;
  char FldNam[4096];

  NbrStr = GmfStatKwd(InpSol, GmfReferenceStrings);
  GmfGotoKwd(InpSol, GmfReferenceStrings);
  for (iStr = 0; iStr < NbrStr; iStr++) {
    GmfGetLin(InpSol, GmfReferenceStrings, &FldKwd, &iFld, &FldNam);
    if (viz_IsSolKwd(FldKwd)) {
      FldNam[strcspn(FldNam, "\n")] = 0; //-- remove end of line
      viz_AttachSolutionName(iObj, FldKwd, iFld, FldNam);
    }
  }

  return VIZINT_SUCCESS;
}

int viz_AttachSolutionName(VizObject* iObj, int LibMshKwd, int iSol, char* Nam)
{
  solution* sol = NULL;
  int       idxSol;

  switch (LibMshKwd) {
    case GmfSolAtVertices:
      sol = iObj->CrdSol;
      break;

    case GmfSolAtEdges:
    case GmfHOSolAtEdgesP1:
      sol = iObj->EdgSol;
      break;

    case GmfHOSolAtEdgesP2:
      sol = iObj->P2EdgSol;
      break;

    case GmfHOSolAtEdgesP3:
      sol = iObj->P3EdgSol;
      break;

    case GmfHOSolAtEdgesP4:
      sol = iObj->P4EdgSol;
      break;

    case GmfSolAtTriangles:
    case GmfHOSolAtTrianglesP1:
      sol = iObj->TriSol;
      break;

    case GmfHOSolAtTrianglesP2:
      sol = iObj->P2TriSol;
      break;

    case GmfHOSolAtTrianglesP3:
      sol = iObj->P3TriSol;
      break;

    case GmfHOSolAtTrianglesP4:
      sol = iObj->P4TriSol;
      break;

    case GmfSolAtQuadrilaterals:
    case GmfHOSolAtQuadrilateralsQ1:
      sol = iObj->QuaSol;
      break;

    case GmfHOSolAtQuadrilateralsQ2:
      sol = iObj->Q2QuaSol;
      break;

    case GmfHOSolAtQuadrilateralsQ3:
      sol = iObj->Q3QuaSol;
      break;

    case GmfHOSolAtQuadrilateralsQ4:
      sol = iObj->Q4QuaSol;
      break;

    case GmfSolAtTetrahedra:
    case GmfHOSolAtTetrahedraP1:
      sol = iObj->TetSol;
      break;

    case GmfHOSolAtTetrahedraP2:
      sol = iObj->P2TetSol;
      break;

    case GmfHOSolAtTetrahedraP3:
      sol = iObj->P3TetSol;
      break;

    case GmfHOSolAtTetrahedraP4:
      sol = iObj->P4TetSol;
      break;

    case GmfSolAtPrisms:
    case GmfHOSolAtPrismsP1:
      sol = iObj->PriSol;
      break;

    case GmfHOSolAtPrismsP2:
      sol = iObj->P2PriSol;
      break;

    case GmfHOSolAtPrismsP3:
      sol = iObj->P3PriSol;
      break;

    case GmfHOSolAtPrismsP4:
      sol = iObj->P4PriSol;
      break;

    case GmfSolAtPyramids:
    case GmfHOSolAtPyramidsP1:
      sol = iObj->PyrSol;
      break;

    case GmfHOSolAtPyramidsP2:
      sol = iObj->P2PyrSol;
      break;

    case GmfHOSolAtPyramidsP3:
      sol = iObj->P3PyrSol;
      break;

    case GmfHOSolAtPyramidsP4:
      sol = iObj->P4PyrSol;
      break;

    case GmfSolAtHexahedra:
    case GmfHOSolAtHexahedraQ1:
      sol = iObj->HexSol;
      break;

    case GmfHOSolAtHexahedraQ2:
      sol = iObj->Q2HexSol;
      break;

    case GmfHOSolAtHexahedraQ3:
      sol = iObj->Q3HexSol;
      break;

    case GmfHOSolAtHexahedraQ4:
      sol = iObj->Q4HexSol;
      break;

    default:
      printf(" ERROR: Keyword %s (= %d) is not of solution type\n", GmfKwdFmt[LibMshKwd][0], LibMshKwd);
      return VIZINT_ERROR;
  }

  idxSol = 0;
  while (sol) {
    idxSol++;
    if (idxSol == iSol)
      break;
    else
      sol = sol->nxt;
  }
  if (idxSol == iSol) {
    sprintf(sol->Nam, "%s", Nam);
  }
  else {
    printf(" ERROR: Solution # %d corresponding to keyword %s (= %d) does not exists\n", iSol, GmfKwdFmt[LibMshKwd][0], LibMshKwd);
    return VIZINT_ERROR;
  }
  return VIZINT_SUCCESS;
}

void viz_ConvertHOSolutionInVizirBasis(int64_t InpSol, int SolAtNodesPositions, solution* sol)
{
#if defined(INC_F77)

  int     CrdNodRefSiz, iSol, iNod, jNod, i, j, k, n, sizFld;
  double *CrdNodRef, *CrdNodUsr;
  double *Mat = NULL, *Mat2 = NULL, *solBuf = NULL, *solBuf2 = NULL;
  int     info, lda, lwork;
  double* work;
  int*    ipiv;

  work = NULL;
  ipiv = NULL;

  solution* curSol = NULL;

  CrdNodRef = NULL;

  if (sol->Deg <= 0 || sol->Deg > 10)
    return;

  CrdNodRefSiz = GetCrdNodRefSiz(SolAtNodesPositions);
  CrdNodUsr    = malloc(CrdNodRefSiz * sol->NbrNod * sizeof(double));

  if (CrdNodRefSiz == 2) {
    GmfGetBlock(InpSol, SolAtNodesPositions, 1, sol->NbrNod, 0, NULL, NULL,
        GmfDouble, &(CrdNodUsr[0]), &(CrdNodUsr[2 * (sol->NbrNod - 1) + 0]),
        GmfDouble, &(CrdNodUsr[1]), &(CrdNodUsr[2 * (sol->NbrNod - 1) + 1]));
  }
  else if (CrdNodRefSiz == 3) {
    GmfGetBlock(InpSol, SolAtNodesPositions, 1, sol->NbrNod, 0, NULL, NULL,
        GmfDouble, &(CrdNodUsr[0]), &(CrdNodUsr[3 * (sol->NbrNod - 1) + 0]),
        GmfDouble, &(CrdNodUsr[1]), &(CrdNodUsr[3 * (sol->NbrNod - 1) + 1]),
        GmfDouble, &(CrdNodUsr[2]), &(CrdNodUsr[3 * (sol->NbrNod - 1) + 2]));
  }
  else if (CrdNodRefSiz == 4) {
    GmfGetBlock(InpSol, SolAtNodesPositions, 1, sol->NbrNod, 0, NULL, NULL,
        GmfDouble, &(CrdNodUsr[0]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 0]),
        GmfDouble, &(CrdNodUsr[1]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 1]),
        GmfDouble, &(CrdNodUsr[2]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 2]),
        GmfDouble, &(CrdNodUsr[3]), &(CrdNodUsr[4 * (sol->NbrNod - 1) + 3]));
  }
  else {
    printf("Wrong keyword for HO solution Nodes positions conversion\n");
    free(CrdNodUsr);
    CrdNodUsr = NULL;
    return;
  }

  //-- pyramid is special -> we do not really have the shape functions but only the "monomial" basis proposed by Bergot, Cohen and Durufle in 2010
  if (SolAtNodesPositions == GmfHOSolAtPyramidsP1NodesPositions || SolAtNodesPositions == GmfHOSolAtPyramidsP2NodesPositions || SolAtNodesPositions == GmfHOSolAtPyramidsP3NodesPositions || SolAtNodesPositions == GmfHOSolAtPyramidsP4NodesPositions) {

    CrdNodRef = GetCrdNodRef(SolAtNodesPositions, sol->Deg);

    //---  Vandermonde matrices of "monomials" in user and ref basis
    Mat  = (double*)malloc(sol->NbrNod * sol->NbrNod * sizeof(double));
    Mat2 = (double*)malloc(sol->NbrNod * sol->NbrNod * sizeof(double));

    //-- fill in matrices
    for (iNod = 0; iNod < sol->NbrNod; iNod++) {   //--lines
      for (jNod = 0; jNod < sol->NbrNod; jNod++) { //-- column
        i                               = ((int*)PyramidsOrderingPtr[sol->Deg])[3 * jNod + 0];
        j                               = ((int*)PyramidsOrderingPtr[sol->Deg])[3 * jNod + 1];
        k                               = ((int*)PyramidsOrderingPtr[sol->Deg])[3 * jNod + 2];
        Mat[iNod * sol->NbrNod + jNod]  = pow((CrdNodUsr[3 * iNod]) / (1. - CrdNodUsr[3 * iNod + 2]), i) * pow((CrdNodUsr[3 * iNod + 1]) / (1. - CrdNodUsr[3 * iNod + 2]), j) * pow((1. - CrdNodUsr[3 * iNod + 2]), k);
        Mat2[iNod * sol->NbrNod + jNod] = pow((CrdNodRef[3 * iNod]) / (1. - CrdNodRef[3 * iNod + 2]), i) * pow((CrdNodRef[3 * iNod + 1]) / (1. - CrdNodRef[3 * iNod + 2]), j) * pow((1. - CrdNodRef[3 * iNod + 2]), k);
      }
    }

    //-- set variables to call lapack
    n     = sol->NbrNod;
    info  = 0;
    lda   = n;
    lwork = n;
    work  = malloc(lwork * sizeof(double));
    ipiv  = malloc(n * sizeof(double));

    memset(ipiv, 0, n * sizeof(int));

    //-- get LU decomp from LAPACK
    call(dgetrf)(&n, &n, Mat, &lda, ipiv, &info);

    //-- inverse matrix after LU decomp using LAPACK
    call(dgetri)(&n, Mat, &lda, ipiv, work, &lwork, &info);

    free(work);
    free(ipiv);
    work = NULL;
    ipiv = NULL;

    //--- check if it succeeded
    if (info != 0) {
      printf("HO solution Nodes positions conversion failed for pyramid as the matrix cannot be inverted. Display of the nodes in vizir uniform nodes distribution \n");
      free(CrdNodUsr);
      CrdNodUsr = NULL;
      free(CrdNodRef);
      CrdNodRef = NULL;
      free(Mat);
      Mat = NULL;
      free(Mat2);
      Mat2 = NULL;
      return;
    }

    //-- temporary solutions.
    solBuf  = malloc(sol->NbrNod * sizeof(double));
    solBuf2 = malloc(sol->NbrNod * sizeof(double));

    // inversion of the mapping succeed, now let us convert the solution
    curSol = sol;
    while (curSol != NULL) {                                   //--- loop over the fields
      sizFld = viz_GetSolSize(curSol->Dim, curSol->SolTyp, 1); //-- get the size of the field <=> size of the total solution size for field with one node
      for (i = 0; i < sizFld; i++) {                           //-- loop over each component of the field
        for (iSol = 1; iSol <= curSol->NbrSol; iSol++) {       //-- loop over all the mesh entities
          for (iNod = 0; iNod < curSol->NbrNod; iNod++)        //-- store the current sol at element in a buffer array
            solBuf[iNod] = curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i];

          //-- perform two matrix vector product to compute the new solution
          //-- get coefficients in Bergot's basis
          for (iNod = 0; iNod < curSol->NbrNod; iNod++) {
            solBuf2[iNod] = 0.;
            for (jNod = 0; jNod < curSol->NbrNod; jNod++)
              solBuf2[iNod] += Mat[iNod * curSol->NbrNod + jNod] * solBuf[jNod];
          }
          //-- get the nodes in the uniform basis
          for (iNod = 0; iNod < curSol->NbrNod; iNod++) {
            curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] = 0.;
            for (jNod = 0; jNod < curSol->NbrNod; jNod++)
              curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] += Mat2[iNod * curSol->NbrNod + jNod] * solBuf2[jNod];
          }
        }
      }
      curSol = curSol->nxt;
    }

    free(CrdNodUsr);
    free(CrdNodRef);
    CrdNodUsr = NULL;
    CrdNodRef = NULL;
    free(solBuf);
    solBuf = NULL;
    free(solBuf2);
    solBuf2 = NULL;
    free(Mat);
    Mat = NULL;
    free(Mat2);
    Mat2 = NULL;
  }
  else {

    //-- Assembling of the matrix that will be inverted
    //-- fill in matrix with uniform shape functions evaluated in user's node points
    Mat = (double*)malloc(sol->NbrNod * sol->NbrNod * sizeof(double));
    for (iNod = 0; iNod < sol->NbrNod; iNod++) { //--lines
      for (jNod = 0; jNod < sol->NbrNod; jNod++) //-- columns
        Mat[sol->NbrNod * iNod + jNod] = viz_UniformShapeFunctionEvaluation(SolAtNodesPositions, sol->Deg, jNod, (double*)&CrdNodUsr[CrdNodRefSiz * iNod]);
    }

    //-- set variables to call lapack
    n     = sol->NbrNod;
    info  = 0;
    lda   = n;
    lwork = n;
    work  = malloc(lwork * sizeof(double));
    ipiv  = malloc(n * sizeof(double));

    memset(ipiv, 0, n * sizeof(int));

    //-- get LU decomp from LAPACK
    call(dgetrf)(&n, &n, Mat, &lda, ipiv, &info);

    //-- inverse matrix after LU decomp using LAPACK
    call(dgetri)(&n, Mat, &lda, ipiv, work, &lwork, &info);

    free(work);
    free(ipiv);
    work = NULL;
    ipiv = NULL;

    //--- check if it succeeded
    if (info != 0) {
      printf("HO solution Nodes positions conversion failed as the matrix cannot be inverted. Display of the nodes in vizir uniform nodes distribution \n");
      free(CrdNodUsr);
      CrdNodUsr = NULL;
      free(Mat);
      Mat = NULL;
      return;
    }

    solBuf = malloc(sol->NbrNod * sizeof(double));

    // inversion of the mapping succeed, now let us convert the solution
    curSol = sol;
    while (curSol != NULL) {                                   //--- loop over the fields
      sizFld = viz_GetSolSize(curSol->Dim, curSol->SolTyp, 1); //-- get the size of the field <=> size of the total solution size for field with one node
      for (i = 0; i < sizFld; i++) {                           //-- loop over each component of the field
        for (iSol = 1; iSol <= curSol->NbrSol; iSol++) {       //-- loop over all the mesh entities
          for (iNod = 0; iNod < curSol->NbrNod; iNod++)        //-- store the current sol at element in a buffer array
            solBuf[iNod] = curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i];

          for (iNod = 0; iNod < curSol->NbrNod; iNod++) { //-- perform matrix vector product to coimpute the new solution
            curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] = 0.;
            for (jNod = 0; jNod < curSol->NbrNod; jNod++)
              curSol->Sol[iSol * sizFld * curSol->NbrNod + iNod * sizFld + i] += Mat[iNod * curSol->NbrNod + jNod] * solBuf[jNod];
          }
        }
      }
      curSol = curSol->nxt;
    }

    free(CrdNodUsr);
    CrdNodUsr = NULL;
    free(solBuf);
    solBuf = NULL;
    free(Mat);
    Mat = NULL;
  }

#else
  printf("HO solution Nodes positions conversion not supported as F77 libraries are not included in the project\n");
#endif
  return;
}

int viz_OpenMesh(VizObject* iObj, const char* MshFile, VizIntStatus* status)
{
  int ierr;

  //--- redirect error signals
#if defined(__linux__) || defined(__APPLE__)
  signal(SIGILL, readMshErr);
  signal(SIGFPE, readMshErr);
  signal(SIGBUS, readMshErr);
  signal(SIGSEGV, readMshErr);
  signal(SIGQUIT, readMshErr);
#endif
  ierr = viz_OpenMeshLibMeshb(iObj, MshFile, status);
//--- reset default error signals
#if defined(__linux__) || defined(__APPLE__)
  signal(SIGILL, SIG_DFL);
  signal(SIGFPE, SIG_DFL);
  signal(SIGBUS, SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
  signal(SIGQUIT, SIG_DFL);
#endif
  return ierr;
}

int viz_OpenMeshLibMeshb(VizObject* Obj, const char* MshFile, VizIntStatus* status)
{

  int1 iVer;

  //--- HO numbering arrays
  int1 P2EdgFilOrd[3], P3EdgFilOrd[4], P4EdgFilOrd[5], P1TriFilOrd[3][3], P2TriFilOrd[6][3], P3TriFilOrd[10][3], P4TriFilOrd[15][3], Q2QuaFilOrd[9][2], Q3QuaFilOrd[16][2], Q4QuaFilOrd[25][2];
  int1 P2TetFilOrd[10][4], P3TetFilOrd[20][4], P4TetFilOrd[35][4], Q2HexFilOrd[27][3], Q3HexFilOrd[64][3], Q4HexFilOrd[125][3], P2PyrFilOrd[14][3], P3PyrFilOrd[30][3], P4PyrFilOrd[55][3], P2PriFilOrd[18][4], P3PriFilOrd[40][4], P4PriFilOrd[75][4];

  char bufNam[1024];
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!MshFile) {
    viz_writestatus(status, "NULL (MshFile)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  int     FilVer, DimRead;
  int64_t InpMsh = GmfOpenMesh(MshFile, GmfRead, &FilVer, &DimRead);
  if (InpMsh <= 0) {
    if (strstr(MshFile, ".mesh") == NULL) {
      sprintf(bufNam, "%s.meshb", MshFile);
      InpMsh = GmfOpenMesh(bufNam, GmfRead, &FilVer, &DimRead);
      if (InpMsh <= 0) {
        sprintf(bufNam, "%s.mesh", MshFile);
        InpMsh = GmfOpenMesh(bufNam, GmfRead, &FilVer, &DimRead);
        if (InpMsh <= 0) {
          viz_writestatus(status, "Cannot open mesh file %s.[mesh[b]]\n", MshFile);
          return VIZINT_ERROR;
        }
      }
      viz_writeinfo(status, "%s", bufNam);
    }
    else {
      viz_writestatus(status, "Cannot open mesh file %s\n", MshFile);
      return VIZINT_ERROR;
    }
  }
  else
    viz_writeinfo(status, "%s", MshFile);

  /* free previous mesh */
  viz_FreeObject(Obj);

  Obj->Dim = DimRead;

  //--- read HO numbering arrays if any
  if (GmfStatKwd(InpMsh, GmfEdgesP2Ordering)) {
    GmfGetBlock(InpMsh, GmfEdgesP2Ordering, 1, 3, 0, NULL, NULL, typeOf[sizeof(int1)], &(P2EdgFilOrd[0]), &(P2EdgFilOrd[2]));
    GmfSetHONodesOrdering(InpMsh, GmfEdgesP2, (int*)P2EdgesOrdering, (int*)P2EdgFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfEdgesP3Ordering)) {
    GmfGetBlock(InpMsh, GmfEdgesP3Ordering, 1, 4, 0, NULL, NULL, typeOf[sizeof(int1)], &(P3EdgFilOrd[0]), &(P3EdgFilOrd[3]));
    GmfSetHONodesOrdering(InpMsh, GmfEdgesP3, (int*)P3EdgesOrdering, (int*)P3EdgFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfEdgesP4Ordering)) {
    GmfGetBlock(InpMsh, GmfEdgesP4Ordering, 1, 5, 0, NULL, NULL, typeOf[sizeof(int1)], &(P4EdgFilOrd[0]), &(P4EdgFilOrd[4]));
    GmfSetHONodesOrdering(InpMsh, GmfEdgesP4, (int*)P4EdgesOrdering, (int*)P4EdgFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP1Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP1Ordering, 1, 3, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P1TriFilOrd[0], P1TriFilOrd[2]);
    GmfSetHONodesOrdering(InpMsh, GmfTriangles, (int*)P1TrianglesOrdering, (int*)P1TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP2Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP2Ordering, 1, 6, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P2TriFilOrd[0], P2TriFilOrd[5]);
    GmfSetHONodesOrdering(InpMsh, GmfTrianglesP2, (int*)P2TrianglesOrdering, (int*)P2TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP3Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP3Ordering, 1, 10, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P3TriFilOrd[0], P3TriFilOrd[9]);
    GmfSetHONodesOrdering(InpMsh, GmfTrianglesP3, (int*)P3TrianglesOrdering, (int*)P3TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTrianglesP4Ordering)) {
    GmfGetBlock(InpMsh, GmfTrianglesP4Ordering, 1, 15, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P4TriFilOrd[0], P4TriFilOrd[14]);
    GmfSetHONodesOrdering(InpMsh, GmfTrianglesP4, (int*)P4TrianglesOrdering, (int*)P4TriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfQuadrilateralsQ2Ordering)) {
    GmfGetBlock(InpMsh, GmfQuadrilateralsQ2Ordering, 1, 9, 0, NULL, NULL, typeOfTab[sizeof(int1)], 2, Q2QuaFilOrd[0], Q2QuaFilOrd[8]);
    GmfSetHONodesOrdering(InpMsh, GmfQuadrilateralsQ2, (int*)Q2QuadrilateralsOrdering, (int*)Q2QuaFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfQuadrilateralsQ3Ordering)) {
    GmfGetBlock(InpMsh, GmfQuadrilateralsQ3Ordering, 1, 16, 0, NULL, NULL, typeOfTab[sizeof(int1)], 2, Q3QuaFilOrd[0], Q3QuaFilOrd[15]);
    GmfSetHONodesOrdering(InpMsh, GmfQuadrilateralsQ3, (int*)Q3QuadrilateralsOrdering, (int*)Q3QuaFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfQuadrilateralsQ4Ordering)) {
    GmfGetBlock(InpMsh, GmfQuadrilateralsQ4Ordering, 1, 25, 0, NULL, NULL, typeOfTab[sizeof(int1)], 2, Q4QuaFilOrd[0], Q4QuaFilOrd[24]);
    GmfSetHONodesOrdering(InpMsh, GmfQuadrilateralsQ4, (int*)Q4QuadrilateralsOrdering, (int*)Q4QuaFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTetrahedraP2Ordering)) {
    GmfGetBlock(InpMsh, GmfTetrahedraP2Ordering, 1, 10, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P2TetFilOrd[0], P2TetFilOrd[9]);
    GmfSetHONodesOrdering(InpMsh, GmfTetrahedraP2, (int*)P2TetrahedraOrdering, (int*)P2TetFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTetrahedraP3Ordering)) {
    GmfGetBlock(InpMsh, GmfTetrahedraP3Ordering, 1, 20, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P3TetFilOrd[0], P3TetFilOrd[19]);
    GmfSetHONodesOrdering(InpMsh, GmfTetrahedraP3, (int*)P3TetrahedraOrdering, (int*)P3TetFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfTetrahedraP4Ordering)) {
    GmfGetBlock(InpMsh, GmfTetrahedraP4Ordering, 1, 35, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P4TetFilOrd[0], P4TetFilOrd[34]);
    GmfSetHONodesOrdering(InpMsh, GmfTetrahedraP4, (int*)P4TetrahedraOrdering, (int*)P4TetFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPyramidsP2Ordering)) {
    GmfGetBlock(InpMsh, GmfPyramidsP2Ordering, 1, 14, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P2PyrFilOrd[0], P2PyrFilOrd[13]);
    GmfSetHONodesOrdering(InpMsh, GmfPyramidsP2, (int*)P2PyramidsOrdering, (int*)P2PyrFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPyramidsP3Ordering)) {
    GmfGetBlock(InpMsh, GmfPyramidsP3Ordering, 1, 30, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P3PyrFilOrd[0], P3PyrFilOrd[29]);
    GmfSetHONodesOrdering(InpMsh, GmfPyramidsP3, (int*)P3PyramidsOrdering, (int*)P3PyrFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPyramidsP4Ordering)) {
    GmfGetBlock(InpMsh, GmfPyramidsP4Ordering, 1, 55, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, P4PyrFilOrd[0], P4PyrFilOrd[54]);
    GmfSetHONodesOrdering(InpMsh, GmfPyramidsP4, (int*)P4PyramidsOrdering, (int*)P4PyrFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPrismsP2Ordering)) {
    GmfGetBlock(InpMsh, GmfPrismsP2Ordering, 1, 18, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P2PriFilOrd[0], P2PriFilOrd[17]);
    GmfSetHONodesOrdering(InpMsh, GmfPrismsP2, (int*)P2PrismsOrdering, (int*)P2PriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPrismsP3Ordering)) {
    GmfGetBlock(InpMsh, GmfPrismsP3Ordering, 1, 40, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P3PriFilOrd[0], P3PriFilOrd[39]);
    GmfSetHONodesOrdering(InpMsh, GmfPrismsP3, (int*)P3PrismsOrdering, (int*)P3PriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfPrismsP4Ordering)) {
    GmfGetBlock(InpMsh, GmfPrismsP4Ordering, 1, 75, 0, NULL, NULL, typeOfTab[sizeof(int1)], 4, P4PriFilOrd[0], P4PriFilOrd[74]);
    GmfSetHONodesOrdering(InpMsh, GmfPrismsP4, (int*)P4PrismsOrdering, (int*)P4PriFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfHexahedraQ2Ordering)) {
    GmfGetBlock(InpMsh, GmfHexahedraQ2Ordering, 1, 27, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, Q2HexFilOrd[0], Q2HexFilOrd[26]);
    GmfSetHONodesOrdering(InpMsh, GmfHexahedraQ2, (int*)Q2HexahedraOrdering, (int*)Q2HexFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfHexahedraQ3Ordering)) {
    GmfGetBlock(InpMsh, GmfHexahedraQ3Ordering, 1, 64, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, Q3HexFilOrd[0], Q3HexFilOrd[63]);
    GmfSetHONodesOrdering(InpMsh, GmfHexahedraQ3, (int*)Q3HexahedraOrdering, (int*)Q3HexFilOrd);
  }
  if (GmfStatKwd(InpMsh, GmfHexahedraQ4Ordering)) {
    GmfGetBlock(InpMsh, GmfHexahedraQ4Ordering, 1, 125, 0, NULL, NULL, typeOfTab[sizeof(int1)], 3, Q4HexFilOrd[0], Q4HexFilOrd[124]);
    GmfSetHONodesOrdering(InpMsh, GmfHexahedraQ4, (int*)Q4HexahedraOrdering, (int*)Q4HexFilOrd);
  }

  /* Reading vertices */
  Obj->NbrVer = GmfStatKwd(InpMsh, GmfVertices);
  Obj->Crd    = viz_reallocate(sizeof(double3) * (Obj->NbrVer + 1), Obj->Crd);
  if (!Obj->Crd) {
    viz_writestatus(status, "Allocation failed for vertex-arrays, size %lg Mb\n", sizeof(double3) * (Obj->NbrVer + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->CrdRef = viz_reallocate(sizeof(int1) * (Obj->NbrVer + 1), Obj->CrdRef);
  if (!Obj->CrdRef) {
    viz_writestatus(status, "Allocation failed for vertex-arrays, size %lg Mb\n", sizeof(double3) * (Obj->NbrVer + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }

  /* Reading vertices */
  if (DimRead == 3) {
    Obj->NbrVerMax = Obj->NbrVer;
    // GmfGotoKwd(InpMsh, GmfVertices);
    GmfGetBlock(InpMsh, GmfVertices, 1, Obj->NbrVer, 0, NULL, NULL,
        GmfDouble, &(Obj->Crd[1][0]), &(Obj->Crd[Obj->NbrVer][0]),
        GmfDouble, &(Obj->Crd[1][1]), &(Obj->Crd[Obj->NbrVer][1]),
        GmfDouble, &(Obj->Crd[1][2]), &(Obj->Crd[Obj->NbrVer][2]),
        typeOf[sizeof(int1)], &Obj->CrdRef[1], &Obj->CrdRef[Obj->NbrVer]);
  }
  else {
    Obj->NbrVerMax = Obj->NbrVer;
    // GmfGotoKwd(InpMsh, GmfVertices);
    GmfGetBlock(InpMsh, GmfVertices, 1, Obj->NbrVer, 0, NULL, NULL,
        GmfDouble, &(Obj->Crd[1][0]), &(Obj->Crd[Obj->NbrVer][0]),
        GmfDouble, &(Obj->Crd[1][1]), &(Obj->Crd[Obj->NbrVer][1]),
        typeOf[sizeof(int1)], &Obj->CrdRef[1], &Obj->CrdRef[Obj->NbrVer]);
    for (iVer = 1; iVer <= Obj->NbrVer; iVer++)
      Obj->Crd[iVer][2] = 0.0;
  }

  /* Reading edges */
  Obj->NbrEdg = GmfStatKwd(InpMsh, GmfEdges);
  Obj->Edg    = viz_reallocate(sizeof(int2) * (Obj->NbrEdg + 1), Obj->Edg);
  Obj->EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrEdg + 1), Obj->EdgRef);
  if (!Obj->Edg || !Obj->EdgRef) {
    viz_writestatus(status, "Allocation failed for Edge-arrays, size %lg Mb\n", sizeof(int2) * (Obj->NbrEdg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrEdgMax = Obj->NbrEdg;
  // GmfGotoKwd(InpMsh, GmfEdges);
  GmfGetBlock(InpMsh, GmfEdges, 1, Obj->NbrEdg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 2, &Obj->Edg[1], &Obj->Edg[Obj->NbrEdg],
      typeOf[sizeof(int1)], &Obj->EdgRef[1], &Obj->EdgRef[Obj->NbrEdg]);

  /* Reading corners */
  Obj->NbrCrn = GmfStatKwd(InpMsh, GmfCorners);
  Obj->Crn    = viz_reallocate(sizeof(int1) * (Obj->NbrCrn + 1), Obj->Crn);
  Obj->CrnRef = viz_reallocate(sizeof(int1) * (Obj->NbrCrn + 1), Obj->CrnRef);
  if (!Obj->Crn) {
    viz_writestatus(status, "Allocation failed for corners-arrays, size %lg Mb\n", sizeof(int) * (Obj->NbrCrn + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrCrnMax = Obj->NbrCrn;
  // GmfGotoKwd(InpMsh, GmfCorners);
  GmfGetBlock(InpMsh, GmfCorners, 1, Obj->NbrCrn, 0, NULL, NULL,
      typeOf[sizeof(int1)], &Obj->Crn[1], &Obj->Crn[Obj->NbrCrn]);

  for (iVer = 1; iVer <= Obj->NbrCrn; iVer++) {
    if (Obj->Crn[iVer] < Obj->NbrVer)
      Obj->CrnRef[iVer] = Obj->CrdRef[Obj->Crn[iVer]];
    else
      Obj->CrnRef[iVer] = 0;
  }

  /* Reading triangles */
  Obj->NbrTri = GmfStatKwd(InpMsh, GmfTriangles);
  Obj->Tri    = viz_reallocate(sizeof(int3) * (Obj->NbrTri + 1), Obj->Tri);
  Obj->TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrTri + 1), Obj->TriRef);
  if (!Obj->Tri || !Obj->TriRef) {
    viz_writestatus(status, "Allocation failed for triangle-arrays, size %lg Mb\n", sizeof(int3) * (Obj->NbrTri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrTriMax = Obj->NbrTri;
  // GmfGotoKwd(InpMsh, GmfTriangles);
  GmfGetBlock(InpMsh, GmfTriangles, 1, Obj->NbrTri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 3, &Obj->Tri[1], &Obj->Tri[Obj->NbrTri],
      typeOf[sizeof(int1)], &Obj->TriRef[1], &Obj->TriRef[Obj->NbrTri]);

  /* Reading quads */
  Obj->NbrQua = GmfStatKwd(InpMsh, GmfQuadrilaterals);
  Obj->Qua    = viz_reallocate(sizeof(int4) * (Obj->NbrQua + 1), Obj->Qua);
  Obj->QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQua + 1), Obj->QuaRef);
  if (!Obj->Qua || !Obj->QuaRef) {
    viz_writestatus(status, "Allocation failed for Quad-arrays, size %lg Mb\n", sizeof(int4) * (Obj->NbrQua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQuaMax = Obj->NbrQua;
  // GmfGotoKwd(InpMsh, GmfQuadrilaterals);
  GmfGetBlock(InpMsh, GmfQuadrilaterals, 1, Obj->NbrQua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 4, &Obj->Qua[1], &Obj->Qua[Obj->NbrQua],
      typeOf[sizeof(int1)], &Obj->QuaRef[1], &Obj->QuaRef[Obj->NbrQua]);

  /* Reading tets */
  Obj->NbrTet = GmfStatKwd(InpMsh, GmfTetrahedra);
  Obj->Tet    = viz_reallocate(sizeof(int4) * (Obj->NbrTet + 1), Obj->Tet);
  Obj->TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrTet + 1), Obj->TetRef);
  if (!Obj->Tet || !Obj->TetRef) {
    viz_writestatus(status, "Allocation failed for Tet-arrays, size %lg Mb\n", sizeof(int4) * (Obj->NbrTet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrTetMax = Obj->NbrTet;
  // GmfGotoKwd(InpMsh, GmfTetrahedra);
  GmfGetBlock(InpMsh, GmfTetrahedra, 1, Obj->NbrTet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 4, &Obj->Tet[1], &Obj->Tet[Obj->NbrTet],
      typeOf[sizeof(int1)], &Obj->TetRef[1], &Obj->TetRef[Obj->NbrTet]);

  /* Reading Pyramids */
  Obj->NbrPyr = GmfStatKwd(InpMsh, GmfPyramids);
  Obj->Pyr    = viz_reallocate(sizeof(int5) * (Obj->NbrPyr + 1), Obj->Pyr);
  Obj->PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrPyr + 1), Obj->PyrRef);
  if (!Obj->Pyr || !Obj->PyrRef) {
    viz_writestatus(status, "Allocation failed for Pyramid-arrays, size %lg Mb\n", sizeof(int5) * (Obj->NbrPyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrPyrMax = Obj->NbrPyr;
  // GmfGotoKwd(InpMsh,  GmfPyramids);
  GmfGetBlock(InpMsh, GmfPyramids, 1, Obj->NbrPyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 5, &Obj->Pyr[1], &Obj->Pyr[Obj->NbrPyr],
      typeOf[sizeof(int1)], &Obj->PyrRef[1], &Obj->PyrRef[Obj->NbrPyr]);

  /* Reading Prisms */
  Obj->NbrPri = GmfStatKwd(InpMsh, GmfPrisms);
  Obj->Pri    = viz_reallocate(sizeof(int6) * (Obj->NbrPri + 1), Obj->Pri);
  Obj->PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrPri + 1), Obj->PriRef);
  if (!Obj->Pri || !Obj->PriRef) {
    viz_writestatus(status, "Allocation failed for Prism-arrays, size %lg Mb\n", sizeof(int6) * (Obj->NbrPri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrPriMax = Obj->NbrPri;
  // GmfGotoKwd(InpMsh,  GmfPrisms);
  GmfGetBlock(InpMsh, GmfPrisms, 1, Obj->NbrPri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 6, &Obj->Pri[1], &Obj->Pri[Obj->NbrPri],
      typeOf[sizeof(int1)], &Obj->PriRef[1], &Obj->PriRef[Obj->NbrPri]);

  /* Reading hexahedra */
  Obj->NbrHex = GmfStatKwd(InpMsh, GmfHexahedra);
  Obj->Hex    = viz_reallocate(sizeof(int8) * (Obj->NbrHex + 1), Obj->Hex);
  Obj->HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrHex + 1), Obj->HexRef);
  if (!Obj->Hex || !Obj->HexRef) {
    viz_writestatus(status, "Allocation failed for Hex-arrays, size %lg Mb\n", sizeof(int8) * (Obj->NbrHex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrHexMax = Obj->NbrHex;
  // GmfGotoKwd(InpMsh,  GmfHexahedra);
  GmfGetBlock(InpMsh, GmfHexahedra, 1, Obj->NbrHex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 8, &Obj->Hex[1], &Obj->Hex[Obj->NbrHex],
      typeOf[sizeof(int1)], &Obj->HexRef[1], &Obj->HexRef[Obj->NbrHex]);

  /* Read High order entities if any */

  /* reading P2 edges */
  Obj->NbrP2Edg = GmfStatKwd(InpMsh, GmfEdgesP2);
  Obj->P2Edg    = viz_reallocate(sizeof(int3) * (Obj->NbrP2Edg + 1), Obj->P2Edg);
  Obj->P2EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Edg + 1), Obj->P2EdgRef);
  if (!Obj->P2Edg || !Obj->P2EdgRef) {
    viz_writestatus(status, "Allocation failed for P2Edge-arrays, size %lg Mb\n", sizeof(int3) * (Obj->NbrP2Edg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2EdgMax = Obj->NbrP2Edg;
  // GmfGotoKwd(InpMsh, GmfEdgesP2);
  GmfGetBlock(InpMsh, GmfEdgesP2, 1, Obj->NbrP2Edg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 3, &Obj->P2Edg[1], &Obj->P2Edg[Obj->NbrP2Edg],
      typeOf[sizeof(int1)], &Obj->P2EdgRef[1], &Obj->P2EdgRef[Obj->NbrP2Edg]);

  /* reading P3 edges */
  Obj->NbrP3Edg = GmfStatKwd(InpMsh, GmfEdgesP3);
  Obj->P3Edg    = viz_reallocate(sizeof(int4) * (Obj->NbrP3Edg + 1), Obj->P3Edg);
  Obj->P3EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Edg + 1), Obj->P3EdgRef);
  if (!Obj->P3Edg || !Obj->P3EdgRef) {
    viz_writestatus(status, "Allocation failed for P3Edge-arrays, size %lg Mb\n", sizeof(int4) * (Obj->NbrP3Edg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3EdgMax = Obj->NbrP3Edg;
  // GmfGotoKwd(InpMsh, GmfEdgesP3);
  GmfGetBlock(InpMsh, GmfEdgesP3, 1, Obj->NbrP3Edg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 4, &Obj->P3Edg[1], &Obj->P3Edg[Obj->NbrP3Edg],
      typeOf[sizeof(int1)], &Obj->P3EdgRef[1], &Obj->P3EdgRef[Obj->NbrP3Edg]);

  /* reading P4 edges */
  Obj->NbrP4Edg = GmfStatKwd(InpMsh, GmfEdgesP4);
  Obj->P4Edg    = viz_reallocate(sizeof(int5) * (Obj->NbrP4Edg + 1), Obj->P4Edg);
  Obj->P4EdgRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Edg + 1), Obj->P4EdgRef);
  if (!Obj->P4Edg || !Obj->P4EdgRef) {
    viz_writestatus(status, "Allocation failed for P4Edge-arrays, size %lg Mb\n", sizeof(int5) * (Obj->NbrP4Edg + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4EdgMax = Obj->NbrP4Edg;
  // GmfGotoKwd(InpMsh, GmfEdgesP4);
  GmfGetBlock(InpMsh, GmfEdgesP4, 1, Obj->NbrP4Edg, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 5, &Obj->P4Edg[1], &Obj->P4Edg[Obj->NbrP4Edg],
      typeOf[sizeof(int1)], &Obj->P4EdgRef[1], &Obj->P4EdgRef[Obj->NbrP4Edg]);

  /* Reading P2 triangles */
  Obj->NbrP2Tri = GmfStatKwd(InpMsh, GmfTrianglesP2);
  Obj->P2Tri    = viz_reallocate(sizeof(int6) * (Obj->NbrP2Tri + 1), Obj->P2Tri);
  Obj->P2TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Tri + 1), Obj->P2TriRef);
  if (!Obj->P2Tri || !Obj->P2TriRef) {
    viz_writestatus(status, "Allocation failed for P2Triangle-arrays, size %lg Mb\n", sizeof(int6) * (Obj->NbrP2Tri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2TriMax = Obj->NbrP2Tri;
  // GmfGotoKwd(InpMsh, GmfTrianglesP2);
  GmfGetBlock(InpMsh, GmfTrianglesP2, 1, Obj->NbrP2Tri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 6, &Obj->P2Tri[1], &Obj->P2Tri[Obj->NbrP2Tri],
      typeOf[sizeof(int1)], &Obj->P2TriRef[1], &Obj->P2TriRef[Obj->NbrP2Tri]);

  /* Reading P3 triangles */
  Obj->NbrP3Tri = GmfStatKwd(InpMsh, GmfTrianglesP3);
  Obj->P3Tri    = viz_reallocate(sizeof(int10) * (Obj->NbrP3Tri + 1), Obj->P3Tri);
  Obj->P3TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Tri + 1), Obj->P3TriRef);
  if (!Obj->P3Tri || !Obj->P3TriRef) {
    viz_writestatus(status, "Allocation failed for P3Triangle-arrays, size %lg Mb\n", sizeof(int10) * (Obj->NbrP3Tri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3TriMax = Obj->NbrP3Tri;
  // GmfGotoKwd(InpMsh, GmfTrianglesP3);
  GmfGetBlock(InpMsh, GmfTrianglesP3, 1, Obj->NbrP3Tri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 10, &Obj->P3Tri[1], &Obj->P3Tri[Obj->NbrP3Tri],
      typeOf[sizeof(int1)], &Obj->P3TriRef[1], &Obj->P3TriRef[Obj->NbrP3Tri]);

  /* Reading P4 triangles */
  Obj->NbrP4Tri = GmfStatKwd(InpMsh, GmfTrianglesP4);
  Obj->P4Tri    = viz_reallocate(sizeof(int15) * (Obj->NbrP4Tri + 1), Obj->P4Tri);
  Obj->P4TriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Tri + 1), Obj->P4TriRef);
  if (!Obj->P4Tri || !Obj->P4TriRef) {
    viz_writestatus(status, "Allocation failed for P4Triangle-arrays, size %lg Mb\n", sizeof(int15) * (Obj->NbrP4Tri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4TriMax = Obj->NbrP4Tri;
  // GmfGotoKwd(InpMsh, GmfTrianglesP4);
  GmfGetBlock(InpMsh, GmfTrianglesP4, 1, Obj->NbrP4Tri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 15, &Obj->P4Tri[1], &Obj->P4Tri[Obj->NbrP4Tri],
      typeOf[sizeof(int1)], &Obj->P4TriRef[1], &Obj->P4TriRef[Obj->NbrP4Tri]);

  /* Reading Q2 quads */
  Obj->NbrQ2Qua = GmfStatKwd(InpMsh, GmfQuadrilateralsQ2);
  Obj->Q2Qua    = viz_reallocate(sizeof(int9) * (Obj->NbrQ2Qua + 1), Obj->Q2Qua);
  Obj->Q2QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ2Qua + 1), Obj->Q2QuaRef);
  if (!Obj->Q2Qua || !Obj->Q2QuaRef) {
    viz_writestatus(status, "Allocation failed for Q2Quad-arrays, size %lg Mb\n", sizeof(int9) * (Obj->NbrQ2Qua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ2QuaMax = Obj->NbrQ2Qua;
  // GmfGotoKwd(InpMsh, GmfQuadrilateralsQ2);
  GmfGetBlock(InpMsh, GmfQuadrilateralsQ2, 1, Obj->NbrQ2Qua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 9, &Obj->Q2Qua[1], &Obj->Q2Qua[Obj->NbrQ2Qua],
      typeOf[sizeof(int1)], &Obj->Q2QuaRef[1], &Obj->Q2QuaRef[Obj->NbrQ2Qua]);

  /* Reading Q3 quads */
  Obj->NbrQ3Qua = GmfStatKwd(InpMsh, GmfQuadrilateralsQ3);
  Obj->Q3Qua    = viz_reallocate(sizeof(int16) * (Obj->NbrQ3Qua + 1), Obj->Q3Qua);
  Obj->Q3QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ3Qua + 1), Obj->Q3QuaRef);
  if (!Obj->Q3Qua || !Obj->Q3QuaRef) {
    viz_writestatus(status, "Allocation failed for Q3Quad-arrays, size %lg Mb\n", sizeof(int16) * (Obj->NbrQ3Qua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ3QuaMax = Obj->NbrQ3Qua;
  // GmfGotoKwd(InpMsh, GmfQuadrilateralsQ3);
  GmfGetBlock(InpMsh, GmfQuadrilateralsQ3, 1, Obj->NbrQ3Qua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 16, &Obj->Q3Qua[1], &Obj->Q3Qua[Obj->NbrQ3Qua],
      typeOf[sizeof(int1)], &Obj->Q3QuaRef[1], &Obj->Q3QuaRef[Obj->NbrQ3Qua]);

  /* Reading Q4 quads */
  Obj->NbrQ4Qua = GmfStatKwd(InpMsh, GmfQuadrilateralsQ4);
  Obj->Q4Qua    = viz_reallocate(sizeof(int25) * (Obj->NbrQ4Qua + 1), Obj->Q4Qua);
  Obj->Q4QuaRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ4Qua + 1), Obj->Q4QuaRef);
  if (!Obj->Q4Qua || !Obj->Q4QuaRef) {
    viz_writestatus(status, "Allocation failed for Q4Quad-arrays, size %lg Mb\n", sizeof(int25) * (Obj->NbrQ4Qua + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ4QuaMax = Obj->NbrQ4Qua;
  // GmfGotoKwd(InpMsh, GmfQuadrilateralsQ4);
  GmfGetBlock(InpMsh, GmfQuadrilateralsQ4, 1, Obj->NbrQ4Qua, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 25, &Obj->Q4Qua[1], &Obj->Q4Qua[Obj->NbrQ4Qua],
      typeOf[sizeof(int1)], &Obj->Q4QuaRef[1], &Obj->Q4QuaRef[Obj->NbrQ4Qua]);

  /* Reading P2 Tets */
  Obj->NbrP2Tet = GmfStatKwd(InpMsh, GmfTetrahedraP2);
  Obj->P2Tet    = viz_reallocate(sizeof(int10) * (Obj->NbrP2Tet + 1), Obj->P2Tet);
  Obj->P2TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Tet + 1), Obj->P2TetRef);
  if (!Obj->P2Tet || !Obj->P2TetRef) {
    viz_writestatus(status, "Allocation failed for P2Tet-arrays, size %lg Mb\n", sizeof(int10) * (Obj->NbrP2Tet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2TetMax = Obj->NbrP2Tet;
  // GmfGotoKwd(InpMsh, GmfTetrahedraP2);
  GmfGetBlock(InpMsh, GmfTetrahedraP2, 1, Obj->NbrP2Tet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 10, &Obj->P2Tet[1], &Obj->P2Tet[Obj->NbrP2Tet],
      typeOf[sizeof(int1)], &Obj->P2TetRef[1], &Obj->P2TetRef[Obj->NbrP2Tet]);

  /* Reading P3 Tets */
  Obj->NbrP3Tet = GmfStatKwd(InpMsh, GmfTetrahedraP3);
  Obj->P3Tet    = viz_reallocate(sizeof(int20) * (Obj->NbrP3Tet + 1), Obj->P3Tet);
  Obj->P3TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Tet + 1), Obj->P3TetRef);
  if (!Obj->P3Tet || !Obj->P3TetRef) {
    viz_writestatus(status, "Allocation failed for P3Tet-arrays, size %lg Mb\n", sizeof(int20) * (Obj->NbrP3Tet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3TetMax = Obj->NbrP3Tet;
  // GmfGotoKwd(InpMsh, GmfTetrahedraP3);
  GmfGetBlock(InpMsh, GmfTetrahedraP3, 1, Obj->NbrP3Tet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 20, &Obj->P3Tet[1], &Obj->P3Tet[Obj->NbrP3Tet],
      typeOf[sizeof(int1)], &Obj->P3TetRef[1], &Obj->P3TetRef[Obj->NbrP3Tet]);

  /* Reading P4 Tets */
  Obj->NbrP4Tet = GmfStatKwd(InpMsh, GmfTetrahedraP4);
  Obj->P4Tet    = viz_reallocate(sizeof(int35) * (Obj->NbrP4Tet + 1), Obj->P4Tet);
  Obj->P4TetRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Tet + 1), Obj->P4TetRef);
  if (!Obj->P4Tet || !Obj->P4TetRef) {
    viz_writestatus(status, "Allocation failed for P4Tet-arrays, size %lg Mb\n", sizeof(int35) * (Obj->NbrP4Tet + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4TetMax = Obj->NbrP4Tet;
  // GmfGotoKwd(InpMsh, GmfTetrahedraP4);
  GmfGetBlock(InpMsh, GmfTetrahedraP4, 1, Obj->NbrP4Tet, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 35, &Obj->P4Tet[1], &Obj->P4Tet[Obj->NbrP4Tet],
      typeOf[sizeof(int1)], &Obj->P4TetRef[1], &Obj->P4TetRef[Obj->NbrP4Tet]);

  /* Reading Q2 hexahedra */
  Obj->NbrQ2Hex = GmfStatKwd(InpMsh, GmfHexahedraQ2);
  Obj->Q2Hex    = viz_reallocate(sizeof(int27) * (Obj->NbrQ2Hex + 1), Obj->Q2Hex);
  Obj->Q2HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ2Hex + 1), Obj->Q2HexRef);
  if (!Obj->Q2Hex || !Obj->Q2HexRef) {
    viz_writestatus(status, "Allocation failed for Q2Hex-arrays, size %lg Mb\n", sizeof(int27) * (Obj->NbrQ2Hex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ2HexMax = Obj->NbrQ2Hex;
  // GmfGotoKwd(InpMsh,  GmfHexahedraQ2);
  GmfGetBlock(InpMsh, GmfHexahedraQ2, 1, Obj->NbrQ2Hex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 27, &Obj->Q2Hex[1], &Obj->Q2Hex[Obj->NbrQ2Hex],
      typeOf[sizeof(int1)], &Obj->Q2HexRef[1], &Obj->Q2HexRef[Obj->NbrQ2Hex]);

  /* Reading Q3 hexahedra */
  Obj->NbrQ3Hex = GmfStatKwd(InpMsh, GmfHexahedraQ3);
  Obj->Q3Hex    = viz_reallocate(sizeof(int64) * (Obj->NbrQ3Hex + 1), Obj->Q3Hex);
  Obj->Q3HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ3Hex + 1), Obj->Q3HexRef);
  if (!Obj->Q3Hex || !Obj->Q3HexRef) {
    viz_writestatus(status, "Allocation failed for Q3Hex-arrays, size %lg Mb\n", sizeof(int64) * (Obj->NbrQ3Hex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ3HexMax = Obj->NbrQ3Hex;
  // GmfGotoKwd(InpMsh,  GmfHexahedraQ3);
  GmfGetBlock(InpMsh, GmfHexahedraQ3, 1, Obj->NbrQ3Hex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 64, &Obj->Q3Hex[1], &Obj->Q3Hex[Obj->NbrQ3Hex],
      typeOf[sizeof(int1)], &Obj->Q3HexRef[1], &Obj->Q3HexRef[Obj->NbrQ3Hex]);

  /* Reading Q4 hexahedra */
  Obj->NbrQ4Hex = GmfStatKwd(InpMsh, GmfHexahedraQ4);
  Obj->Q4Hex    = viz_reallocate(sizeof(int125) * (Obj->NbrQ4Hex + 1), Obj->Q4Hex);
  Obj->Q4HexRef = viz_reallocate(sizeof(int1) * (Obj->NbrQ4Hex + 1), Obj->Q4HexRef);
  if (!Obj->Q4Hex || !Obj->Q4HexRef) {
    viz_writestatus(status, "Allocation failed for Q4Hex-arrays, size %lg Mb\n", sizeof(int125) * (Obj->NbrQ4Hex + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrQ4HexMax = Obj->NbrQ4Hex;
  // GmfGotoKwd(InpMsh,  GmfHexahedraQ4);
  GmfGetBlock(InpMsh, GmfHexahedraQ4, 1, Obj->NbrQ4Hex, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 125, &Obj->Q4Hex[1], &Obj->Q4Hex[Obj->NbrQ4Hex],
      typeOf[sizeof(int1)], &Obj->Q4HexRef[1], &Obj->Q4HexRef[Obj->NbrQ4Hex]);

  /* Reading P2 Pyramids */
  Obj->NbrP2Pyr = GmfStatKwd(InpMsh, GmfPyramidsP2);
  Obj->P2Pyr    = viz_reallocate(sizeof(int14) * (Obj->NbrP2Pyr + 1), Obj->P2Pyr);
  Obj->P2PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Pyr + 1), Obj->P2PyrRef);
  if (!Obj->P2Pyr || !Obj->P2PyrRef) {
    viz_writestatus(status, "Allocation failed for P2Pyramid-arrays, size %lg Mb\n", sizeof(int14) * (Obj->NbrP2Pyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2PyrMax = Obj->NbrP2Pyr;
  // GmfGotoKwd(InpMsh,  GmfPyramidsP2);
  GmfGetBlock(InpMsh, GmfPyramidsP2, 1, Obj->NbrP2Pyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 14, &Obj->P2Pyr[1], &Obj->P2Pyr[Obj->NbrP2Pyr],
      typeOf[sizeof(int1)], &Obj->P2PyrRef[1], &Obj->P2PyrRef[Obj->NbrP2Pyr]);

  /* Reading P3 Pyramids */
  Obj->NbrP3Pyr = GmfStatKwd(InpMsh, GmfPyramidsP3);
  Obj->P3Pyr    = viz_reallocate(sizeof(int30) * (Obj->NbrP3Pyr + 1), Obj->P3Pyr);
  Obj->P3PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Pyr + 1), Obj->P3PyrRef);
  if (!Obj->P3Pyr || !Obj->P3PyrRef) {
    viz_writestatus(status, "Allocation failed for P3Pyramid-arrays, size %lg Mb\n", sizeof(int30) * (Obj->NbrP3Pyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3PyrMax = Obj->NbrP3Pyr;
  // GmfGotoKwd(InpMsh,  GmfPyramidsP3);
  GmfGetBlock(InpMsh, GmfPyramidsP3, 1, Obj->NbrP3Pyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 30, &Obj->P3Pyr[1], &Obj->P3Pyr[Obj->NbrP3Pyr],
      typeOf[sizeof(int1)], &Obj->P3PyrRef[1], &Obj->P3PyrRef[Obj->NbrP3Pyr]);

  /* Reading P4 Pyramids */
  Obj->NbrP4Pyr = GmfStatKwd(InpMsh, GmfPyramidsP4);
  Obj->P4Pyr    = viz_reallocate(sizeof(int55) * (Obj->NbrP4Pyr + 1), Obj->P4Pyr);
  Obj->P4PyrRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Pyr + 1), Obj->P4PyrRef);
  if (!Obj->P4Pyr || !Obj->P4PyrRef) {
    viz_writestatus(status, "Allocation failed for P4Pyramid-arrays, size %lg Mb\n", sizeof(int55) * (Obj->NbrP4Pyr + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4PyrMax = Obj->NbrP4Pyr;
  // GmfGotoKwd(InpMsh,  GmfPyramidsP4);
  GmfGetBlock(InpMsh, GmfPyramidsP4, 1, Obj->NbrP4Pyr, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 55, &Obj->P4Pyr[1], &Obj->P4Pyr[Obj->NbrP4Pyr],
      typeOf[sizeof(int1)], &Obj->P4PyrRef[1], &Obj->P4PyrRef[Obj->NbrP4Pyr]);

  /* Reading P2 Prisms */
  Obj->NbrP2Pri = GmfStatKwd(InpMsh, GmfPrismsP2);
  Obj->P2Pri    = viz_reallocate(sizeof(int18) * (Obj->NbrP2Pri + 1), Obj->P2Pri);
  Obj->P2PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP2Pri + 1), Obj->P2PriRef);
  if (!Obj->P2Pri || !Obj->P2PriRef) {
    viz_writestatus(status, "Allocation failed for P2Prism-arrays, size %lg Mb\n", sizeof(int18) * (Obj->NbrP2Pri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP2PriMax = Obj->NbrP2Pri;
  // GmfGotoKwd(InpMsh,  GmfPrismsP2);
  GmfGetBlock(InpMsh, GmfPrismsP2, 1, Obj->NbrP2Pri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 18, &Obj->P2Pri[1], &Obj->P2Pri[Obj->NbrP2Pri],
      typeOf[sizeof(int1)], &Obj->P2PriRef[1], &Obj->P2PriRef[Obj->NbrP2Pri]);

  /* Reading P3 Prisms */
  Obj->NbrP3Pri = GmfStatKwd(InpMsh, GmfPrismsP3);
  Obj->P3Pri    = viz_reallocate(sizeof(int40) * (Obj->NbrP3Pri + 1), Obj->P3Pri);
  Obj->P3PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP3Pri + 1), Obj->P3PriRef);
  if (!Obj->P3Pri || !Obj->P3PriRef) {
    viz_writestatus(status, "Allocation failed for P3Prism-arrays, size %lg Mb\n", sizeof(int40) * (Obj->NbrP3Pri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP3PriMax = Obj->NbrP3Pri;
  // GmfGotoKwd(InpMsh,  GmfPrismsP3);
  GmfGetBlock(InpMsh, GmfPrismsP3, 1, Obj->NbrP3Pri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 40, &Obj->P3Pri[1], &Obj->P3Pri[Obj->NbrP3Pri],
      typeOf[sizeof(int1)], &Obj->P3PriRef[1], &Obj->P3PriRef[Obj->NbrP3Pri]);

  /* Reading P4 Prisms */
  Obj->NbrP4Pri = GmfStatKwd(InpMsh, GmfPrismsP4);
  Obj->P4Pri    = viz_reallocate(sizeof(int75) * (Obj->NbrP4Pri + 1), Obj->P4Pri);
  Obj->P4PriRef = viz_reallocate(sizeof(int1) * (Obj->NbrP4Pri + 1), Obj->P4PriRef);
  if (!Obj->P4Pri || !Obj->P4PriRef) {
    viz_writestatus(status, "Allocation failed for P4Prism-arrays, size %lg Mb\n", sizeof(int75) * (Obj->NbrP4Pri + 1) / (1024. * 1024.));
    return VIZINT_ERROR;
  }
  Obj->NbrP4PriMax = Obj->NbrP4Pri;
  // GmfGotoKwd(InpMsh,  GmfPrismsP4);
  GmfGetBlock(InpMsh, GmfPrismsP4, 1, Obj->NbrP4Pri, 0, NULL, NULL,
      typeOfTab[sizeof(int1)], 75, &Obj->P4Pri[1], &Obj->P4Pri[Obj->NbrP4Pri],
      typeOf[sizeof(int1)], &Obj->P4PriRef[1], &Obj->P4PriRef[Obj->NbrP4Pri]);

  /* Close current mesh */
  GmfCloseMesh(InpMsh);

  return VIZINT_SUCCESS;
}

int1* uti_String2Array(const char* str, int* size)
{
  int        i;
  const char delimiters[] = " ,";

  *size = 0;

  // printf(" string: %s \n",str);
  if (strlen(str) <= 0)
    return NULL;

  char* token = NULL;
  char* sub   = NULL;

  UtiTuple* tuple = uti_NewTuple();

  char* string = strdup(str);
  token        = mystrsep(&string, delimiters);

  while (token != NULL) {
    // printf(" token %s %d\n",token,strlen(token));
    if (strlen(token) > 0) {
      int beg = strtol(token, &sub, 10);
      int end = beg;
      if (sub != NULL) {
        if (sub[0] != '\0' && strlen(sub) >= 1) {
          // printf(" sub %s \n",sub);
          end = strtol(sub + 1, NULL, 10);
        }
      }
      // printf(" Add beg-end %d %d \n",beg,end);
      for (i = beg; i <= end; i++)
        tuple = uti_AddTuple(i, tuple);
    }
    token = mystrsep(&string, delimiters);
  }

  // printf(" end \n");

  int1* array = uti_GetTupleArray(tuple, size);

  tuple = uti_FreeTuple(tuple);
  free(string);

  // for(i=0; i<*size; i++) printf(" %d ",array[i]);
  // printf("\n");

  return array;
}

UtiTuple* uti_NewTuple()
{
  UtiTuple* tup = malloc(sizeof(UtiTuple));
  if (!tup)
    return NULL;
  tup->size = 0;
  tup->nxt  = NULL;
  return tup;
}

/* add en entity and return the tuple in which the object has been added */
UtiTuple* uti_AddTuple(int1 iEnt, UtiTuple* head)
{
  UtiTuple* tup = NULL;
  if (head == NULL) {
    tup                   = uti_NewTuple();
    tup->tuple[tup->size] = iEnt;
    tup->size++;
    tup->nxt = NULL;
    return tup;
  }
  else if (head->size >= UTI_TUPLE_MAX_SIZE) {
    tup                   = uti_NewTuple();
    tup->tuple[tup->size] = iEnt;
    tup->size++;
    tup->nxt = head;
    return tup;
  }
  else {
    head->tuple[head->size] = iEnt;
    head->size++;
    return head;
  }
}

UtiTuple* uti_FreeTuple(UtiTuple* head)
{
  UtiTuple* tup = head;
  UtiTuple* nxt = NULL;
  while (tup != NULL) {
    nxt = tup->nxt;
    free(tup);
    tup = NULL;
    tup = nxt;
  }
  return NULL;
}

int uti_GetTupleSize(UtiTuple* head)
{
  int       size = 0;
  UtiTuple* tup  = head;
  while (tup != NULL) {
    size += tup->size;
    tup = tup->nxt;
  }
  return size;
}

int1* uti_GetTupleArray(UtiTuple* head, int* size)
{
  int i;
  *size = uti_GetTupleSize(head);
  if (*size <= 0)
    return NULL;

  int1*     lst = malloc(sizeof(int1) * (*size));
  UtiTuple* tup = head;
  int       idx = *size - 1;
  while (tup != NULL) {
    for (i = tup->size - 1; i >= 0; i--) {
      lst[idx--] = tup->tuple[i];
    }
    tup = tup->nxt;
  }

  return lst;
}

char* mystrsep(char** stringp, const char* delim)
{
  char* start = *stringp;
  char* p;

  p = (start != NULL) ? strpbrk(start, delim) : NULL;

  if (p == NULL) {
    *stringp = NULL;
  }
  else {
    *p       = '\0';
    *stringp = p + 1;
  }

  return start;
}

int viz_OpenSolution(VizObject* iObj, const char* SolFile, VizIntStatus* status)
{
  int ierr;

  //--- redirect error signals
#if defined(__linux__) || defined(__APPLE__)
  signal(SIGILL, readSolErr);
  signal(SIGFPE, readSolErr);
  signal(SIGBUS, readSolErr);
  signal(SIGSEGV, readSolErr);
  signal(SIGQUIT, readSolErr);
#endif

  ierr = viz_OpenSolutionLibMeshb(iObj, SolFile, status);
  //--- reset default error signals
#if defined(__linux__) || defined(__APPLE__)
  signal(SIGILL, SIG_DFL);
  signal(SIGFPE, SIG_DFL);
  signal(SIGBUS, SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
  signal(SIGQUIT, SIG_DFL);
#endif
  return ierr;
}

int viz_OpenSolutionLibMeshb(VizObject* Obj, const char* SolFile, VizIntStatus* status)
{
  char    bufNam[1024], noMshNam[1024];
  int     SolSiz, NbrTyp, TypTab[GmfMaxTyp + 1], Dim, NbrSol, Ite, Deg, NbrNod, NbrNodRef;
  double  Tim;
  float   flt;
  int     FilVer;
  int64_t InpSol;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!SolFile) {
    viz_writestatus(status, "NULL (Solution Name) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status) parameter on input\n");
    return VIZINT_ERROR;
  }

  sprintf(noMshNam, "%s", SolFile);
  if (strstr(SolFile, ".mesh") != NULL)
    noMshNam[strstr(SolFile, ".mesh") - SolFile] = '\0'; // check if it has the extension .mesh[b] and remove it if it is the case

  InpSol = GmfOpenMesh(noMshNam, GmfRead, &FilVer, &Dim);

  if (InpSol <= 0) {
    if (strstr(noMshNam, ".sol") == NULL) {
      sprintf(bufNam, "%s.solb", noMshNam);
      InpSol = GmfOpenMesh(bufNam, GmfRead, &FilVer, &Dim);
      if (InpSol <= 0) {
        sprintf(bufNam, "%s.sol", noMshNam);
        InpSol = GmfOpenMesh(bufNam, GmfRead, &FilVer, &Dim);
        if (InpSol <= 0) {
          viz_writestatus(status, "Cannot open solution file %s.[sol[b]]\n", noMshNam);
          return VIZINT_ERROR;
        }
      }
      viz_writeinfo(status, "%s", bufNam);
    }
    else {
      viz_writestatus(status, "Cannot open solution file %s.[sol[b]]\n", noMshNam);
      return VIZINT_ERROR;
    }
  }
  else
    viz_writeinfo(status, "%s", noMshNam);

  if (InpSol == 0) {
    viz_writestatus(status, "Cannot open solution file %s\n", noMshNam);
    return VIZINT_ERROR;
  }
  if (Dim != Obj->Dim) {
    viz_writestatus(status, "Invalid solution dimension %d should be %d \n", Dim, Obj->Dim);
    return VIZINT_ERROR;
  }

  if (GmfStatKwd(InpSol, GmfTime)) {
    GmfGotoKwd(InpSol, GmfTime);
    if (FilVer == GmfFloat) { // read 32 bits float
      GmfGetLin(InpSol, GmfTime, &flt);
      Tim = (double)flt;
    }
    else if (FilVer == GmfDouble) // read 64 bits float
      GmfGetLin(InpSol, GmfTime, &Tim);
  }
  else
    Tim = 0.;

  if (GmfStatKwd(InpSol, GmfIterations)) {
    GmfGotoKwd(InpSol, GmfIterations);
    GmfGetLin(InpSol, GmfIterations, &Ite);
  }
  else
    Ite = 0;

  // Reading sol at vertices
  NbrSol = GmfStatKwd(InpSol, GmfSolAtVertices, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrVer) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtVertices \n", NbrSol, Obj->NbrVer);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) { //--- by convention solatvertices is of degree 0
    Obj->CrdSol = viz_ReadSolutionLibMeshb(InpSol, Obj->CrdSol, FilVer, GmfSolAtVertices, NbrTyp, SolSiz, TypTab, Obj->NbrVer, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  // Reading sol at Edges
  NbrSol = GmfStatKwd(InpSol, GmfSolAtEdges, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtEdges \n", NbrSol, Obj->NbrTet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->EdgSol, FilVer, GmfSolAtEdges, NbrTyp, SolSiz, TypTab, Obj->NbrEdg, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrEdg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP1 \n", NbrSol, Obj->NbrEdg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->EdgSol, FilVer, GmfHOSolAtEdgesP1, NbrTyp, SolSiz, TypTab, Obj->NbrEdg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP1NodesPositions, Obj->EdgSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Edg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP2 \n", NbrSol, Obj->NbrP2Edg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2EdgSol, FilVer, GmfHOSolAtEdgesP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Edg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP2NodesPositions, Obj->P2EdgSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Edg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP3 \n", NbrSol, Obj->NbrP3Edg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3EdgSol, FilVer, GmfHOSolAtEdgesP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Edg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP3NodesPositions, Obj->P3EdgSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtEdgesP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Edg) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtEdgesP4 \n", NbrSol, Obj->NbrP4Edg);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4EdgSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4EdgSol, FilVer, GmfHOSolAtEdgesP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Edg, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtEdgesP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtEdgesP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtEdgesP4NodesPositions, Obj->P4EdgSol);
  }

  // Reading sol at triangles
  NbrSol = GmfStatKwd(InpSol, GmfSolAtTriangles, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtTriangles \n", NbrSol, Obj->NbrTri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TriSol, FilVer, GmfSolAtTriangles, NbrTyp, SolSiz, TypTab, Obj->NbrTri, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP1 \n", NbrSol, Obj->NbrTri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TriSol, FilVer, GmfHOSolAtTrianglesP1, NbrTyp, SolSiz, TypTab, Obj->NbrTri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP1NodesPositions, Obj->TriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Tri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP2 \n", NbrSol, Obj->NbrP2Tri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2TriSol, FilVer, GmfHOSolAtTrianglesP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Tri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP2NodesPositions, Obj->P2TriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Tri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP3 \n", NbrSol, Obj->NbrP3Tri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3TriSol, FilVer, GmfHOSolAtTrianglesP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Tri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP3NodesPositions, Obj->P3TriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Tri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTrianglesP4 \n", NbrSol, Obj->NbrP4Tri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4TriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4TriSol, FilVer, GmfHOSolAtTrianglesP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Tri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTrianglesP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTrianglesP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTrianglesP4NodesPositions, Obj->P4TriSol);
  }

  // Reading sol at Quadrilaterals
  NbrSol = GmfStatKwd(InpSol, GmfSolAtQuadrilaterals, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtQuadrilaterals \n", NbrSol, Obj->NbrQua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->QuaSol, FilVer, GmfSolAtQuadrilaterals, NbrTyp, SolSiz, TypTab, Obj->NbrQua, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ1 \n", NbrSol, Obj->NbrQua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ1, NbrTyp, SolSiz, TypTab, Obj->NbrQua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ1NodesPositions, Obj->QuaSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ2Qua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ2 \n", NbrSol, Obj->NbrQ2Qua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q2QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q2QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ2, NbrTyp, SolSiz, TypTab, Obj->NbrQ2Qua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ2NodesPositions, Obj->Q2QuaSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ3Qua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ3 \n", NbrSol, Obj->NbrQ3Qua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q3QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q3QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ3, NbrTyp, SolSiz, TypTab, Obj->NbrQ3Qua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ3NodesPositions, Obj->Q3QuaSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ4Qua) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtQuadrilateralsQ4 \n", NbrSol, Obj->NbrQ4Qua);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q4QuaSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q4QuaSol, FilVer, GmfHOSolAtQuadrilateralsQ4, NbrTyp, SolSiz, TypTab, Obj->NbrQ4Qua, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtQuadrilateralsQ4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtQuadrilateralsQ4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtQuadrilateralsQ4NodesPositions, Obj->Q4QuaSol);
  }

  // Reading sol at tetrahedra
  NbrSol = GmfStatKwd(InpSol, GmfSolAtTetrahedra, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtTetrahedra \n", NbrSol, Obj->NbrTet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TetSol, FilVer, GmfSolAtTetrahedra, NbrTyp, SolSiz, TypTab, Obj->NbrTet, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrTet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP1 \n", NbrSol, Obj->NbrTet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->TetSol, FilVer, GmfHOSolAtTetrahedraP1, NbrTyp, SolSiz, TypTab, Obj->NbrTet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP1NodesPositions, Obj->TetSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Tet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP2 \n", NbrSol, Obj->NbrP2Tet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2TetSol, FilVer, GmfHOSolAtTetrahedraP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Tet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP2NodesPositions, Obj->P2TetSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Tet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP3 \n", NbrSol, Obj->NbrP3Tet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3TetSol, FilVer, GmfHOSolAtTetrahedraP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Tet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP3NodesPositions, Obj->P3TetSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Tet) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtTetrahedraP4 \n", NbrSol, Obj->NbrP4Tet);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4TetSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4TetSol, FilVer, GmfHOSolAtTetrahedraP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Tet, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtTetrahedraP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtTetrahedraP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtTetrahedraP4NodesPositions, Obj->P4TetSol);
  }

  // Reading sol at Pyramids
  NbrSol = GmfStatKwd(InpSol, GmfSolAtPyramids, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtPyramids \n", NbrSol, Obj->NbrPyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PyrSol, FilVer, GmfSolAtPyramids, NbrTyp, SolSiz, TypTab, Obj->NbrPyr, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP1 \n", NbrSol, Obj->NbrPyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PyrSol, FilVer, GmfHOSolAtPyramidsP1, NbrTyp, SolSiz, TypTab, Obj->NbrPyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP1NodesPositions, Obj->PyrSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Pyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP2 \n", NbrSol, Obj->NbrP2Pyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2PyrSol, FilVer, GmfHOSolAtPyramidsP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Pyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP2NodesPositions, Obj->P2PyrSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Pyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP3 \n", NbrSol, Obj->NbrP3Pyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3PyrSol, FilVer, GmfHOSolAtPyramidsP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Pyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP3NodesPositions, Obj->P3PyrSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Pyr) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPyramidsP4 \n", NbrSol, Obj->NbrP4Pyr);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4PyrSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4PyrSol, FilVer, GmfHOSolAtPyramidsP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Pyr, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPyramidsP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPyramidsP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPyramidsP4NodesPositions, Obj->P4PyrSol);
  }

  // Reading sol at Prisms
  NbrSol = GmfStatKwd(InpSol, GmfSolAtPrisms, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtPrisms \n", NbrSol, Obj->NbrPri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PriSol, FilVer, GmfSolAtPrisms, NbrTyp, SolSiz, TypTab, Obj->NbrPri, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrPri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP1 \n", NbrSol, Obj->NbrPri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->PriSol, FilVer, GmfHOSolAtPrismsP1, NbrTyp, SolSiz, TypTab, Obj->NbrPri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP1NodesPositions, Obj->PriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP2Pri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP2 \n", NbrSol, Obj->NbrP2Pri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P2PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P2PriSol, FilVer, GmfHOSolAtPrismsP2, NbrTyp, SolSiz, TypTab, Obj->NbrP2Pri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP2NodesPositions, Obj->P2PriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP3Pri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP3 \n", NbrSol, Obj->NbrP3Pri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P3PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P3PriSol, FilVer, GmfHOSolAtPrismsP3, NbrTyp, SolSiz, TypTab, Obj->NbrP3Pri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP3NodesPositions, Obj->P3PriSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtPrismsP4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrP4Pri) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtPrismsP4 \n", NbrSol, Obj->NbrP4Pri);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->P4PriSol = viz_ReadSolutionLibMeshb(InpSol, Obj->P4PriSol, FilVer, GmfHOSolAtPrismsP4, NbrTyp, SolSiz, TypTab, Obj->NbrP4Pri, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtPrismsP4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtPrismsP4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtPrismsP4NodesPositions, Obj->P4PriSol);
  }

  // Reading sol at Hexahedra
  NbrSol = GmfStatKwd(InpSol, GmfSolAtHexahedra, &NbrTyp, &SolSiz, TypTab);
  if ((NbrSol > 0) && NbrSol != Obj->NbrHex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for solAtHexahedra \n", NbrSol, Obj->NbrHex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->HexSol, FilVer, GmfSolAtHexahedra, NbrTyp, SolSiz, TypTab, Obj->NbrHex, 0, 1, Obj->Dim, Ite, Tim, status);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ1, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrHex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ1 \n", NbrSol, Obj->NbrHex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->HexSol, FilVer, GmfHOSolAtHexahedraQ1, NbrTyp, SolSiz, TypTab, Obj->NbrHex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ1NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ1NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ1NodesPositions, Obj->HexSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ2, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ2Hex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ2 \n", NbrSol, Obj->NbrQ2Hex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q2HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q2HexSol, FilVer, GmfHOSolAtHexahedraQ2, NbrTyp, SolSiz, TypTab, Obj->NbrQ2Hex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ2NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ2NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ2NodesPositions, Obj->Q2HexSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ3, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ3Hex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ3 \n", NbrSol, Obj->NbrQ3Hex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q3HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q3HexSol, FilVer, GmfHOSolAtHexahedraQ3, NbrTyp, SolSiz, TypTab, Obj->NbrQ3Hex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }
  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ3NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ3NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ3NodesPositions, Obj->Q3HexSol);
  }

  NbrSol = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ4, &NbrTyp, &SolSiz, TypTab, &Deg, &NbrNod);
  if ((NbrSol > 0) && NbrSol != Obj->NbrQ4Hex) {
    viz_writestatus(status, "Invalid number of solution  %d should be %d for HosolAtHexahedraQ4 \n", NbrSol, Obj->NbrQ4Hex);
    return VIZINT_ERROR;
  }
  else if (NbrSol > 0) {
    Obj->Q4HexSol = viz_ReadSolutionLibMeshb(InpSol, Obj->Q4HexSol, FilVer, GmfHOSolAtHexahedraQ4, NbrTyp, SolSiz, TypTab, Obj->NbrQ4Hex, Deg, NbrNod, Obj->Dim, Ite, Tim, status);
  }

  NbrNodRef = GmfStatKwd(InpSol, GmfHOSolAtHexahedraQ4NodesPositions);
  if ((NbrNodRef > 0) && NbrNodRef != NbrNod) {
    viz_writestatus(status, "Invalid number of solution reference nodes  %d should be %d for HosolAtHexahedraQ4NodesPositions \n", NbrNodRef, NbrNod);
    return VIZINT_ERROR;
  }
  else if (NbrNodRef > 0) {
    viz_ConvertHOSolutionInVizirBasis(InpSol, GmfHOSolAtHexahedraQ4NodesPositions, Obj->Q4HexSol);
  }

  viz_ReadSolutionsStrings(InpSol, Obj);

  // Close current mesh
  GmfCloseMesh(InpSol);

  return VIZINT_SUCCESS;
}

int viz_getIthSolInfo(solution* sol, int ith, int1* nbrEnt, int* SolAtEntTyp, int1* iter, double* tim, int1* deg, int1* nbrNod, double** solp, VizIntStatus* status)
{
  int       i    = 1;
  solution* csol = sol;
  if (!csol) {
    // printf("Invalid request to get a solution\n");
    return 2;
  }

  while (ith != i) {
    if (csol) {
      csol = csol->nxt;
      i++;
    }
    else {
      printf(" warning : unsucessfully attempting to access to solution %d\n", ith);
      break;
    }
  }
  if (csol) {
    *nbrEnt      = csol->NbrSol;
    *SolAtEntTyp = csol->SolTyp;
    *tim         = csol->Tim;
    *iter        = csol->Ite;
    *nbrNod      = csol->NbrNod;
    *deg         = csol->Deg;
    *solp        = csol->Sol;
    return 0;
  }
  else {
    // printf("Invalid request to get a solution\n");
    return 2;
  }
}

double* viz_GetSolutionsArray(VizObject* Obj, int SolTyp, int iSol, int1* nbrEnt, int1* SolAtEntTyp, int1* iter, double* tim, int1* deg, int1* nbrNod, VizIntStatus* status)
{

  *nbrEnt      = 0;
  *SolAtEntTyp = 0;
  *tim         = 0.;
  *deg         = 0;
  *nbrNod      = 0;

  //-- sol info
  double* sol = NULL;

  switch (SolTyp) {
    case VizAny:
      return NULL;
    case VizVerSol:
      viz_getIthSolInfo(Obj->CrdSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizTriSol:
      viz_getIthSolInfo(Obj->TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQuaSol:
      viz_getIthSolInfo(Obj->QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizTetSol:
      viz_getIthSolInfo(Obj->TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizPyrSol:
      viz_getIthSolInfo(Obj->PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizPriSol:
      viz_getIthSolInfo(Obj->PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizHexSol:
      viz_getIthSolInfo(Obj->HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2TriSol:
      viz_getIthSolInfo(Obj->P2TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ2QuaSol:
      viz_getIthSolInfo(Obj->Q2QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2TetSol:
      viz_getIthSolInfo(Obj->P2TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2PyrSol:
      viz_getIthSolInfo(Obj->P2PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP2PriSol:
      viz_getIthSolInfo(Obj->P2PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ2HexSol:
      viz_getIthSolInfo(Obj->Q2HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3TriSol:
      viz_getIthSolInfo(Obj->P3TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ3QuaSol:
      viz_getIthSolInfo(Obj->Q3QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3TetSol:
      viz_getIthSolInfo(Obj->P3TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3PyrSol:
      viz_getIthSolInfo(Obj->P3PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP3PriSol:
      viz_getIthSolInfo(Obj->P3PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ3HexSol:
      viz_getIthSolInfo(Obj->Q3HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4TriSol:
      viz_getIthSolInfo(Obj->P4TriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ4QuaSol:
      viz_getIthSolInfo(Obj->Q4QuaSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4TetSol:
      viz_getIthSolInfo(Obj->P4TetSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4PyrSol:
      viz_getIthSolInfo(Obj->P4PyrSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizP4PriSol:
      viz_getIthSolInfo(Obj->P4PriSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    case VizQ4HexSol:
      viz_getIthSolInfo(Obj->Q4HexSol, iSol, nbrEnt, SolAtEntTyp, iter, tim, deg, nbrNod, &sol, status);
      return sol;
    default:
      printf(" GetSolutionArray is undefined for Typ %d !\n", SolTyp);
      return NULL;
  }
}

int viz_GetSolutionFieldMax(VizObject* Obj)
{
  solution* sol;

  int FldMax = 0, Fmax = 0;

  if (Obj->CrdSol) {
    sol  = Obj->CrdSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }
  if (Obj->EdgSol) {
    sol  = Obj->EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P2EdgSol) {
    sol  = Obj->P2EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P3EdgSol) {
    sol  = Obj->P3EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P4EdgSol) {
    sol  = Obj->P4EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->TriSol) {
    sol  = Obj->TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P2TriSol) {
    sol  = Obj->P2TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P3TriSol) {
    sol  = Obj->P3TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P4TriSol) {
    sol  = Obj->P4TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->QuaSol) {
    sol  = Obj->QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->Q2QuaSol) {
    sol  = Obj->Q2QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->Q3QuaSol) {
    sol  = Obj->Q3QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->Q4QuaSol) {
    sol  = Obj->Q4QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->TetSol) {
    sol  = Obj->TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P2TetSol) {
    sol  = Obj->P2TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P3TetSol) {
    sol  = Obj->P3TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P4TetSol) {
    sol  = Obj->P4TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->PyrSol) {
    sol  = Obj->PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P2PyrSol) {
    sol  = Obj->P2PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P3PyrSol) {
    sol  = Obj->P3PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P4PyrSol) {
    sol  = Obj->P4PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->PriSol) {
    sol  = Obj->PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P2PriSol) {
    sol  = Obj->P2PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P3PriSol) {
    sol  = Obj->P3PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->P4PriSol) {
    sol  = Obj->P4PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->HexSol) {
    sol  = Obj->HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->Q2HexSol) {
    sol  = Obj->Q2HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->Q3HexSol) {
    sol  = Obj->Q3HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  if (Obj->Q4HexSol) {
    sol  = Obj->Q4HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      sol = sol->nxt;
    }
    FldMax = max(Fmax, FldMax);
  }

  return FldMax;
}

int viz_GetNumberOfFields(VizObject* Obj, const int SolTyp)
{
  solution* sol;

  int NbrFld = 0;

  switch (SolTyp) {
    case VizEdgSol:
      if (Obj->EdgSol) {
        sol = Obj->EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2EdgSol:
      if (Obj->P2EdgSol) {
        sol = Obj->P2EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3EdgSol:
      if (Obj->P3EdgSol) {
        sol = Obj->P3EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4EdgSol:
      if (Obj->P4EdgSol) {
        sol = Obj->P4EdgSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizTriSol:
      if (Obj->TriSol) {
        sol = Obj->TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2TriSol:
      if (Obj->P2TriSol) {
        sol = Obj->P2TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3TriSol:
      if (Obj->P3TriSol) {
        sol = Obj->P3TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4TriSol:
      if (Obj->P4TriSol) {
        sol = Obj->P4TriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQuaSol:
      if (Obj->QuaSol) {
        sol = Obj->QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ2QuaSol:
      if (Obj->Q2QuaSol) {
        sol = Obj->Q2QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ3QuaSol:
      if (Obj->Q3QuaSol) {
        sol = Obj->Q3QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ4QuaSol:
      if (Obj->Q4QuaSol) {
        sol = Obj->Q4QuaSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizTetSol:
      if (Obj->TetSol) {
        sol = Obj->TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2TetSol:
      if (Obj->P2TetSol) {
        sol = Obj->P2TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3TetSol:
      if (Obj->P3TetSol) {
        sol = Obj->P3TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4TetSol:
      if (Obj->P4TetSol) {
        sol = Obj->P4TetSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizPyrSol:
      if (Obj->PyrSol) {
        sol = Obj->PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2PyrSol:
      if (Obj->P2PyrSol) {
        sol = Obj->P2PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3PyrSol:
      if (Obj->P3PyrSol) {
        sol = Obj->P3PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4PyrSol:
      if (Obj->P4PyrSol) {
        sol = Obj->P4PyrSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizPriSol:
      if (Obj->PriSol) {
        sol = Obj->PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP2PriSol:
      if (Obj->P2PriSol) {
        sol = Obj->P2PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP3PriSol:
      if (Obj->P3PriSol) {
        sol = Obj->P3PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizP4PriSol:
      if (Obj->P4PriSol) {
        sol = Obj->P4PriSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizHexSol:
      if (Obj->HexSol) {
        sol = Obj->HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ2HexSol:
      if (Obj->Q2HexSol) {
        sol = Obj->Q2HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ3HexSol:
      if (Obj->Q3HexSol) {
        sol = Obj->Q3HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    case VizQ4HexSol:
      if (Obj->Q4HexSol) {
        sol = Obj->Q4HexSol;
        while (sol) {
          NbrFld++;
          sol = sol->nxt;
        }
      }
      break;

    default:
      printf("Warining: viz_GetNumberOfFields: case not considered");
      break;
  }

  return NbrFld;
}

int viz_PrintMeshInfo(VizObject* Obj, VizIntStatus* status)
{
  double bbox[6];
  int1   nbrEnt;
  int    dim;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (viz_GetDimension(Obj, &dim, status) == VIZINT_SUCCESS)
    printf("  Dimension %d  \n", dim);
  if (viz_GetNumberOfVertices(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Vertices           %12d \n", nbrEnt);
  if (viz_GetNumberOfCorners(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Corners            %12d \n", nbrEnt);
  if (viz_GetNumberOfEdges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Edges              %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Edges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Edges           %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Edges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Edges           %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Edges(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Edges           %12d \n", nbrEnt);
  if (viz_GetNumberOfTriangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Triangles          %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Triangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Triangles       %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Triangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Triangles       %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Triangles(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Triangles       %12d \n", nbrEnt);
  if (viz_GetNumberOfQuadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Quadrilaterals     %12d \n", nbrEnt);
  if (viz_GetNumberOfQ2Quadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q2-Quadrilaterals  %12d \n", nbrEnt);
  if (viz_GetNumberOfQ3Quadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q3-Quadrilaterals  %12d \n", nbrEnt);
  if (viz_GetNumberOfQ4Quadrilaterals(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q4-Quadrilaterals  %12d \n", nbrEnt);
  if (viz_GetNumberOfTetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Tetrahedra         %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Tetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Tetrahedra      %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Tetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Tetrahedra      %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Tetrahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Tetrahedra      %12d \n", nbrEnt);
  if (viz_GetNumberOfPrisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Prisms             %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Prisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Prisms          %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Prisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Prisms          %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Prisms(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Prisms          %12d \n", nbrEnt);
  if (viz_GetNumberOfHexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Hexahedra          %12d \n", nbrEnt);
  if (viz_GetNumberOfQ2Hexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q2-Hexahedra       %12d \n", nbrEnt);
  if (viz_GetNumberOfQ3Hexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q3-Hexahedra       %12d \n", nbrEnt);
  if (viz_GetNumberOfQ4Hexahedra(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Q4-Hexahedra       %12d \n", nbrEnt);
  if (viz_GetNumberOfPyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of Pyramids           %12d \n", nbrEnt);
  if (viz_GetNumberOfP2Pyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P2-Pyramids        %12d \n", nbrEnt);
  if (viz_GetNumberOfP3Pyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P3-Pyramids        %12d \n", nbrEnt);
  if (viz_GetNumberOfP4Pyramids(Obj, &nbrEnt, status) == VIZINT_SUCCESS)
    if (nbrEnt > 0)
      printf("  Number Of P4-Pyramids        %12d \n", nbrEnt);
  if (viz_GetMeshBoundingBox(Obj, bbox, status) == VIZINT_SUCCESS)
    printf("  x:[%lg %lg] y:[%lg %lg] z:[%lg %lg]\n", bbox[0], bbox[1], bbox[2], bbox[3], bbox[4], bbox[5]);

  printf("\n");
  return VIZINT_SUCCESS;
}

int viz_PrintSolutionInfo(VizObject* Obj, VizIntStatus* status)
{
  solution* sol;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (Obj->EdgSol) {
    sol = Obj->EdgSol;
    printf("  Degree of HOSolAtEdgesP1          %7d \n", sol->Deg);
  }

  if (Obj->P2EdgSol) {
    sol = Obj->P2EdgSol;
    printf("  Degree of HOSolAtEdgesP2          %7d \n", sol->Deg);
  }

  if (Obj->P3EdgSol) {
    sol = Obj->P3EdgSol;
    printf("  Degree of HOSolAtEdgesP3          %7d \n", sol->Deg);
  }

  if (Obj->P4EdgSol) {
    sol = Obj->P4EdgSol;
    printf("  Degree of HOSolAtEdgesP4          %7d \n", sol->Deg);
  }

  if (Obj->TriSol) {
    sol = Obj->TriSol;
    printf("  Degree of HOSolAtTrianglesP1      %7d \n", sol->Deg);
  }

  if (Obj->P2TriSol) {
    sol = Obj->P2TriSol;
    printf("  Degree of HOSolAtTrianglesP2      %7d \n", sol->Deg);
  }

  if (Obj->P3TriSol) {
    sol = Obj->P3TriSol;
    printf("  Degree of HOSolAtTrianglesP3      %7d \n", sol->Deg);
  }

  if (Obj->P4TriSol) {
    sol = Obj->P4TriSol;
    printf("  Degree of HOSolAtTrianglesP4      %7d \n", sol->Deg);
  }

  if (Obj->QuaSol) {
    sol = Obj->QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ1 %7d \n", sol->Deg);
  }

  if (Obj->Q2QuaSol) {
    sol = Obj->Q2QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ2 %7d \n", sol->Deg);
  }

  if (Obj->Q3QuaSol) {
    sol = Obj->Q3QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ3 %7d \n", sol->Deg);
  }

  if (Obj->Q4QuaSol) {
    sol = Obj->Q4QuaSol;
    printf("  Degree of HOSolAtQuadrilateralsQ4 %7d \n", sol->Deg);
  }

  if (Obj->TetSol) {
    sol = Obj->TetSol;
    printf("  Degree of HOSolAtTetrahedraP1     %7d \n", sol->Deg);
  }

  if (Obj->P2TetSol) {
    sol = Obj->P2TetSol;
    printf("  Degree of HOSolAtTetrahedraP2     %7d \n", sol->Deg);
  }

  if (Obj->P3TetSol) {
    sol = Obj->P3TetSol;
    printf("  Degree of HOSolAtTetrahedraP3     %7d \n", sol->Deg);
  }

  if (Obj->P4TetSol) {
    sol = Obj->P4TetSol;
    printf("  Degree of HOSolAtTetrahedraP4     %7d \n", sol->Deg);
  }

  if (Obj->PyrSol) {
    sol = Obj->PyrSol;
    printf("  Degree of HOSolAtPyramidsP1       %7d \n", sol->Deg);
  }

  if (Obj->P2PyrSol) {
    sol = Obj->P2PyrSol;
    printf("  Degree of HOSolAtPyramidsP2       %7d \n", sol->Deg);
  }

  if (Obj->P3PyrSol) {
    sol = Obj->P3PyrSol;
    printf("  Degree of HOSolAtPyramidsP3       %7d \n", sol->Deg);
  }

  if (Obj->P4PyrSol) {
    sol = Obj->P4PyrSol;
    printf("  Degree of HOSolAtPyramidsP4       %7d \n", sol->Deg);
  }

  if (Obj->PriSol) {
    sol = Obj->PriSol;
    printf("  Degree of HOSolAtPrismsP1         %7d \n", sol->Deg);
  }

  if (Obj->P2PriSol) {
    sol = Obj->P2PriSol;
    printf("  Degree of HOSolAtPrismsP2         %7d \n", sol->Deg);
  }

  if (Obj->P3PriSol) {
    sol = Obj->P3PriSol;
    printf("  Degree of HOSolAtPrismsP3         %7d \n", sol->Deg);
  }

  if (Obj->P4PriSol) {
    sol = Obj->P4PriSol;
    printf("  Degree of HOSolAtPrismsP4         %7d \n", sol->Deg);
  }

  if (Obj->HexSol) {
    sol = Obj->HexSol;
    printf("  Degree of HOSolAtHexahedraQ1      %7d \n", sol->Deg);
  }

  if (Obj->Q2HexSol) {
    sol = Obj->Q2HexSol;
    printf("  Degree of HOSolAtHexahedraQ2      %7d \n", sol->Deg);
  }

  if (Obj->Q3HexSol) {
    sol = Obj->Q3HexSol;
    printf("  Degree of HOSolAtHexahedraQ3      %7d \n", sol->Deg);
  }

  if (Obj->Q4HexSol) {
    sol = Obj->Q4HexSol;
    printf("  Degree of HOSolAtHexahedraQ4      %7d \n", sol->Deg);
  }

  printf("\n");
  return VIZINT_SUCCESS;
}

int viz_GetDimension(VizObject* Obj, int1* dim, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!dim) {
    viz_writestatus(status, "NULL (dim)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *dim = Obj->Dim;
  return VIZINT_SUCCESS;
}

int viz_GetMeshBoundingBox(VizObject* Obj, double* bbox, VizIntStatus* status)
{
  int iVer;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!bbox) {
    viz_writestatus(status, "NULL (bbox)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (Obj->NbrVer <= 0) {
    bbox[0] = bbox[1] = 0.0;
    bbox[2] = bbox[3] = 0.0;
    bbox[4] = bbox[5] = 0.0;
    return VIZINT_SUCCESS;
  }

  if (Obj->Crd) {
    bbox[0] = bbox[1] = Obj->Crd[1][0];
    bbox[2] = bbox[3] = Obj->Crd[1][1];
    bbox[4] = bbox[5] = Obj->Crd[1][2];

    for (iVer = 2; iVer <= Obj->NbrVer; iVer++) {
      bbox[0] = fmin(bbox[0], Obj->Crd[iVer][0]);
      bbox[1] = fmax(bbox[1], Obj->Crd[iVer][0]);
      bbox[2] = fmin(bbox[2], Obj->Crd[iVer][1]);
      bbox[3] = fmax(bbox[3], Obj->Crd[iVer][1]);
      bbox[4] = fmin(bbox[4], Obj->Crd[iVer][2]);
      bbox[5] = fmax(bbox[5], Obj->Crd[iVer][2]);
    }
  }
  else if (Obj->Crd_f) {
    bbox[0] = bbox[1] = Obj->Crd_f[1][0];
    bbox[2] = bbox[3] = Obj->Crd_f[1][1];
    bbox[4] = bbox[5] = Obj->Crd_f[1][2];

    for (iVer = 2; iVer <= Obj->NbrVer; iVer++) {
      bbox[0] = fmin(bbox[0], Obj->Crd_f[iVer][0]);
      bbox[1] = fmax(bbox[1], Obj->Crd_f[iVer][0]);
      bbox[2] = fmin(bbox[2], Obj->Crd_f[iVer][1]);
      bbox[3] = fmax(bbox[3], Obj->Crd_f[iVer][1]);
      bbox[4] = fmin(bbox[4], Obj->Crd_f[iVer][2]);
      bbox[5] = fmax(bbox[5], Obj->Crd_f[iVer][2]);
    }
  }

  return VIZINT_SUCCESS;
}

int viz_GetNumberOfVertices(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrVer;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfCorners(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrCrn;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfNormals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrNor;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfEdges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrEdg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Edges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Edg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Edges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Edg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Edges(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Edg;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfTriangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrTri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Triangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Tri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Triangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Tri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Triangles(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Tri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQuadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ2Quadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ2Qua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ3Quadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ3Qua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ4Quadrilaterals(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ4Qua;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfTetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrTet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Tetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Tet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Tetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Tet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Tetrahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Tet;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfPrisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrPri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP2Prisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Pri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP3Prisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Pri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfP4Prisms(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Pri;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfHexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrHex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ2Hexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ2Hex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ3Hexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ3Hex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfQ4Hexahedra(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrQ4Hex;
  return VIZINT_SUCCESS;
}
int viz_GetNumberOfPyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrPyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfP2Pyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP2Pyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfP3Pyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP3Pyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfP4Pyramids(VizObject* Obj, int1* nbrEnt, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!nbrEnt) {
    viz_writestatus(status, "NULL (nbrEnt)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }
  *nbrEnt = Obj->NbrP4Pyr;
  return VIZINT_SUCCESS;
}

int viz_GetNumberOfEntities(VizObject* Obj, int Typ, int1* nbrEnt, VizIntStatus* status)
{
  *nbrEnt = 0;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  switch (Typ) {
    case VizAny:
      return VIZINT_SUCCESS;
    case VizVer:
      return viz_GetNumberOfVertices(Obj, nbrEnt, status);
    case VizCrn:
      return viz_GetNumberOfCorners(Obj, nbrEnt, status);
    case VizEdg:
      return viz_GetNumberOfEdges(Obj, nbrEnt, status);
    case VizTri:
      return viz_GetNumberOfTriangles(Obj, nbrEnt, status);
    case VizQua:
      return viz_GetNumberOfQuadrilaterals(Obj, nbrEnt, status);
    case VizTet:
      return viz_GetNumberOfTetrahedra(Obj, nbrEnt, status);
    case VizPyr:
      return viz_GetNumberOfPyramids(Obj, nbrEnt, status);
    case VizPri:
      return viz_GetNumberOfPrisms(Obj, nbrEnt, status);
    case VizHex:
      return viz_GetNumberOfHexahedra(Obj, nbrEnt, status);
    case VizP2Edg:
      return viz_GetNumberOfP2Edges(Obj, nbrEnt, status);
    case VizP3Edg:
      return viz_GetNumberOfP3Edges(Obj, nbrEnt, status);
    case VizP4Edg:
      return viz_GetNumberOfP4Edges(Obj, nbrEnt, status);
    case VizP2Tri:
      return viz_GetNumberOfP2Triangles(Obj, nbrEnt, status);
    case VizP3Tri:
      return viz_GetNumberOfP3Triangles(Obj, nbrEnt, status);
    case VizP4Tri:
      return viz_GetNumberOfP4Triangles(Obj, nbrEnt, status);
    case VizQ2Qua:
      return viz_GetNumberOfQ2Quadrilaterals(Obj, nbrEnt, status);
    case VizQ3Qua:
      return viz_GetNumberOfQ3Quadrilaterals(Obj, nbrEnt, status);
    case VizQ4Qua:
      return viz_GetNumberOfQ4Quadrilaterals(Obj, nbrEnt, status);
    case VizP2Tet:
      return viz_GetNumberOfP2Tetrahedra(Obj, nbrEnt, status);
    case VizP3Tet:
      return viz_GetNumberOfP3Tetrahedra(Obj, nbrEnt, status);
    case VizP4Tet:
      return viz_GetNumberOfP4Tetrahedra(Obj, nbrEnt, status);
    case VizQ2Hex:
      return viz_GetNumberOfQ2Hexahedra(Obj, nbrEnt, status);
    case VizQ3Hex:
      return viz_GetNumberOfQ3Hexahedra(Obj, nbrEnt, status);
    case VizQ4Hex:
      return viz_GetNumberOfQ4Hexahedra(Obj, nbrEnt, status);
    case VizP2Pyr:
      return viz_GetNumberOfP2Pyramids(Obj, nbrEnt, status);
    case VizP3Pyr:
      return viz_GetNumberOfP3Pyramids(Obj, nbrEnt, status);
    case VizP4Pyr:
      return viz_GetNumberOfP4Pyramids(Obj, nbrEnt, status);
    case VizP2Pri:
      return viz_GetNumberOfP2Prisms(Obj, nbrEnt, status);
    case VizP3Pri:
      return viz_GetNumberOfP3Prisms(Obj, nbrEnt, status);
    case VizP4Pri:
      return viz_GetNumberOfP4Prisms(Obj, nbrEnt, status);
    default:
      viz_writestatus(status, "viz_GetNumberOfEntities is undefined for Typ %d !\n", Typ);
      return VIZINT_ERROR;
  }
}

int1* viz_GetReferenceArray(VizObject* Obj, int Typ, VizIntStatus* status)
{

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return NULL;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return NULL;
  }

  switch (Typ) {
    case VizAny:
      return NULL;
    case VizVer:
      return Obj->CrdRef;
    case VizCrn:
      return Obj->CrnRef;
    case VizEdg:
      return Obj->EdgRef;
    case VizTri:
      return Obj->TriRef;
    case VizQua:
      return Obj->QuaRef;
    case VizTet:
      return Obj->TetRef;
    case VizPyr:
      return Obj->PyrRef;
    case VizPri:
      return Obj->PriRef;
    case VizHex:
      return Obj->HexRef;
    case VizP2Edg:
      return Obj->P2EdgRef;
    case VizP3Edg:
      return Obj->P3EdgRef;
    case VizP4Edg:
      return Obj->P4EdgRef;
    case VizP2Tri:
      return Obj->P2TriRef;
    case VizP3Tri:
      return Obj->P3TriRef;
    case VizP4Tri:
      return Obj->P4TriRef;
    case VizQ2Qua:
      return Obj->Q2QuaRef;
    case VizQ3Qua:
      return Obj->Q3QuaRef;
    case VizQ4Qua:
      return Obj->Q4QuaRef;
    case VizP2Tet:
      return Obj->P2TetRef;
    case VizP3Tet:
      return Obj->P3TetRef;
    case VizP4Tet:
      return Obj->P4TetRef;
    case VizQ2Hex:
      return Obj->Q2HexRef;
    case VizQ3Hex:
      return Obj->Q3HexRef;
    case VizQ4Hex:
      return Obj->Q4HexRef;
    case VizP2Pyr:
      return Obj->P2PyrRef;
    case VizP3Pyr:
      return Obj->P3PyrRef;
    case VizP4Pyr:
      return Obj->P4PyrRef;
    case VizP2Pri:
      return Obj->P2PriRef;
    case VizP3Pri:
      return Obj->P3PriRef;
    case VizP4Pri:
      return Obj->P4PriRef;
    default:
      viz_writestatus(status, " viz_GetReferenceArray is undefined for Typ %d !\n", Typ);
      return NULL;
  }
}

int1* viz_GetIndicesArray(VizObject* Obj, int Typ, int1* nbrEnt, int1* sizEnt, VizIntStatus* status)
{

  *nbrEnt = 0;
  *sizEnt = 0;

  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return NULL;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return NULL;
  }

  switch (Typ) {
    case VizAny:
      return NULL;
    case VizVer:
      *nbrEnt = Obj->NbrVer;
      *sizEnt = 1;
      // int1* index = NULL;
      // int1* index = malloc((Obj->NbrVer + 1) * sizeof(int));
      // for (int i = 1; i <= Obj->NbrVer; i++) {
      //   index[i] = i;
      //   printf("index[%d] %d \n", i, index[i]);
      // }
      return (Obj->Crd) ? (int1*)Obj->Crd : (int1*)Obj->Crd_f;
    case VizCrn:
      *nbrEnt = Obj->NbrCrn;
      *sizEnt = 1;
      return (int1*)Obj->Crn;
    case VizNor:
      *nbrEnt = Obj->NbrNor;
      *sizEnt = 1;
      return (Obj->Nor_f) ? (int1*)Obj->Nor_f : (int1*)NULL;
    case VizEdg:
      *nbrEnt = Obj->NbrEdg;
      *sizEnt = 2;
      return (int1*)Obj->Edg;
    case VizTri:
      *nbrEnt = Obj->NbrTri;
      *sizEnt = 3;
      return (int1*)Obj->Tri;
    case VizQua:
      *nbrEnt = Obj->NbrQua;
      *sizEnt = 4;
      return (int1*)Obj->Qua;
    case VizTet:
      *nbrEnt = Obj->NbrTet;
      *sizEnt = 4;
      return (int1*)Obj->Tet;
    case VizPyr:
      *nbrEnt = Obj->NbrPyr;
      *sizEnt = 5;
      return (int1*)Obj->Pyr;
    case VizPri:
      *nbrEnt = Obj->NbrPri;
      *sizEnt = 6;
      return (int1*)Obj->Pri;
    case VizHex:
      *nbrEnt = Obj->NbrHex;
      *sizEnt = 8;
      return (int1*)Obj->Hex;
    case VizP2Edg:
      *nbrEnt = Obj->NbrP2Edg;
      *sizEnt = 3;
      return (int1*)Obj->P2Edg;
    case VizP3Edg:
      *nbrEnt = Obj->NbrP3Edg;
      *sizEnt = 4;
      return (int1*)Obj->P3Edg;
    case VizP4Edg:
      *nbrEnt = Obj->NbrP4Edg;
      *sizEnt = 5;
      return (int1*)Obj->P4Edg;
    case VizP2Tri:
      *nbrEnt = Obj->NbrP2Tri;
      *sizEnt = 6;
      return (int1*)Obj->P2Tri;
    case VizP3Tri:
      *nbrEnt = Obj->NbrP3Tri;
      *sizEnt = 10;
      return (int1*)Obj->P3Tri;
    case VizP4Tri:
      *nbrEnt = Obj->NbrP4Tri;
      *sizEnt = 15;
      return (int1*)Obj->P4Tri;
    case VizQ2Qua:
      *nbrEnt = Obj->NbrQ2Qua;
      *sizEnt = 9;
      return (int1*)Obj->Q2Qua;
    case VizQ3Qua:
      *nbrEnt = Obj->NbrQ3Qua;
      *sizEnt = 16;
      return (int1*)Obj->Q3Qua;
    case VizQ4Qua:
      *nbrEnt = Obj->NbrQ4Qua;
      *sizEnt = 25;
      return (int1*)Obj->Q4Qua;
    case VizP2Tet:
      *nbrEnt = Obj->NbrP2Tet;
      *sizEnt = 10;
      return (int1*)Obj->P2Tet;
    case VizP3Tet:
      *nbrEnt = Obj->NbrP3Tet;
      *sizEnt = 20;
      return (int1*)Obj->P3Tet;
    case VizP4Tet:
      *nbrEnt = Obj->NbrP4Tet;
      *sizEnt = 35;
      return (int1*)Obj->P4Tet;
    case VizQ2Hex:
      *nbrEnt = Obj->NbrQ2Hex;
      *sizEnt = 27;
      return (int1*)Obj->Q2Hex;
    case VizQ3Hex:
      *nbrEnt = Obj->NbrQ3Hex;
      *sizEnt = 64;
      return (int1*)Obj->Q3Hex;
    case VizQ4Hex:
      *nbrEnt = Obj->NbrQ4Hex;
      *sizEnt = 125;
      return (int1*)Obj->Q4Hex;
    case VizP2Pyr:
      *nbrEnt = Obj->NbrP2Pyr;
      *sizEnt = 14;
      return (int1*)Obj->P2Pyr;
    case VizP3Pyr:
      *nbrEnt = Obj->NbrP3Pyr;
      *sizEnt = 30;
      return (int1*)Obj->P3Pyr;
    case VizP4Pyr:
      *nbrEnt = Obj->NbrP4Pyr;
      *sizEnt = 55;
      return (int1*)Obj->P4Pyr;
    case VizP2Pri:
      *nbrEnt = Obj->NbrP2Pri;
      *sizEnt = 18;
      return (int1*)Obj->P2Pri;
    case VizP3Pri:
      *nbrEnt = Obj->NbrP3Pri;
      *sizEnt = 40;
      return (int1*)Obj->P3Pri;
    case VizP4Pri:
      *nbrEnt = Obj->NbrP4Pri;
      *sizEnt = 75;
      return (int1*)Obj->P4Pri;
    default:
      viz_writestatus(status, " viz_GetIndicesArray is undefined for Typ %d !\n", Typ);
      return NULL;
  }
}

int viz_WriteMesh(VizObject* iObj, const char* MshFile, VizIntStatus* status)
{
  return viz_WriteMeshLibMeshb(iObj, MshFile, status);
}

int viz_WriteMeshLibMeshb(VizObject* Obj, const char* MshFile, VizIntStatus* status)
{
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!MshFile) {
    viz_writestatus(status, "NULL (MshFile)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  int FilVer = 3;
  if (sizeof(int1) == 8)
    FilVer = 4;

  int64_t OutMsh = GmfOpenMesh(MshFile, GmfWrite, FilVer, Obj->Dim);
  if (OutMsh == 0) {
    viz_writestatus(status, "Cannot open mesh file %s\n", MshFile);
    return VIZINT_ERROR;
  }
  else
    viz_writeinfo(status, "%s", MshFile);

  // set vertices
  GmfSetKwd(OutMsh, GmfVertices, Obj->NbrVer);
  GmfSetBlock(OutMsh, GmfVertices, 1, Obj->NbrVer, 0, NULL, NULL,
      GmfDouble, &(Obj->Crd[1][0]), &(Obj->Crd[Obj->NbrVer][0]),
      GmfDouble, &(Obj->Crd[1][1]), &(Obj->Crd[Obj->NbrVer][1]),
      GmfDouble, &(Obj->Crd[1][2]), &(Obj->Crd[Obj->NbrVer][2]),
      typeOf[sizeof(int1)], &Obj->CrdRef[1], &Obj->CrdRef[Obj->NbrVer]);

  // set edges
  if (Obj->NbrEdg) {
    GmfSetKwd(OutMsh, GmfEdges, Obj->NbrEdg);
    GmfSetBlock(OutMsh, GmfEdges, 1, Obj->NbrEdg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Obj->Edg[1], &Obj->Edg[Obj->NbrEdg],
        typeOf[sizeof(int1)], &Obj->EdgRef[1], &Obj->EdgRef[Obj->NbrEdg]);

    GmfSetKwd(OutMsh, GmfEdgesP1Ordering, 2);
    GmfSetBlock(OutMsh, GmfEdgesP1Ordering, 1, 2, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P1EdgesOrdering[0], &P1EdgesOrdering[1]);
  }

  if (Obj->NbrP2Edg) {
    GmfSetKwd(OutMsh, GmfEdgesP2, Obj->NbrP2Edg);
    GmfSetBlock(OutMsh, GmfEdgesP2, 1, Obj->NbrP2Edg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Obj->P2Edg[1], &Obj->P2Edg[Obj->NbrP2Edg],
        typeOf[sizeof(int1)], &Obj->P2EdgRef[1], &Obj->P2EdgRef[Obj->NbrP2Edg]);

    GmfSetKwd(OutMsh, GmfEdgesP2Ordering, 3);
    GmfSetBlock(OutMsh, GmfEdgesP2Ordering, 1, 3, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P2EdgesOrdering[0], &P2EdgesOrdering[2]);
  }

  if (Obj->NbrP3Edg) {
    GmfSetKwd(OutMsh, GmfEdgesP3, Obj->NbrP3Edg);
    GmfSetBlock(OutMsh, GmfEdgesP3, 1, Obj->NbrP3Edg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &Obj->P3Edg[1], &Obj->P3Edg[Obj->NbrP3Edg],
        typeOf[sizeof(int1)], &Obj->P3EdgRef[1], &Obj->P3EdgRef[Obj->NbrP3Edg]);

    GmfSetKwd(OutMsh, GmfEdgesP3Ordering, 4);
    GmfSetBlock(OutMsh, GmfEdgesP3Ordering, 1, 4, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P3EdgesOrdering[0], &P3EdgesOrdering[3]);
  }

  if (Obj->NbrP4Edg) {
    GmfSetKwd(OutMsh, GmfEdgesP4, Obj->NbrP4Edg);
    GmfSetBlock(OutMsh, GmfEdgesP4, 1, Obj->NbrP4Edg, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 5, &Obj->P4Edg[1], &Obj->P4Edg[Obj->NbrP4Edg],
        typeOf[sizeof(int1)], &Obj->P4EdgRef[1], &Obj->P4EdgRef[Obj->NbrP4Edg]);

    GmfSetKwd(OutMsh, GmfEdgesP4Ordering, 5);
    GmfSetBlock(OutMsh, GmfEdgesP4Ordering, 1, 5, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &P4EdgesOrdering[0], &P4EdgesOrdering[4]);
  }

  // set triangles
  if (Obj->NbrTri) {
    GmfSetKwd(OutMsh, GmfTriangles, Obj->NbrTri);
    GmfSetBlock(OutMsh, GmfTriangles, 1, Obj->NbrTri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Obj->Tri[1], &Obj->Tri[Obj->NbrTri],
        typeOf[sizeof(int1)], &Obj->TriRef[1], &Obj->TriRef[Obj->NbrTri]);

    GmfSetKwd(OutMsh, GmfTrianglesP1Ordering, 3);
    GmfSetBlock(OutMsh, GmfTrianglesP1Ordering, 1, 3, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P1TrianglesOrdering[0], &P1TrianglesOrdering[2]);
  }

  if (Obj->NbrP2Tri) {
    GmfSetKwd(OutMsh, GmfTrianglesP2, Obj->NbrP2Tri);
    GmfSetBlock(OutMsh, GmfTrianglesP2, 1, Obj->NbrP2Tri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 6, &Obj->P2Tri[1], &Obj->P2Tri[Obj->NbrP2Tri],
        typeOf[sizeof(int1)], &Obj->P2TriRef[1], &Obj->P2TriRef[Obj->NbrP2Tri]);

    GmfSetKwd(OutMsh, GmfTrianglesP2Ordering, 6);
    GmfSetBlock(OutMsh, GmfTrianglesP2Ordering, 1, 6, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P2TrianglesOrdering[0], &P2TrianglesOrdering[5]);
  }

  if (Obj->NbrP3Tri) {
    GmfSetKwd(OutMsh, GmfTrianglesP3, Obj->NbrP3Tri);
    GmfSetBlock(OutMsh, GmfTrianglesP3, 1, Obj->NbrP3Tri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 10, &Obj->P3Tri[1], &Obj->P3Tri[Obj->NbrP3Tri],
        typeOf[sizeof(int1)], &Obj->P3TriRef[1], &Obj->P3TriRef[Obj->NbrP3Tri]);

    GmfSetKwd(OutMsh, GmfTrianglesP3Ordering, 10);
    GmfSetBlock(OutMsh, GmfTrianglesP3Ordering, 1, 10, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P3TrianglesOrdering[0], &P3TrianglesOrdering[9]);
  }

  if (Obj->NbrP4Tri) {
    GmfSetKwd(OutMsh, GmfTrianglesP4, Obj->NbrP4Tri);
    GmfSetBlock(OutMsh, GmfTrianglesP4, 1, Obj->NbrP4Tri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 15, &Obj->P4Tri[1], &Obj->P4Tri[Obj->NbrP4Tri],
        typeOf[sizeof(int1)], &Obj->P4TriRef[1], &Obj->P4TriRef[Obj->NbrP4Tri]);

    GmfSetKwd(OutMsh, GmfTrianglesP4Ordering, 15);
    GmfSetBlock(OutMsh, GmfTrianglesP4Ordering, 1, 15, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P4TrianglesOrdering[0], &P4TrianglesOrdering[14]);
  }

  // set quads
  if (Obj->NbrQua) {
    GmfSetKwd(OutMsh, GmfQuadrilaterals, Obj->NbrQua);
    GmfSetBlock(OutMsh, GmfQuadrilaterals, 1, Obj->NbrQua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &Obj->Qua[1], &Obj->Qua[Obj->NbrQua],
        typeOf[sizeof(int1)], &Obj->QuaRef[1], &Obj->QuaRef[Obj->NbrQua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ1Ordering, 4);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ1Ordering, 1, 4, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q1QuadrilateralsOrdering[0], &Q1QuadrilateralsOrdering[3]);
  }

  if (Obj->NbrQ2Qua) {
    GmfSetKwd(OutMsh, GmfQuadrilateralsQ2, Obj->NbrQ2Qua);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ2, 1, Obj->NbrQ2Qua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 9, &Obj->Q2Qua[1], &Obj->Q2Qua[Obj->NbrQ2Qua],
        typeOf[sizeof(int1)], &Obj->Q2QuaRef[1], &Obj->Q2QuaRef[Obj->NbrQ2Qua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ2Ordering, 9);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ2Ordering, 1, 9, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q2QuadrilateralsOrdering[0], &Q2QuadrilateralsOrdering[8]);
  }

  if (Obj->NbrQ3Qua) {
    GmfSetKwd(OutMsh, GmfQuadrilateralsQ3, Obj->NbrQ3Qua);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ3, 1, Obj->NbrQ3Qua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 16, &Obj->Q3Qua[1], &Obj->Q3Qua[Obj->NbrQ3Qua],
        typeOf[sizeof(int1)], &Obj->Q3QuaRef[1], &Obj->Q3QuaRef[Obj->NbrQ3Qua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ3Ordering, 16);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ3Ordering, 1, 16, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q3QuadrilateralsOrdering[0], &Q3QuadrilateralsOrdering[15]);
  }

  if (Obj->NbrQ4Qua) {
    GmfSetKwd(OutMsh, GmfQuadrilateralsQ4, Obj->NbrQ4Qua);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ4, 1, Obj->NbrQ4Qua, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 25, &Obj->Q4Qua[1], &Obj->Q4Qua[Obj->NbrQ4Qua],
        typeOf[sizeof(int1)], &Obj->Q4QuaRef[1], &Obj->Q4QuaRef[Obj->NbrQ4Qua]);

    GmfSetKwd(OutMsh, GmfQuadrilateralsQ4Ordering, 25);
    GmfSetBlock(OutMsh, GmfQuadrilateralsQ4Ordering, 1, 25, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 2, &Q4QuadrilateralsOrdering[0], &Q4QuadrilateralsOrdering[24]);
  }

  // set tets
  if (Obj->NbrTet) {
    GmfSetKwd(OutMsh, GmfTetrahedra, Obj->NbrTet);
    GmfSetBlock(OutMsh, GmfTetrahedra, 1, Obj->NbrTet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &Obj->Tet[1], &Obj->Tet[Obj->NbrTet],
        typeOf[sizeof(int1)], &Obj->TetRef[1], &Obj->TetRef[Obj->NbrTet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP1Ordering, 4);
    GmfSetBlock(OutMsh, GmfTetrahedraP1Ordering, 1, 4, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P1TetrahedraOrdering[0], &P1TetrahedraOrdering[3]);
  }

  if (Obj->NbrP2Tet) {
    GmfSetKwd(OutMsh, GmfTetrahedraP2, Obj->NbrP2Tet);
    GmfSetBlock(OutMsh, GmfTetrahedraP2, 1, Obj->NbrP2Tet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 10, &Obj->P2Tet[1], &Obj->P2Tet[Obj->NbrP2Tet],
        typeOf[sizeof(int1)], &Obj->P2TetRef[1], &Obj->P2TetRef[Obj->NbrP2Tet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP2Ordering, 10);
    GmfSetBlock(OutMsh, GmfTetrahedraP2Ordering, 1, 10, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P2TetrahedraOrdering[0], &P2TetrahedraOrdering[9]);
  }

  if (Obj->NbrP3Tet) {
    GmfSetKwd(OutMsh, GmfTetrahedraP3, Obj->NbrP3Tet);
    GmfSetBlock(OutMsh, GmfTetrahedraP3, 1, Obj->NbrP3Tet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 20, &Obj->P3Tet[1], &Obj->P3Tet[Obj->NbrP3Tet],
        typeOf[sizeof(int1)], &Obj->P3TetRef[1], &Obj->P3TetRef[Obj->NbrP3Tet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP3Ordering, 20);
    GmfSetBlock(OutMsh, GmfTetrahedraP3Ordering, 1, 20, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P3TetrahedraOrdering[0], &P3TetrahedraOrdering[19]);
  }

  if (Obj->NbrP4Tet) {
    GmfSetKwd(OutMsh, GmfTetrahedraP4, Obj->NbrP4Tet);
    GmfSetBlock(OutMsh, GmfTetrahedraP4, 1, Obj->NbrP4Tet, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 35, &Obj->P4Tet[1], &Obj->P4Tet[Obj->NbrP4Tet],
        typeOf[sizeof(int1)], &Obj->P4TetRef[1], &Obj->P4TetRef[Obj->NbrP4Tet]);

    GmfSetKwd(OutMsh, GmfTetrahedraP4Ordering, 35);
    GmfSetBlock(OutMsh, GmfTetrahedraP4Ordering, 1, 35, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P4TetrahedraOrdering[0], &P4TetrahedraOrdering[34]);
  }

  // set pyrs
  if (Obj->NbrPyr) {
    GmfSetKwd(OutMsh, GmfPyramids, Obj->NbrPyr);
    GmfSetBlock(OutMsh, GmfPyramids, 1, Obj->NbrPyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 5, &Obj->Pyr[1], &Obj->Pyr[Obj->NbrPyr],
        typeOf[sizeof(int1)], &Obj->PyrRef[1], &Obj->PyrRef[Obj->NbrPyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP1Ordering, 5);
    GmfSetBlock(OutMsh, GmfPyramidsP1Ordering, 1, 5, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P1PyramidsOrdering[0], &P1PyramidsOrdering[4]);
  }

  if (Obj->NbrP2Pyr) {
    GmfSetKwd(OutMsh, GmfPyramidsP2, Obj->NbrP2Pyr);
    GmfSetBlock(OutMsh, GmfPyramidsP2, 1, Obj->NbrP2Pyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 14, &Obj->P2Pyr[1], &Obj->P2Pyr[Obj->NbrP2Pyr],
        typeOf[sizeof(int1)], &Obj->P2PyrRef[1], &Obj->P2PyrRef[Obj->NbrP2Pyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP2Ordering, 14);
    GmfSetBlock(OutMsh, GmfPyramidsP2Ordering, 1, 14, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P2PyramidsOrdering[0], &P2PyramidsOrdering[13]);
  }

  if (Obj->NbrP3Pyr) {
    GmfSetKwd(OutMsh, GmfPyramidsP3, Obj->NbrP3Pyr);
    GmfSetBlock(OutMsh, GmfPyramidsP3, 1, Obj->NbrP3Pyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 30, &Obj->P3Pyr[1], &Obj->P3Pyr[Obj->NbrP3Pyr],
        typeOf[sizeof(int1)], &Obj->P3PyrRef[1], &Obj->P3PyrRef[Obj->NbrP3Pyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP3Ordering, 30);
    GmfSetBlock(OutMsh, GmfPyramidsP3Ordering, 1, 30, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P3PyramidsOrdering[0], &P3PyramidsOrdering[29]);
  }

  if (Obj->NbrP4Pyr) {
    GmfSetKwd(OutMsh, GmfPyramidsP4, Obj->NbrP4Pyr);
    GmfSetBlock(OutMsh, GmfPyramidsP4, 1, Obj->NbrP4Pyr, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 55, &Obj->P4Pyr[1], &Obj->P4Pyr[Obj->NbrP4Pyr],
        typeOf[sizeof(int1)], &Obj->P4PyrRef[1], &Obj->P4PyrRef[Obj->NbrP4Pyr]);

    GmfSetKwd(OutMsh, GmfPyramidsP4Ordering, 55);
    GmfSetBlock(OutMsh, GmfPyramidsP4Ordering, 1, 55, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &P4PyramidsOrdering[0], &P4PyramidsOrdering[54]);
  }

  // set prims
  if (Obj->NbrPri) {
    GmfSetKwd(OutMsh, GmfPrisms, Obj->NbrPri);
    GmfSetBlock(OutMsh, GmfPrisms, 1, Obj->NbrPri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 6, &Obj->Pri[1], &Obj->Pri[Obj->NbrPri],
        typeOf[sizeof(int1)], &Obj->PriRef[1], &Obj->PriRef[Obj->NbrPri]);

    GmfSetKwd(OutMsh, GmfPrismsP1Ordering, 6);
    GmfSetBlock(OutMsh, GmfPrismsP1Ordering, 1, 6, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P1PrismsOrdering[0], &P1PrismsOrdering[5]);
  }

  if (Obj->NbrP2Pri) {
    GmfSetKwd(OutMsh, GmfPrismsP2, Obj->NbrP2Pri);
    GmfSetBlock(OutMsh, GmfPrismsP2, 1, Obj->NbrP2Pri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 18, &Obj->P2Pri[1], &Obj->P2Pri[Obj->NbrP2Pri],
        typeOf[sizeof(int1)], &Obj->P2PriRef[1], &Obj->P2PriRef[Obj->NbrP2Pri]);

    GmfSetKwd(OutMsh, GmfPrismsP2Ordering, 18);
    GmfSetBlock(OutMsh, GmfPrismsP2Ordering, 1, 18, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P2PrismsOrdering[0], &P2PrismsOrdering[17]);
  }

  if (Obj->NbrP3Pri) {
    GmfSetKwd(OutMsh, GmfPrismsP3, Obj->NbrP3Pri);
    GmfSetBlock(OutMsh, GmfPrismsP3, 1, Obj->NbrP3Pri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 40, &Obj->P3Pri[1], &Obj->P3Pri[Obj->NbrP3Pri],
        typeOf[sizeof(int1)], &Obj->P3PriRef[1], &Obj->P3PriRef[Obj->NbrP3Pri]);

    GmfSetKwd(OutMsh, GmfPrismsP3Ordering, 40);
    GmfSetBlock(OutMsh, GmfPrismsP3Ordering, 1, 40, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P3PrismsOrdering[0], &P3PrismsOrdering[39]);
  }

  if (Obj->NbrP4Pri) {
    GmfSetKwd(OutMsh, GmfPrismsP4, Obj->NbrP4Pri);
    GmfSetBlock(OutMsh, GmfPrismsP4, 1, Obj->NbrP4Pri, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 75, &Obj->P4Pri[1], &Obj->P4Pri[Obj->NbrP4Pri],
        typeOf[sizeof(int1)], &Obj->P4PriRef[1], &Obj->P4PriRef[Obj->NbrP4Pri]);

    GmfSetKwd(OutMsh, GmfPrismsP4Ordering, 75);
    GmfSetBlock(OutMsh, GmfPrismsP4Ordering, 1, 75, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 4, &P4PrismsOrdering[0], &P4PrismsOrdering[74]);
  }

  // set hexes
  if (Obj->NbrHex) {
    GmfSetKwd(OutMsh, GmfHexahedra, Obj->NbrHex);
    GmfSetBlock(OutMsh, GmfHexahedra, 1, Obj->NbrHex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 8, &Obj->Hex[1], &Obj->Hex[Obj->NbrHex],
        typeOf[sizeof(int1)], &Obj->HexRef[1], &Obj->HexRef[Obj->NbrHex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ1Ordering, 8);
    GmfSetBlock(OutMsh, GmfHexahedraQ1Ordering, 1, 8, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q1HexahedraOrdering[0], &Q1HexahedraOrdering[7]);
  }

  if (Obj->NbrQ2Hex) {
    GmfSetKwd(OutMsh, GmfHexahedraQ2, Obj->NbrQ2Hex);
    GmfSetBlock(OutMsh, GmfHexahedraQ2, 1, Obj->NbrQ2Hex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 27, &Obj->Q2Hex[1], &Obj->Q2Hex[Obj->NbrQ2Hex],
        typeOf[sizeof(int1)], &Obj->Q2HexRef[1], &Obj->Q2HexRef[Obj->NbrQ2Hex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ2Ordering, 27);
    GmfSetBlock(OutMsh, GmfHexahedraQ2Ordering, 1, 27, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q2HexahedraOrdering[0], &Q2HexahedraOrdering[26]);
  }

  if (Obj->NbrQ3Hex) {
    GmfSetKwd(OutMsh, GmfHexahedraQ3, Obj->NbrQ3Hex);
    GmfSetBlock(OutMsh, GmfHexahedraQ3, 1, Obj->NbrQ3Hex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 64, &Obj->Q3Hex[1], &Obj->Q3Hex[Obj->NbrQ3Hex],
        typeOf[sizeof(int1)], &Obj->Q3HexRef[1], &Obj->Q3HexRef[Obj->NbrQ3Hex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ3Ordering, 64);
    GmfSetBlock(OutMsh, GmfHexahedraQ3Ordering, 1, 64, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q3HexahedraOrdering[0], &Q3HexahedraOrdering[63]);
  }

  if (Obj->NbrQ4Hex) {
    GmfSetKwd(OutMsh, GmfHexahedraQ4, Obj->NbrQ4Hex);
    GmfSetBlock(OutMsh, GmfHexahedraQ4, 1, Obj->NbrQ4Hex, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 125, &Obj->Q4Hex[1], &Obj->Q4Hex[Obj->NbrQ4Hex],
        typeOf[sizeof(int1)], &Obj->Q4HexRef[1], &Obj->Q4HexRef[Obj->NbrQ4Hex]);

    GmfSetKwd(OutMsh, GmfHexahedraQ4Ordering, 125);
    GmfSetBlock(OutMsh, GmfHexahedraQ4Ordering, 1, 125, 0, NULL, NULL,
        typeOfTab[sizeof(int1)], 3, &Q4HexahedraOrdering[0], &Q4HexahedraOrdering[124]);
  }

  /* Close current mesh */
  GmfCloseMesh(OutMsh);

  return VIZINT_SUCCESS;
}

double GetWallClock()
{
#ifdef WIN32
  struct __timeb64 tb;
  _ftime64(&tb);
  return ((double)tb.time + (double)tb.millitm / 1000.);
#else
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return (tp.tv_sec + tp.tv_usec / 1000000.);
#endif
}

int viz_WriteSolutionsStrings(int64_t OutSol, VizObject* iObj)
{
  int iFld, NbrFldNam;
  char(*FldNam)[2048];
  int* FldKwd;

  NbrFldNam = viz_CountNumberOfFieldNames(iObj);

  FldKwd = malloc(sizeof(int) * NbrFldNam);
  FldNam = malloc(sizeof(char) * 2018 * NbrFldNam);
  viz_GetSolutionFieldNamesAndKwd(iObj, FldNam, FldKwd);

  GmfSetKwd(OutSol, GmfReferenceStrings, NbrFldNam);
  for (iFld = 0; iFld < NbrFldNam; iFld++)
    GmfSetLin(OutSol, GmfReferenceStrings, FldKwd[iFld], iFld + 1, FldNam[iFld]);

  free(FldKwd);
  free(FldNam);

  return VIZINT_SUCCESS;
}

void viz_GetSolutionFieldNamesAndKwd(VizObject* Obj, char (*Nam)[2048], int* Kwd)
{
  solution* sol;

  int Fmax, cpt;

  Fmax = cpt = 0;

  if (!Nam || !Kwd)
    return;

  if (Obj->CrdSol) {
    sol  = Obj->CrdSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfSolAtVertices;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->EdgSol) {
    sol  = Obj->EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2EdgSol) {
    sol  = Obj->P2EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3EdgSol) {
    sol  = Obj->P3EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4EdgSol) {
    sol  = Obj->P4EdgSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtEdgesP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->TriSol) {
    sol  = Obj->TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2TriSol) {
    sol  = Obj->P2TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3TriSol) {
    sol  = Obj->P3TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4TriSol) {
    sol  = Obj->P4TriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTrianglesP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->QuaSol) {
    sol  = Obj->QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q2QuaSol) {
    sol  = Obj->Q2QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q3QuaSol) {
    sol  = Obj->Q3QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q4QuaSol) {
    sol  = Obj->Q4QuaSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtQuadrilateralsQ4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->TetSol) {
    sol  = Obj->TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2TetSol) {
    sol  = Obj->P2TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3TetSol) {
    sol  = Obj->P3TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4TetSol) {
    sol  = Obj->P4TetSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtTetrahedraP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->PyrSol) {
    sol  = Obj->PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2PyrSol) {
    sol  = Obj->P2PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3PyrSol) {
    sol  = Obj->P3PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4PyrSol) {
    sol  = Obj->P4PyrSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPyramidsP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->PriSol) {
    sol  = Obj->PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P2PriSol) {
    sol  = Obj->P2PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P3PriSol) {
    sol  = Obj->P3PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->P4PriSol) {
    sol  = Obj->P4PriSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtPrismsP4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->HexSol) {
    sol  = Obj->HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ1;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q2HexSol) {
    sol  = Obj->Q2HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ2;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q3HexSol) {
    sol  = Obj->Q3HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ3;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  if (Obj->Q4HexSol) {
    sol  = Obj->Q4HexSol;
    Fmax = 0;
    while (sol) {
      Fmax++;
      if (sol->Nam[0] != '\0') {
        sprintf(Nam[cpt], "%s", sol->Nam);
        Kwd[cpt] = GmfHOSolAtHexahedraQ4;
        cpt++;
      }
      sol = sol->nxt;
    }
  }

  return;
}

int viz_CountNumberOfFieldNames(VizObject* Obj)
{
  solution* sol;

  int cpt = 0;

  if (Obj->CrdSol) {
    sol = Obj->CrdSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->EdgSol) {
    sol = Obj->EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2EdgSol) {
    sol = Obj->P2EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3EdgSol) {
    sol = Obj->P3EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4EdgSol) {
    sol = Obj->P4EdgSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->TriSol) {
    sol = Obj->TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2TriSol) {
    sol = Obj->P2TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3TriSol) {
    sol = Obj->P3TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4TriSol) {
    sol = Obj->P4TriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->QuaSol) {
    sol = Obj->QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q2QuaSol) {
    sol = Obj->Q2QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q3QuaSol) {
    sol = Obj->Q3QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q4QuaSol) {
    sol = Obj->Q4QuaSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->TetSol) {
    sol = Obj->TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2TetSol) {
    sol = Obj->P2TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3TetSol) {
    sol = Obj->P3TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4TetSol) {
    sol = Obj->P4TetSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->PyrSol) {
    sol = Obj->PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2PyrSol) {
    sol = Obj->P2PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3PyrSol) {
    sol = Obj->P3PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4PyrSol) {
    sol = Obj->P4PyrSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->PriSol) {
    sol = Obj->PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P2PriSol) {
    sol = Obj->P2PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P3PriSol) {
    sol = Obj->P3PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->P4PriSol) {
    sol = Obj->P4PriSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->HexSol) {
    sol = Obj->HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q2HexSol) {
    sol = Obj->Q2HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q3HexSol) {
    sol = Obj->Q3HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  if (Obj->Q4HexSol) {
    sol = Obj->Q4HexSol;
    while (sol) {
      if (sol->Nam[0] != '\0')
        cpt++;
      sol = sol->nxt;
    }
  }

  return cpt;
}

int viz_WriteSolution(VizObject* Obj, const char* SolFile, int SolDeg, VizIntStatus* status)
{
  return viz_WriteSolutionLibMeshb(Obj, SolFile, SolDeg, status);
}

static int viz_TestWriteHOSol(int SolAtEnt, int SolDeg)
{
  if (((SolDeg == 0 && (SolAtEnt == GmfSolAtVertices || SolAtEnt == GmfSolAtEdges || SolAtEnt == GmfSolAtTriangles || SolAtEnt == GmfSolAtQuadrilaterals || SolAtEnt == GmfSolAtTetrahedra || SolAtEnt == GmfSolAtPyramids || SolAtEnt == GmfSolAtPrisms || SolAtEnt == GmfSolAtHexahedra))))
    return 0;
  else
    return 1;
}

static int viz_TestCompatibilitySolDegKwd(int SolDeg, int SolAtEnt)
{
  if (SolDeg == 0 && (SolAtEnt == GmfSolAtVertices || SolAtEnt == GmfSolAtEdges || SolAtEnt == GmfSolAtTriangles || SolAtEnt == GmfSolAtQuadrilaterals || SolAtEnt == GmfSolAtTetrahedra || SolAtEnt == GmfSolAtPyramids || SolAtEnt == GmfSolAtPrisms || SolAtEnt == GmfSolAtHexahedra))
    return 1;
  else if (SolDeg != 0 && (SolAtEnt == GmfHOSolAtEdgesP1 || SolAtEnt == GmfHOSolAtTrianglesP1 || SolAtEnt == GmfHOSolAtQuadrilateralsQ1 || SolAtEnt == GmfHOSolAtTetrahedraP1 || SolAtEnt == GmfHOSolAtPyramidsP1 || SolAtEnt == GmfHOSolAtPrismsP1 || SolAtEnt == GmfHOSolAtHexahedraQ1))
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP2 || SolAtEnt == GmfHOSolAtTrianglesP2 || SolAtEnt == GmfHOSolAtQuadrilateralsQ2 || SolAtEnt == GmfHOSolAtTetrahedraP2 || SolAtEnt == GmfHOSolAtPyramidsP2 || SolAtEnt == GmfHOSolAtPrismsP2 || SolAtEnt == GmfHOSolAtHexahedraQ2)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP3 || SolAtEnt == GmfHOSolAtTrianglesP3 || SolAtEnt == GmfHOSolAtQuadrilateralsQ3 || SolAtEnt == GmfHOSolAtTetrahedraP3 || SolAtEnt == GmfHOSolAtPyramidsP3 || SolAtEnt == GmfHOSolAtPrismsP3 || SolAtEnt == GmfHOSolAtHexahedraQ3)
    return 1;
  else if (SolAtEnt == GmfHOSolAtEdgesP4 || SolAtEnt == GmfHOSolAtTrianglesP4 || SolAtEnt == GmfHOSolAtQuadrilateralsQ4 || SolAtEnt == GmfHOSolAtTetrahedraP4 || SolAtEnt == GmfHOSolAtPyramidsP4 || SolAtEnt == GmfHOSolAtPrismsP4 || SolAtEnt == GmfHOSolAtHexahedraQ4)
    return 1;

  return 0;
}

static int viz_WriteSolAtEntLibMeshb(int64_t OutSol, int Dim, solution* sol, int SolAtEnt, int SolDeg, double* Tim, int* Ite, VizIntStatus* status)
{
  if (sol == NULL)
    return VIZINT_SUCCESS;

  if (OutSol == 0) {
    viz_writestatus(status, "Wrong OutSol     parameter on input\n");
    return VIZINT_ERROR;
  }
  if (SolDeg < 0) {
    viz_writestatus(status, "Wrong SolDeg     parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!Tim) {
    viz_writestatus(status, "NULL (Tim)       parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!Ite) {
    viz_writestatus(status, "NULL (Ite)       parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  if (viz_TestCompatibilitySolDegKwd(SolDeg, SolAtEnt) == 0) {
    viz_writestatus(status, "Mismatch of a solution of degree %d with solution keyword\n", SolDeg);
    return VIZINT_ERROR;
  }

  int     idx, iSolu, NbrSol, NbrNod, NbrEnt, SolSiz;
  int *   SolTyp, *TypSiz;
  double* dblBuf;
  float*  fltBuf;

  dblBuf = NULL;
  fltBuf = NULL;

  solution *cur, **Sol;

  //-- create the array of solutions after count
  cur    = sol;
  NbrSol = 0;
  NbrNod = -1;
  NbrEnt = -1;
  while (cur) {
    if (SolDeg == cur->Deg) {
      NbrSol++;
      if (NbrNod != -1 && NbrNod != sol->NbrNod) {
        viz_writestatus(status, "ERROR : Different number of nodes for solutions of same degree at the same element\n");
        return VIZINT_ERROR;
      }
      else
        NbrNod = sol->NbrNod;
      if (NbrEnt != -1 && NbrEnt != sol->NbrSol) {
        viz_writestatus(status, "ERROR : Different number of entities for solutions of same degree at the same element\n");
        return VIZINT_ERROR;
      }
      else
        NbrEnt = sol->NbrSol;
    }
    cur = cur->nxt;
  }
  if (NbrSol == 0)
    return VIZINT_SUCCESS;

  //-- alloc arrays and fill it;
  Sol    = malloc(NbrSol * sizeof(solution*));
  SolTyp = malloc(NbrSol * sizeof(int));
  TypSiz = malloc(NbrSol * sizeof(int));
  cur    = sol;
  iSolu  = 0;
  while (cur) {
    if (SolDeg == cur->Deg) {
      Sol[iSolu]    = cur;
      SolTyp[iSolu] = cur->SolTyp;
      (*Tim)        = max(*Tim, cur->Tim);
      (*Ite)        = max(*Ite, cur->Ite);
      iSolu++;
    }
    cur = cur->nxt;
  }

  //-- get solution size
  SolSiz = 0;
  for (int iSol = 0; iSol < NbrSol; iSol++) {
    switch (SolTyp[iSol]) {
      case GmfSca:
        TypSiz[iSol] = 1;
        break;
      case GmfVec:
        TypSiz[iSol] = (Dim == 2) ? 2 : 3;
        break;
      case GmfSymMat:
        TypSiz[iSol] = (Dim == 2) ? 3 : 6;
        break;
      case GmfMat:
        TypSiz[iSol] = (Dim == 2) ? 4 : 9;
        break;
    }
    SolSiz += TypSiz[iSol];
  }
  SolSiz *= NbrNod;

  //-- alloc buffer arrays
  dblBuf = malloc(SolSiz * sizeof(double));

  //-- set keywords
  if (viz_TestWriteHOSol(SolAtEnt, SolDeg))
    GmfSetKwd(OutSol, SolAtEnt, NbrEnt, NbrSol, SolTyp, SolDeg, NbrNod);
  else
    GmfSetKwd(OutSol, SolAtEnt, NbrEnt, NbrSol, SolTyp);

  //-- fill in solution arrays
  for (int iEnt = 1; iEnt <= NbrEnt; iEnt++) {
    idx = 0;

    for (int i = 0; i < NbrNod; i++) {
      for (int iSol = 0; iSol < NbrSol; iSol++) {
        for (int j = 0; j < TypSiz[iSol]; j++) {
          dblBuf[idx++] = Sol[iSol]->Sol[iEnt * TypSiz[iSol] * NbrNod + i * TypSiz[iSol] + j];
        }
      }
    }
    GmfSetLin(OutSol, SolAtEnt, dblBuf);
  }

  //-- free working arrays
  if (fltBuf) {
    free(fltBuf);
    fltBuf = NULL;
  }
  if (dblBuf) {
    free(dblBuf);
    dblBuf = NULL;
  }
  if (SolTyp) {
    free(SolTyp);
    SolTyp = NULL;
  }
  if (TypSiz) {
    free(TypSiz);
    TypSiz = NULL;
  }
  if (Sol) {
    free(Sol);
    Sol = NULL;
  }

  return VIZINT_SUCCESS;
}

int viz_GetSolutionDegMax(VizObject* Obj)
{
  solution* sol;

  int DegMax = 0;

  if (Obj->EdgSol) {
    sol = Obj->EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2EdgSol) {
    sol = Obj->P2EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3EdgSol) {
    sol = Obj->P3EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4EdgSol) {
    sol = Obj->P4EdgSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->TriSol) {
    sol = Obj->TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2TriSol) {
    sol = Obj->P2TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3TriSol) {
    sol = Obj->P3TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4TriSol) {
    sol = Obj->P4TriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->QuaSol) {
    sol = Obj->QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q2QuaSol) {
    sol = Obj->Q2QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q3QuaSol) {
    sol = Obj->Q3QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q4QuaSol) {
    sol = Obj->Q4QuaSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->TetSol) {
    sol = Obj->TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2TetSol) {
    sol = Obj->P2TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3TetSol) {
    sol = Obj->P3TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4TetSol) {
    sol = Obj->P4TetSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->PyrSol) {
    sol = Obj->PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2PyrSol) {
    sol = Obj->P2PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3PyrSol) {
    sol = Obj->P3PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4PyrSol) {
    sol = Obj->P4PyrSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->PriSol) {
    sol = Obj->PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P2PriSol) {
    sol = Obj->P2PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P3PriSol) {
    sol = Obj->P3PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->P4PriSol) {
    sol = Obj->P4PriSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->HexSol) {
    sol = Obj->HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q2HexSol) {
    sol = Obj->Q2HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q3HexSol) {
    sol = Obj->Q3HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  if (Obj->Q4HexSol) {
    sol = Obj->Q4HexSol;
    while (sol) {
      DegMax = max(sol->Deg, DegMax);
      sol    = sol->nxt;
    }
  }

  return DegMax;
}

/*
  Soldeg == -2  => display sol at vertices
  SolDeg == -1  => display all fields
  SolDeg >=  0  => display solution at elts at the given degree
*/

int viz_WriteSolutionLibMeshb(VizObject* Obj, const char* SolFile, int SolDeg, VizIntStatus* status)
{
  if (SolDeg < -2) {
    viz_writestatus(status, "Wrong SolDeg     parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!Obj) {
    viz_writestatus(status, "NULL (VizObject) parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!SolFile) {
    viz_writestatus(status, "NULL (SolFile)   parameter on input\n");
    return VIZINT_ERROR;
  }
  if (!status) {
    viz_writestatus(status, "NULL (status)    parameter on input\n");
    return VIZINT_ERROR;
  }

  int    Ite;
  double Tim;

  Ite = 0;
  Tim = 0.;

  int FilVer = (sizeof(int1) == 8) ? 4 : 3;

  int deg, degMax, ierr;

  int64_t OutSol = GmfOpenMesh(SolFile, GmfWrite, FilVer, Obj->Dim);

  if (OutSol == 0) {
    viz_writestatus(status, "Cannot open mesh file %s\n", SolFile);
    return VIZINT_ERROR;
  }
  else
    viz_writeinfo(status, "%s", SolFile);

  //-- write for each entity

  if (SolDeg == -2) //-- write solatvertices
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, 0, &Tim, &Ite, status);
  if (SolDeg == -1) { //-- write everything
    // if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
    //   return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfSolAtEdges, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfSolAtTriangles, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfSolAtQuadrilaterals, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfSolAtTetrahedra, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfSolAtPyramids, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfSolAtPrisms, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfSolAtHexahedra, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;
    if ((ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, 0, &Tim, &Ite, status)) != VIZINT_SUCCESS)
      return ierr;

    //-- get higher existing degree
    degMax = viz_GetSolutionDegMax(Obj);

    if (degMax >= 0) {
      for (deg = 0; deg <= degMax; deg++) {
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfHOSolAtEdgesP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2EdgSol, GmfHOSolAtEdgesP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3EdgSol, GmfHOSolAtEdgesP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4EdgSol, GmfHOSolAtEdgesP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfHOSolAtTrianglesP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TriSol, GmfHOSolAtTrianglesP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TriSol, GmfHOSolAtTrianglesP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TriSol, GmfHOSolAtTrianglesP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfHOSolAtQuadrilateralsQ1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2QuaSol, GmfHOSolAtQuadrilateralsQ2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3QuaSol, GmfHOSolAtQuadrilateralsQ3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4QuaSol, GmfHOSolAtQuadrilateralsQ4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfHOSolAtTetrahedraP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TetSol, GmfHOSolAtTetrahedraP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TetSol, GmfHOSolAtTetrahedraP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TetSol, GmfHOSolAtTetrahedraP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfHOSolAtPyramidsP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PyrSol, GmfHOSolAtPyramidsP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PyrSol, GmfHOSolAtPyramidsP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PyrSol, GmfHOSolAtPyramidsP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfHOSolAtPrismsP1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PriSol, GmfHOSolAtPrismsP2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PriSol, GmfHOSolAtPrismsP3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PriSol, GmfHOSolAtPrismsP4, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfHOSolAtHexahedraQ1, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2HexSol, GmfHOSolAtHexahedraQ2, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3HexSol, GmfHOSolAtHexahedraQ3, deg, &Tim, &Ite, status);
        ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4HexSol, GmfHOSolAtHexahedraQ4, deg, &Tim, &Ite, status);
      }
    }
  }
  else {
    if (SolDeg == 0) { //-- write degree 0 aka solatelt and solatvertices (degree 0 by convention)
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfSolAtEdges, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfSolAtTriangles, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfSolAtQuadrilaterals, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfSolAtTetrahedra, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfSolAtPyramids, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfSolAtPrisms, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfSolAtHexahedra, SolDeg, &Tim, &Ite, status);
      ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->CrdSol, GmfSolAtVertices, SolDeg, &Tim, &Ite, status);
    }

    //--- write degree > 1
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->EdgSol, GmfHOSolAtEdgesP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2EdgSol, GmfHOSolAtEdgesP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3EdgSol, GmfHOSolAtEdgesP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4EdgSol, GmfHOSolAtEdgesP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TriSol, GmfHOSolAtTrianglesP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TriSol, GmfHOSolAtTrianglesP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TriSol, GmfHOSolAtTrianglesP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TriSol, GmfHOSolAtTrianglesP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->QuaSol, GmfHOSolAtQuadrilateralsQ1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2QuaSol, GmfHOSolAtQuadrilateralsQ2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3QuaSol, GmfHOSolAtQuadrilateralsQ3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4QuaSol, GmfHOSolAtQuadrilateralsQ4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->TetSol, GmfHOSolAtTetrahedraP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2TetSol, GmfHOSolAtTetrahedraP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3TetSol, GmfHOSolAtTetrahedraP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4TetSol, GmfHOSolAtTetrahedraP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PyrSol, GmfHOSolAtPyramidsP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PyrSol, GmfHOSolAtPyramidsP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PyrSol, GmfHOSolAtPyramidsP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PyrSol, GmfHOSolAtPyramidsP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->PriSol, GmfHOSolAtPrismsP1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P2PriSol, GmfHOSolAtPrismsP2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P3PriSol, GmfHOSolAtPrismsP3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->P4PriSol, GmfHOSolAtPrismsP4, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->HexSol, GmfHOSolAtHexahedraQ1, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q2HexSol, GmfHOSolAtHexahedraQ2, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q3HexSol, GmfHOSolAtHexahedraQ3, SolDeg, &Tim, &Ite, status);
    ierr = viz_WriteSolAtEntLibMeshb(OutSol, Obj->Dim, Obj->Q4HexSol, GmfHOSolAtHexahedraQ4, SolDeg, &Tim, &Ite, status);
  }

  // Write iteration and time if there is some
  if (Ite != 0) {
    GmfSetKwd(OutSol, GmfIterations, 1);
    GmfSetLin(OutSol, GmfIterations, Ite);
  }

  if (Tim != 0.) {
    GmfSetKwd(OutSol, GmfTime, 1);
    GmfSetLin(OutSol, GmfTime, Tim);
  }

  viz_WriteSolutionsStrings(OutSol, Obj);

  // Close current mesh
  GmfCloseMesh(OutSol);

  return VIZINT_SUCCESS;
}