
#ifndef MSH_H
#define MSH_H

#include <math.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "numbering.h"

// To Read and Write  .mesh and .meshb file we use the
// library LibMeshb developed by Loic Maréchal - Gamma team INRIA
#include "libmeshb7.h"

typedef float  float3[3];
typedef double double2[2];
typedef double double3[3];
typedef double double4[4];
typedef double double6[6];

typedef int  int1;
typedef int1 int2[2];
typedef int1 int3[3];
typedef int1 int4[4];
typedef int1 int5[5];
typedef int1 int6[6];
typedef int1 int7[7];
typedef int1 int8[8];
typedef int1 int9[9];
typedef int1 int10[10];
typedef int1 int11[11];
typedef int1 int14[14];
typedef int1 int15[15];
typedef int1 int16[16];
typedef int1 int17[17];
typedef int1 int18[18];
typedef int1 int19[19];
typedef int1 int20[20];
typedef int1 int21[21];
typedef int1 int25[25];
typedef int1 int27[27];
typedef int1 int28[28];
typedef int1 int30[30];
typedef int1 int31[31];
typedef int1 int35[35];
typedef int1 int40[40];
typedef int1 int41[41];
typedef int1 int55[55];
typedef int1 int64[64];
typedef int1 int65[65];
typedef int1 int75[75];
typedef int1 int125[125];

extern const char* GmfKwdFmt[GmfMaxKwd + 1][3];

//--- definition of macro call (compatibility C/fortran)
#if defined(INC_F77)
#ifndef _UNDERSCORE_
#define call(x) x
#else
#define call(x) x##_
#endif
#endif

#define VIZINT_SUCCESS 0
#define VIZINT_NOTIMPLEMENTED 1 /* IHM should pass => no action */
#define VIZINT_ERROR 2          /* see status                   */

typedef struct viz_object   VizObject;
typedef struct viz_solution solution;

typedef struct VizInt_Status {
  char ErrMsg[512];
  char InfoMsg[512];

} VizIntStatus;

/* in the following, an entity refers to one of the following type */
enum VizTyp {
  VizAny,
  VizVer,
  VizCrn,
  VizEdg,
  VizTri,
  VizPat,
  VizQua,
  VizTet,
  VizPyr,
  VizPri,
  VizHex,
  VizP2Edg,
  VizP2Tri,
  VizQ2Qua,
  VizP2Tet,
  VizQ2Hex,
  VizP2Pyr,
  VizP2Pri,
  VizP3Edg,
  VizP3Tri,
  VizQ3Qua,
  VizP3Tet,
  VizQ3Hex,
  VizP3Pyr,
  VizP3Pri,
  VizP4Edg,
  VizP4Tri,
  VizQ4Qua,
  VizP4Tet,
  VizQ4Hex,
  VizP4Pyr,
  VizP4Pri,
  VizP5Edg,
  VizP5Tri,
  VizQ5Qua,
  VizP5Tet,
  VizQ5Hex,
  VizP5Pyr,
  VizP5Pri,
  VizRef,
  VizP6Edg,
  VizP6Tri,
  VizQ6Qua,
  VizP6Tet,
  VizQ6Hex,
  VizP6Pyr,
  VizP6Pri,
  VizP7Edg,
  VizP7Tri,
  VizQ7Qua,
  VizP7Tet,
  VizQ7Hex,
  VizP7Pyr,
  VizP7Pri,
  VizP8Edg,
  VizP8Tri,
  VizQ8Qua,
  VizP8Tet,
  VizQ8Hex,
  VizP8Pyr,
  VizP8Pri,
  VizP9Edg,
  VizP9Tri,
  VizQ9Qua,
  VizP9Tet,
  VizQ9Hex,
  VizP9Pyr,
  VizP9Pri,
  VizP10Edg,
  VizP10Tri,
  VizQ10Qua,
  VizP10Tet,
  VizQ10Hex,
  VizP10Pyr,
  VizP10Pri,
  VizCutTri,
  VizCutQua,
  VizCutP2Tri,
  VizCutQ2Qua,
  VizCutP3Tri,
  VizCutQ3Qua,
  VizCutP4Tri,
  VizCutQ4Qua,
  VizUsrCutTri,
  VizUsrCutQua,
  VizUsrCutP2Tri,
  VizUsrCutQ2Qua,
  VizUsrCutP3Tri,
  VizUsrCutQ3Qua,
  VizUsrCutP4Tri,
  VizUsrCutQ4Qua,
  VizSrfTri,
  VizSrfQua,
  VizSrfP2Tri,
  VizSrfQ2Qua,
  VizSrfP3Tri,
  VizSrfQ3Qua,
  VizSrfP4Tri,
  VizSrfQ4Qua,
  VizVerSol,
  VizEdgSol,
  VizTriSol,
  VizQuaSol,
  VizTetSol,
  VizPyrSol,
  VizPriSol,
  VizHexSol,
  VizP2EdgSol,
  VizP2TriSol,
  VizQ2QuaSol,
  VizP2TetSol,
  VizP2PyrSol,
  VizP2PriSol,
  VizQ2HexSol,
  VizP3EdgSol,
  VizP3TriSol,
  VizQ3QuaSol,
  VizP3TetSol,
  VizP3PyrSol,
  VizP3PriSol,
  VizQ3HexSol,
  VizP4EdgSol,
  VizP4TriSol,
  VizQ4QuaSol,
  VizP4TetSol,
  VizP4PyrSol,
  VizP4PriSol,
  VizQ4HexSol,
  VizNor,
  VizLast
};

#define UTI_TUPLE_MAX_SIZE 1024
typedef struct uti_tuple {
  int               size;
  int1              tuple[UTI_TUPLE_MAX_SIZE];
  struct uti_tuple* nxt;

} UtiTuple;

UtiTuple* uti_NewTuple();
UtiTuple* uti_FreeTuple(UtiTuple* head);
UtiTuple* uti_AddTuple(int1 iEnt, UtiTuple* head);
int       uti_GetTupleSize(UtiTuple* head);
int1*     uti_GetTupleArray(UtiTuple* head, int* size);

int1* uti_String2Array(const char* str, int* size);

char* mystrsep(char** stringp, const char* delim);

void readMshErr();
void readSolErr();

int viz_GetDimension(VizObject* iObj, int1* dim, VizIntStatus* status);          //--- return the dimension of the mesh (2 or 3)
int viz_GetMeshBoundingBox(VizObject* iObj, double* bbox, VizIntStatus* status); //--- return the bounding box in bbox [xmin xmax ymin ymax zmin zmax]

int viz_GetNumberOfEntities(VizObject* iObj, int Typ, int1* nbrEnt, VizIntStatus* status); //--- return the number of entities in nbrEnt, Typ is either VizVer, VizEdg, ...

int viz_GetNumberOfVertices(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfCorners(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfNormals(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfEdges(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfTriangles(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfQuadrilaterals(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfTetrahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfPrisms(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfHexahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfPyramids(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP2Edges(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP3Edges(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP4Edges(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP2Triangles(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP3Triangles(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP4Triangles(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfQ2Quadrilaterals(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfQ3Quadrilaterals(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfQ4Quadrilaterals(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP2Tetrahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP3Tetrahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP4Tetrahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfQ2Hexahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfQ3Hexahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfQ4Hexahedra(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP2Prisms(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP3Prisms(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP4Prisms(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP2Pyramids(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP3Pyramids(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);
int viz_GetNumberOfP4Pyramids(VizObject* iObj, int1* nbrEnt, VizIntStatus* status);

int viz_NewInterface(VizObject** iObj);  //--- initialize the vizir interface => iObj (pointer to the interface)
int viz_FreeInterface(VizObject** iObj); //--- free the interface handled by iObj

int viz_OpenMesh(VizObject* iObj, const char* MshFile, VizIntStatus* status);         //--- opens a mesh from a file (if a mesh exist, it is removed)
int viz_OpenMeshLibMeshb(VizObject* iObj, const char* MshFile, VizIntStatus* status); //--- opens a mesh from a file (if a mesh exist, it is removed)
int viz_OpenSolution(VizObject* iObj, const char* SolFile, VizIntStatus* status);
int viz_OpenSolutionLibMeshb(VizObject* iObj, const char* SolFile, VizIntStatus* status);

solution* viz_ReadSolution(int64_t InpSol, solution* sol, int FilVer, int SolAt, int NbrSol, int SolSiz, int* SolTyp, int NbrEnt, int Deg, int NbrNod, int Dim, int Ite, double Tim, VizIntStatus* status);
int       viz_getIthSolInfo(solution* sol, int ith, int1* nbrEnt, int* SolAtEntTyp, int1* iter, double* tim, int1* deg, int1* nbrNod, double** solp, VizIntStatus* status);
double*   viz_GetSolutionsArray(VizObject* Obj, int SolTyp, int iSol, int1* nbrEnt, int1* SolAtEntTyp, int1* iter, double* tim, int1* deg, int1* nbrNod, VizIntStatus* status);
int       viz_ReadSolutionsStrings(int64_t InpSol, VizObject* iObj);
int       viz_AttachSolutionName(VizObject* iObj, int LibMshKwd, int iSol, char* Nam);

int  viz_WriteSolution(VizObject* Obj, const char* SolFile, int SolDeg, VizIntStatus* status);         //--- writes the current solution stored in the VizObject
int  viz_WriteSolutionLibMeshb(VizObject* Obj, const char* SolFile, int SolDeg, VizIntStatus* status); //--- writes the current solution stored in the VizObject
int  viz_WriteSolutionsStrings(int64_t OutSol, VizObject* iObj);
int  viz_GetSolutionDegMax(VizObject* Obj);
int  viz_CountNumberOfFieldNames(VizObject* Obj);
void viz_GetSolutionFieldNamesAndKwd(VizObject* Obj, char (*Nam)[2048], int* Kwd);

int viz_PrintMeshInfo(VizObject* Obj, VizIntStatus* status);
int viz_PrintSolutionInfo(VizObject* Obj, VizIntStatus* status);

int viz_GetSolutionFieldMax(VizObject* Obj);                 //-- get the max number of fields solutions
int viz_GetNumberOfFields(VizObject* Obj, const int SolTyp); //-- get the number of fields solutions for a given type

int viz_WriteMesh(VizObject* iObj, const char* MshFile, VizIntStatus* status);         //--- writes the current mesh stored in the VizObject
int viz_WriteMeshLibMeshb(VizObject* iObj, const char* MshFile, VizIntStatus* status); //--- writes the current mesh stored in the VizObject

int1* viz_GetReferenceArray(VizObject* Obj, int Typ, VizIntStatus* status);
int1* viz_GetIndicesArray(VizObject* Obj, int Typ, int1* nbrEnt, int1* sizEnt, VizIntStatus* status);

double GetWallClock();

typedef long long int lint;

/* reallocate a sequence of arrays */
static inline void* viz_reallocate(lint size, void* pcur)
{
  if (pcur != NULL) {
    free(pcur);
    pcur = NULL;
  }
  return malloc(size);
}

#endif
