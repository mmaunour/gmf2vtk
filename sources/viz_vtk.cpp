/*

  gmf2vtk is a tool to do conversions between Gamma Mesh Format (see libMeshb) files and VTK files (ParaView)

  Written by Matthieu Maunoury, INRIA, 2020
  email: Matthieu.Maunoury@inria.fr

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
using namespace std;

#include <string>

#include "viz_vtk.h"
#include <datastruct.h>

int viz_WriteVTK(VizObject* iObj, const char* OutMshNam, bool ascii, int ierrsol)
{
  int          ithSol, nbrSol, nbrtmp, SolAtEntTyp, iter, deg, nbrNod, nbrNodSol;
  int          dim;
  double*      sol = NULL;
  double       tim;
  VizIntStatus status;

  int1 nbrVer   = 0;
  int1 nbrP4Edg = 0, nbrP3Edg = 0, nbrP2Edg = 0, nbrP1Edg = 0;
  int1 nbrP4Tri = 0, nbrP3Tri = 0, nbrP2Tri = 0, nbrP1Tri = 0;
  int1 nbrQ4Qua = 0, nbrQ3Qua = 0, nbrQ2Qua = 0, nbrQ1Qua = 0;
  int1 nbrTet = 0, nbrP2Tet = 0, nbrP3Tet = 0, nbrP4Tet = 0;
  int1 nbrPyr = 0, nbrP2Pyr = 0, nbrP3Pyr = 0, nbrP4Pyr = 0;
  int1 nbrPri = 0, nbrP2Pri = 0, nbrP3Pri = 0, nbrP4Pri = 0;
  int1 nbrHex = 0, nbrQ2Hex = 0, nbrQ3Hex = 0, nbrQ4Hex = 0;

  viz_GetDimension(iObj, &dim, &status);

  //---- Get number of vertices, common for everybody
  viz_GetNumberOfVertices(iObj, &nbrVer, &status);

  //-- count number of each entity
  viz_GetNumberOfEntities(iObj, VizEdg, &nbrP1Edg, &status);
  viz_GetNumberOfEntities(iObj, VizP2Edg, &nbrP2Edg, &status);
  viz_GetNumberOfEntities(iObj, VizP3Edg, &nbrP3Edg, &status);
  viz_GetNumberOfEntities(iObj, VizP4Edg, &nbrP4Edg, &status);

  viz_GetNumberOfEntities(iObj, VizTri, &nbrP1Tri, &status);
  viz_GetNumberOfEntities(iObj, VizP2Tri, &nbrP2Tri, &status);
  viz_GetNumberOfEntities(iObj, VizP3Tri, &nbrP3Tri, &status);
  viz_GetNumberOfEntities(iObj, VizP4Tri, &nbrP4Tri, &status);

  viz_GetNumberOfEntities(iObj, VizQua, &nbrQ1Qua, &status);
  viz_GetNumberOfEntities(iObj, VizQ2Qua, &nbrQ2Qua, &status);
  viz_GetNumberOfEntities(iObj, VizQ3Qua, &nbrQ3Qua, &status);
  viz_GetNumberOfEntities(iObj, VizQ4Qua, &nbrQ4Qua, &status);

  viz_GetNumberOfEntities(iObj, VizTet, &nbrTet, &status);
  viz_GetNumberOfEntities(iObj, VizP2Tet, &nbrP2Tet, &status);
  viz_GetNumberOfEntities(iObj, VizP3Tet, &nbrP3Tet, &status);
  viz_GetNumberOfEntities(iObj, VizP4Tet, &nbrP4Tet, &status);

  viz_GetNumberOfEntities(iObj, VizPri, &nbrPri, &status);
  viz_GetNumberOfEntities(iObj, VizP2Pri, &nbrP2Pri, &status);
  viz_GetNumberOfEntities(iObj, VizP3Pri, &nbrP3Pri, &status);
  viz_GetNumberOfEntities(iObj, VizP4Pri, &nbrP4Pri, &status);

  viz_GetNumberOfEntities(iObj, VizPyr, &nbrPyr, &status);
  viz_GetNumberOfEntities(iObj, VizP2Pyr, &nbrP2Pyr, &status);
  viz_GetNumberOfEntities(iObj, VizP3Pyr, &nbrP3Pyr, &status);
  viz_GetNumberOfEntities(iObj, VizP4Pyr, &nbrP4Pyr, &status);

  viz_GetNumberOfEntities(iObj, VizHex, &nbrHex, &status);
  viz_GetNumberOfEntities(iObj, VizQ2Hex, &nbrQ2Hex, &status);
  viz_GetNumberOfEntities(iObj, VizQ3Hex, &nbrQ3Hex, &status);
  viz_GetNumberOfEntities(iObj, VizQ4Hex, &nbrQ4Hex, &status);

  vtkUnstructuredGrid* vtkGrid;
  vtkGrid = vtkUnstructuredGrid::New();

  // Points
  vtkPoints* const vtk_points = vtkPoints::New();
  vtk_points->Allocate(nbrVer);

  int      sizVer;
  double3* Crd = (double3*)viz_GetIndicesArray(iObj, VizVer, &nbrVer, &sizVer, &status);

  for (int i = 1; i <= nbrVer; i++)
    vtk_points->InsertNextPoint(Crd[i][0], Crd[i][1], Crd[i][2]);

  if (nbrVer > 0)
    vtkGrid->SetPoints(vtk_points);
  vtk_points->Delete();

  // All the cells at the same time
  // WARNING : Indices start at 1 in mesh file.
  vtkIdType ncells = nbrP1Edg + nbrP1Tri + nbrQ1Qua + nbrTet + nbrPyr + nbrPri + nbrHex;
  ncells += nbrP2Edg + nbrP2Tri + nbrQ2Qua + nbrP2Tet + nbrP2Pyr + nbrP2Pri + nbrQ2Hex;
  ncells += nbrP3Edg + nbrP3Tri + nbrQ3Qua + nbrP3Tet + nbrP3Pri + nbrQ3Hex;
  ncells += nbrP4Edg + nbrP4Tri + nbrQ4Qua + nbrP4Tet + nbrP4Pri + nbrQ4Hex;
  // ncells += nbrP3Pyr + nbrP4Pyr;

  vtkCellArray* const cells = vtkCellArray::New();
  cells->Allocate(ncells);

  vector<int> types(ncells);
  int         curCell = 0, nbrEnt, sizEnt;
  int1 *      Ent = NULL, *EntRef = NULL;

  vtkDataArray* refArray = vtkDoubleArray::New();
  refArray->SetName("Reference");
  refArray->SetNumberOfComponents(1); // 1 for scalar
  refArray->SetNumberOfTuples(ncells);

  //-- Edges
  if (nbrP1Edg > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizEdg, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizEdg, &status);
  }
  for (int i = 1; i <= nbrP1Edg; i++) {
    vtkIdType edge[2] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1
    };
    cells->InsertNextCell(2, edge);

    types[curCell] = VTK_LINE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- Triangles
  if (nbrP1Tri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizTri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizTri, &status);
  }
  for (int i = 1; i <= nbrP1Tri; i++) {
    vtkIdType triangle[3] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1
    };
    cells->InsertNextCell(3, triangle);

    types[curCell] = VTK_TRIANGLE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- Quad
  if (nbrQ1Qua > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizQua, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizQua, &status);
  }
  for (int i = 1; i <= nbrQ1Qua; i++) {
    vtkIdType quad[4] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1
    };
    cells->InsertNextCell(4, quad);

    types[curCell] = VTK_QUAD;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- Tets
  if (nbrTet > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizTet, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizTet, &status);
  }
  for (int i = 1; i <= nbrTet; i++) {
    vtkIdType tet[4] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1
    };
    cells->InsertNextCell(4, tet);

    types[curCell] = VTK_TETRA;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- Pyr
  if (nbrPyr > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizPyr, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizPyr, &status);
  }
  for (int i = 1; i <= nbrPyr; i++) {
    vtkIdType pyr[5] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1
    };
    cells->InsertNextCell(5, pyr);

    types[curCell] = VTK_PYRAMID;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- Pri
  if (VizPri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizPri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizPri, &status);
  }
  for (int i = 1; i <= nbrPri; i++) {
    vtkIdType pri[6] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1
    };
    cells->InsertNextCell(6, pri);

    types[curCell] = VTK_WEDGE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- Hex
  if (nbrHex > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizHex, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizHex, &status);
  }
  for (int i = 1; i <= nbrHex; i++) {
    vtkIdType hex[8] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1,
      Ent[sizEnt * i + 6] - 1,
      Ent[sizEnt * i + 7] - 1
    };
    cells->InsertNextCell(8, hex);

    types[curCell] = VTK_HEXAHEDRON;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- EdgP2
  if (nbrP2Edg > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP2Edg, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP2Edg, &status);
  }
  for (int i = 1; i <= nbrP2Edg; i++) {
    vtkIdType edgP2[3] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1
    };
    cells->InsertNextCell(3, edgP2);

    types[curCell] = VTK_QUADRATIC_EDGE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- TriP2
  if (nbrP2Tri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP2Tri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP2Tri, &status);
  }
  for (int i = 1; i <= nbrP2Tri; i++) {
    vtkIdType triP2[6] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1
    };
    cells->InsertNextCell(6, triP2);

    types[curCell] = VTK_QUADRATIC_TRIANGLE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- QuadQ2
  if (nbrQ2Qua > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizQ2Qua, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizQ2Qua, &status);
  }
  for (int i = 1; i <= nbrQ2Qua; i++) {
    vtkIdType quaq2[9] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1,
      Ent[sizEnt * i + 6] - 1,
      Ent[sizEnt * i + 7] - 1,
      Ent[sizEnt * i + 8] - 1
    };
    cells->InsertNextCell(9, quaq2);

    types[curCell] = VTK_BIQUADRATIC_QUAD;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- TetP2
  if (nbrP2Tet > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP2Tet, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP2Tet, &status);
  }
  for (int i = 1; i <= nbrP2Tet; i++) {
    vtkIdType tetp2[10] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1,
      Ent[sizEnt * i + 6] - 1,
      Ent[sizEnt * i + 7] - 1,
      Ent[sizEnt * i + 8] - 1,
      Ent[sizEnt * i + 9] - 1
    };
    cells->InsertNextCell(10, tetp2);

    types[curCell] = VTK_QUADRATIC_TETRA;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- PyrP2
  if (nbrP2Pyr > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP2Pyr, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP2Pyr, &status);
  }
  for (int i = 1; i <= nbrP2Pyr; i++) {
    vtkIdType pyrp2[14] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1,
      Ent[sizEnt * i + 6] - 1,
      Ent[sizEnt * i + 7] - 1,
      Ent[sizEnt * i + 8] - 1,
      Ent[sizEnt * i + 9] - 1,
      Ent[sizEnt * i + 10] - 1,
      Ent[sizEnt * i + 11] - 1,
      Ent[sizEnt * i + 12] - 1,
      Ent[sizEnt * i + 13] - 1
    };
    cells->InsertNextCell(14, pyrp2);

    types[curCell] = VTK_QUADRATIC_PYRAMID;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- PriP2
  if (nbrP2Pri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP2Pri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP2Pri, &status);
  }
  for (int i = 1; i <= nbrP2Pri; i++) {
    vtkIdType prip2[18] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1,
      Ent[sizEnt * i + 6] - 1,
      Ent[sizEnt * i + 7] - 1,
      Ent[sizEnt * i + 8] - 1,
      Ent[sizEnt * i + 9] - 1,
      Ent[sizEnt * i + 10] - 1,
      Ent[sizEnt * i + 11] - 1,
      Ent[sizEnt * i + 12] - 1,
      Ent[sizEnt * i + 13] - 1,
      Ent[sizEnt * i + 14] - 1,
      Ent[sizEnt * i + 15] - 1,
      Ent[sizEnt * i + 16] - 1,
      Ent[sizEnt * i + 17] - 1
    };
    cells->InsertNextCell(18, prip2);

    types[curCell] = VTK_BIQUADRATIC_QUADRATIC_WEDGE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- HexQ2
  if (nbrQ2Hex > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizQ2Hex, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizQ2Hex, &status);
  }
  for (int i = 1; i <= nbrQ2Hex; i++) {
    vtkIdType hexq2[27] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1,
      Ent[sizEnt * i + 6] - 1,
      Ent[sizEnt * i + 7] - 1,
      Ent[sizEnt * i + 8] - 1,
      Ent[sizEnt * i + 9] - 1,
      Ent[sizEnt * i + 10] - 1,
      Ent[sizEnt * i + 11] - 1,
      Ent[sizEnt * i + 12] - 1,
      Ent[sizEnt * i + 13] - 1,
      Ent[sizEnt * i + 14] - 1,
      Ent[sizEnt * i + 15] - 1,
      Ent[sizEnt * i + 16] - 1,
      Ent[sizEnt * i + 17] - 1,
      Ent[sizEnt * i + 18] - 1,
      Ent[sizEnt * i + 19] - 1,
      Ent[sizEnt * i + 25] - 1, //-- node 20 VTK, 25 PLG (starting at 0)
      Ent[sizEnt * i + 23] - 1, //-- node 21 VTK, 23 PLG
      Ent[sizEnt * i + 22] - 1, //-- node 22 VTK, 22 PLG
      Ent[sizEnt * i + 24] - 1, //-- node 23 VTK, 24 PLG
      Ent[sizEnt * i + 20] - 1, //-- node 24 VTK, 20 PLG
      Ent[sizEnt * i + 21] - 1, //-- node 25 VTK, 21 PLG
      Ent[sizEnt * i + 26] - 1
    };
    cells->InsertNextCell(27, hexq2);

    types[curCell] = VTK_TRIQUADRATIC_HEXAHEDRON;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- EdgP3
  if (nbrP3Edg > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP3Edg, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP3Edg, &status);
  }
  for (int i = 1; i <= nbrP3Edg; i++) {
    vtkIdType edgp3[4] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1
    };
    cells->InsertNextCell(4, edgp3);

    types[curCell] = VTK_LAGRANGE_CURVE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- TriP3
  if (nbrP3Tri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP3Tri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP3Tri, &status);

    for (int i = 1; i <= nbrP3Tri; i++) {
      vtkIdType trip3[10] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1,
        Ent[sizEnt * i + 9] - 1
      };
      cells->InsertNextCell(10, trip3);

      types[curCell] = VTK_LAGRANGE_TRIANGLE;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  //-- QuadQ3
  if (nbrQ3Qua > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizQ3Qua, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizQ3Qua, &status);

    for (int i = 1; i <= nbrQ3Qua; i++) {
      vtkIdType quaq3[16] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 9] - 1,  //-- node 8 VTK, 9 PLG
        Ent[sizEnt * i + 8] - 1,  //-- node 9 VTK, 8 PLG
        Ent[sizEnt * i + 11] - 1, //-- node 10 VTK, 11 PLG
        Ent[sizEnt * i + 10] - 1, //-- node 11 VTK, 10 PLG
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 13] - 1,
        Ent[sizEnt * i + 15] - 1, //-- node 14 VTK, 15 PLG
        Ent[sizEnt * i + 14] - 1
      }; //-- node 15 VTK, 14 PLG
      cells->InsertNextCell(16, quaq3);
      //-- ordering shown here https://www.kitware.com/modeling-arbitrary-order-lagrange-finite-elements-in-the-visualization-toolkit/

      types[curCell] = VTK_LAGRANGE_QUADRILATERAL;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  //-- TetP3
  if (nbrP3Tet > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP3Tet, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP3Tet, &status);

    for (int i = 1; i <= nbrP3Tet; i++) {
      vtkIdType tetp3[20] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1,
        Ent[sizEnt * i + 9] - 1,
        Ent[sizEnt * i + 10] - 1,
        Ent[sizEnt * i + 11] - 1,
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 13] - 1,
        Ent[sizEnt * i + 14] - 1,
        Ent[sizEnt * i + 15] - 1,
        Ent[sizEnt * i + 17] - 1, //-- node 16 VTK, 17 PLG
        Ent[sizEnt * i + 18] - 1, //-- node 17 VTK, 18 PLG
        Ent[sizEnt * i + 19] - 1, //-- node 18 VTK, 19 PLG
        Ent[sizEnt * i + 16] - 1
      }; //-- node 19 VTK, 16 PLG
      cells->InsertNextCell(20, tetp3);
      //-- ordering  here is wrong for tetra !! https://www.kitware.com/modeling-arbitrary-order-lagrange-finite-elements-in-the-visualization-toolkit/
      //-- use this one instead https://coreform.com/papers/implementation-of-rational-bezier-cells-into-VTK-report.pdf

      types[curCell] = VTK_LAGRANGE_TETRAHEDRON;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  ////-- PyrP3
  // if (nbrP3Pyr > 0)
  //{
  //   Ent = viz_GetIndicesArray(iObj, VizP3Pyr, &nbrEnt, &sizEnt, &status);
  //   EntRef = viz_GetReferenceArray(iObj, VizP3Pyr, &status);

  //  for (int i = 1; i <= nbrP3Pyr; i++)
  //  {
  //    vtkIdType pyrp3[30] = {
  //        Ent[sizEnt * i] - 1,
  //        Ent[sizEnt * i + 1] - 1,
  //        Ent[sizEnt * i + 2] - 1,
  //        Ent[sizEnt * i + 3] - 1,
  //        Ent[sizEnt * i + 4] - 1,
  //        Ent[sizEnt * i + 5] - 1,
  //        Ent[sizEnt * i + 6] - 1,
  //        Ent[sizEnt * i + 7] - 1,
  //        Ent[sizEnt * i + 8] - 1,
  //        Ent[sizEnt * i + 9] - 1,
  //        Ent[sizEnt * i + 10] - 1,
  //        Ent[sizEnt * i + 11] - 1,
  //        Ent[sizEnt * i + 12] - 1,
  //        Ent[sizEnt * i + 13] - 1,
  //        Ent[sizEnt * i + 14] - 1,
  //        Ent[sizEnt * i + 15] - 1,
  //        Ent[sizEnt * i + 16] - 1,
  //        Ent[sizEnt * i + 17] - 1,
  //        Ent[sizEnt * i + 18] - 1,
  //        Ent[sizEnt * i + 19] - 1,
  //        Ent[sizEnt * i + 20] - 1,
  //        Ent[sizEnt * i + 21] - 1,
  //        Ent[sizEnt * i + 22] - 1,
  //        Ent[sizEnt * i + 23] - 1,
  //        Ent[sizEnt * i + 24] - 1,
  //        Ent[sizEnt * i + 25] - 1,
  //        Ent[sizEnt * i + 26] - 1,
  //        Ent[sizEnt * i + 27] - 1,
  //        Ent[sizEnt * i + 28] - 1,
  //        Ent[sizEnt * i + 29] - 1};
  //    cells->InsertNextCell(30, pyrp3);

  //    types[curCell] = VTK_LAGRANGE_PYRAMID;
  //    refArray->SetTuple1(curCell, EntRef[i]);
  //    curCell++;
  //  }
  //}

  //-- PriP3
  if (nbrP3Pri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP3Pri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP3Pri, &status);

    for (int i = 1; i <= nbrP3Pri; i++) {
      vtkIdType prip3[40] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1,
        Ent[sizEnt * i + 9] - 1,
        Ent[sizEnt * i + 10] - 1,
        Ent[sizEnt * i + 11] - 1,
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 13] - 1,
        Ent[sizEnt * i + 14] - 1,
        Ent[sizEnt * i + 15] - 1,
        Ent[sizEnt * i + 16] - 1,
        Ent[sizEnt * i + 17] - 1,
        Ent[sizEnt * i + 18] - 1,
        Ent[sizEnt * i + 19] - 1,
        Ent[sizEnt * i + 20] - 1,
        Ent[sizEnt * i + 21] - 1,
        Ent[sizEnt * i + 22] - 1,
        Ent[sizEnt * i + 23] - 1,
        Ent[sizEnt * i + 24] - 1,
        Ent[sizEnt * i + 25] - 1,
        Ent[sizEnt * i + 26] - 1,
        Ent[sizEnt * i + 27] - 1,
        Ent[sizEnt * i + 29] - 1, //-- node 28 VTK, 29 PLG
        Ent[sizEnt * i + 28] - 1, //-- node 29 VTK, 28 PLG
        Ent[sizEnt * i + 30] - 1,
        Ent[sizEnt * i + 31] - 1,
        Ent[sizEnt * i + 33] - 1, //-- node 32 VTK, 33 PLG
        Ent[sizEnt * i + 32] - 1, //-- node 33 VTK, 32 PLG
        Ent[sizEnt * i + 35] - 1,
        Ent[sizEnt * i + 34] - 1,
        Ent[sizEnt * i + 36] - 1, //-- node 36 VTK, 37 PLG
        Ent[sizEnt * i + 37] - 1, //-- node 37 VTK, 36 PLG
        Ent[sizEnt * i + 38] - 1,
        Ent[sizEnt * i + 39] - 1
      };
      cells->InsertNextCell(40, prip3);
      //-- ordering here  https://www.kitware.com/modeling-arbitrary-order-lagrange-finite-elements-in-the-visualization-toolkit/
      //-- and here are wrong for last face of prism https://coreform.com/papers/implementation-of-rational-bezier-cells-into-VTK-report.pdf
      //-- the Z in the last face follow the same ordering than for hexahedra

      types[curCell] = VTK_LAGRANGE_WEDGE;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  //-- HexQ3
  if (nbrQ3Hex > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizQ3Hex, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizQ3Hex, &status);

    for (int i = 1; i <= nbrQ3Hex; i++) {
      vtkIdType hexq3[64] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1, //-- First edge
        Ent[sizEnt * i + 9] - 1,
        Ent[sizEnt * i + 10] - 1, //-- Second edge
        Ent[sizEnt * i + 11] - 1,
        Ent[sizEnt * i + 13] - 1, //-- 3rd edge
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 15] - 1, //-- 4th edge
        Ent[sizEnt * i + 14] - 1,
        Ent[sizEnt * i + 16] - 1, //-- 5th edge
        Ent[sizEnt * i + 17] - 1,
        Ent[sizEnt * i + 18] - 1, //-- 6th edge
        Ent[sizEnt * i + 19] - 1,
        Ent[sizEnt * i + 21] - 1, //-- 7th edge
        Ent[sizEnt * i + 20] - 1,
        Ent[sizEnt * i + 23] - 1, //-- 8th edge
        Ent[sizEnt * i + 22] - 1,
        Ent[sizEnt * i + 24] - 1, //-- 9th edge
        Ent[sizEnt * i + 25] - 1,
        Ent[sizEnt * i + 26] - 1, //-- 10th edge
        Ent[sizEnt * i + 27] - 1,
        Ent[sizEnt * i + 30] - 1, //-- 11th edge
        Ent[sizEnt * i + 31] - 1,
        Ent[sizEnt * i + 28] - 1, //-- 12th edge
        Ent[sizEnt * i + 29] - 1,
        Ent[sizEnt * i + 53] - 1, //-- First face
        Ent[sizEnt * i + 52] - 1,
        Ent[sizEnt * i + 54] - 1,
        Ent[sizEnt * i + 55] - 1,
        Ent[sizEnt * i + 44] - 1, //-- Second face
        Ent[sizEnt * i + 45] - 1,
        Ent[sizEnt * i + 47] - 1,
        Ent[sizEnt * i + 46] - 1,
        Ent[sizEnt * i + 40] - 1, //-- 3rd face
        Ent[sizEnt * i + 41] - 1,
        Ent[sizEnt * i + 43] - 1,
        Ent[sizEnt * i + 42] - 1,
        Ent[sizEnt * i + 49] - 1, //-- 4th face
        Ent[sizEnt * i + 48] - 1,
        Ent[sizEnt * i + 50] - 1,
        Ent[sizEnt * i + 51] - 1,
        Ent[sizEnt * i + 32] - 1, //-- 5th face
        Ent[sizEnt * i + 33] - 1,
        Ent[sizEnt * i + 35] - 1,
        Ent[sizEnt * i + 34] - 1,
        Ent[sizEnt * i + 36] - 1, //-- 6th face
        Ent[sizEnt * i + 37] - 1,
        Ent[sizEnt * i + 39] - 1,
        Ent[sizEnt * i + 38] - 1,
        Ent[sizEnt * i + 56] - 1, //-- volume
        Ent[sizEnt * i + 57] - 1,
        Ent[sizEnt * i + 59] - 1,
        Ent[sizEnt * i + 58] - 1,
        Ent[sizEnt * i + 60] - 1,
        Ent[sizEnt * i + 61] - 1,
        Ent[sizEnt * i + 63] - 1,
        Ent[sizEnt * i + 62] - 1
      };
      cells->InsertNextCell(64, hexq3);

      types[curCell] = VTK_LAGRANGE_HEXAHEDRON;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  //-- EdgP4
  if (nbrP4Edg > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP4Edg, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP4Edg, &status);
  }
  for (int i = 1; i <= nbrP4Edg; i++) {
    vtkIdType edgp4[5] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1
    };
    cells->InsertNextCell(5, edgp4);

    types[curCell] = VTK_LAGRANGE_CURVE;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- TriP4
  if (nbrP4Tri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP4Tri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP4Tri, &status);

    for (int i = 1; i <= nbrP4Tri; i++) {
      vtkIdType trip4[15] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1,
        Ent[sizEnt * i + 9] - 1,
        Ent[sizEnt * i + 10] - 1,
        Ent[sizEnt * i + 11] - 1,
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 13] - 1,
        Ent[sizEnt * i + 14] - 1
      };
      cells->InsertNextCell(15, trip4);

      types[curCell] = VTK_LAGRANGE_TRIANGLE;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  //-- QuadQ4
  if (nbrQ4Qua > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizQ4Qua, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizQ4Qua, &status);
  }
  for (int i = 1; i <= nbrQ4Qua; i++) {
    vtkIdType quaq4[25] = {
      Ent[sizEnt * i] - 1,
      Ent[sizEnt * i + 1] - 1,
      Ent[sizEnt * i + 2] - 1,
      Ent[sizEnt * i + 3] - 1,
      Ent[sizEnt * i + 4] - 1,
      Ent[sizEnt * i + 5] - 1,
      Ent[sizEnt * i + 6] - 1,
      Ent[sizEnt * i + 7] - 1,
      Ent[sizEnt * i + 8] - 1,
      Ent[sizEnt * i + 9] - 1,
      Ent[sizEnt * i + 12] - 1, //-- node 10 VTK, 12 PLG
      Ent[sizEnt * i + 11] - 1,
      Ent[sizEnt * i + 10] - 1, //-- node 12 VTK, 10 PLG
      Ent[sizEnt * i + 15] - 1, //-- node 13 VTK, 15 PLG
      Ent[sizEnt * i + 14] - 1,
      Ent[sizEnt * i + 13] - 1, //-- node 15 VTK, 13 PLG
      Ent[sizEnt * i + 16] - 1,
      Ent[sizEnt * i + 20] - 1, //-- node 17 VTK, 20 PLG
      Ent[sizEnt * i + 17] - 1, //-- node 18 VTK, 17 PLG
      Ent[sizEnt * i + 23] - 1, //-- node 19 VTK, 23 PLG
      Ent[sizEnt * i + 24] - 1, //-- node 20 VTK, 24 PLG
      Ent[sizEnt * i + 21] - 1,
      Ent[sizEnt * i + 19] - 1, //-- node 22 VTK, 19 PLG
      Ent[sizEnt * i + 22] - 1, //-- node 23 VTK, 22 PLG
      Ent[sizEnt * i + 18] - 1
    }; //-- node 24 VTK, 18 PLG
    cells->InsertNextCell(25, quaq4);
    //-- ordering shown here https://www.kitware.com/modeling-arbitrary-order-lagrange-finite-elements-in-the-visualization-toolkit/

    types[curCell] = VTK_LAGRANGE_QUADRILATERAL;
    refArray->SetTuple1(curCell, EntRef[i]);
    curCell++;
  }

  //-- TetP4
  if (nbrP4Tet > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP4Tet, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP4Tet, &status);

    for (int i = 1; i <= nbrP4Tet; i++) {
      vtkIdType tetp4[35] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1,
        Ent[sizEnt * i + 9] - 1,
        Ent[sizEnt * i + 10] - 1,
        Ent[sizEnt * i + 11] - 1,
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 13] - 1,
        Ent[sizEnt * i + 14] - 1,
        Ent[sizEnt * i + 15] - 1,
        Ent[sizEnt * i + 16] - 1,
        Ent[sizEnt * i + 17] - 1,
        Ent[sizEnt * i + 18] - 1,
        Ent[sizEnt * i + 19] - 1,
        Ent[sizEnt * i + 20] - 1,
        Ent[sizEnt * i + 21] - 1,
        Ent[sizEnt * i + 25] - 1, //-- Face 013 (starting 0) / 124
        Ent[sizEnt * i + 26] - 1,
        Ent[sizEnt * i + 27] - 1,
        Ent[sizEnt * i + 29] - 1, //-- Face 123 (starting 0) / 234
        Ent[sizEnt * i + 30] - 1, //---- Swap vertices
        Ent[sizEnt * i + 28] - 1,
        Ent[sizEnt * i + 32] - 1, //-- Face 023 (starting 0) / 134
        Ent[sizEnt * i + 33] - 1, //---- Vertices 31 32 33 VTK become 32 33 31 PLG
        Ent[sizEnt * i + 31] - 1,
        Ent[sizEnt * i + 22] - 1, //-- Face 012 (starting 0) / 123
        Ent[sizEnt * i + 24] - 1, //---- Vertices 22 23 24 VTK become 22 24 23 PLG
        Ent[sizEnt * i + 23] - 1,
        Ent[sizEnt * i + 34] - 1
      };
      cells->InsertNextCell(35, tetp4);
      //-- ordering  here is wrong for tetra !! https://www.kitware.com/modeling-arbitrary-order-lagrange-finite-elements-in-the-visualization-toolkit/
      //-- use this one instead https://coreform.com/papers/implementation-of-rational-bezier-cells-into-VTK-report.pdf

      types[curCell] = VTK_LAGRANGE_TETRAHEDRON;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  ////-- PyrP4
  // if (nbrP4Pyr > 0)
  //{
  //   Ent = viz_GetIndicesArray(iObj, VizP4Pyr, &nbrEnt, &sizEnt, &status);
  //   EntRef = viz_GetReferenceArray(iObj, VizP4Pyr, &status);

  //  for (int i = 1; i <= nbrP4Pyr; i++)
  //  {
  //    vtkIdType pyrp4[55] = {
  //        Ent[sizEnt * i] - 1,
  //        Ent[sizEnt * i + 1] - 1,
  //        Ent[sizEnt * i + 2] - 1,
  //        Ent[sizEnt * i + 3] - 1,
  //        Ent[sizEnt * i + 4] - 1,
  //        Ent[sizEnt * i + 5] - 1,
  //        Ent[sizEnt * i + 6] - 1,
  //        Ent[sizEnt * i + 7] - 1,
  //        Ent[sizEnt * i + 8] - 1,
  //        Ent[sizEnt * i + 9] - 1,
  //        Ent[sizEnt * i + 10] - 1,
  //        Ent[sizEnt * i + 11] - 1,
  //        Ent[sizEnt * i + 12] - 1,
  //        Ent[sizEnt * i + 13] - 1,
  //        Ent[sizEnt * i + 14] - 1,
  //        Ent[sizEnt * i + 15] - 1,
  //        Ent[sizEnt * i + 16] - 1,
  //        Ent[sizEnt * i + 17] - 1,
  //        Ent[sizEnt * i + 18] - 1,
  //        Ent[sizEnt * i + 19] - 1,
  //        Ent[sizEnt * i + 20] - 1,
  //        Ent[sizEnt * i + 21] - 1,
  //        Ent[sizEnt * i + 22] - 1,
  //        Ent[sizEnt * i + 23] - 1,
  //        Ent[sizEnt * i + 24] - 1,
  //        Ent[sizEnt * i + 25] - 1,
  //        Ent[sizEnt * i + 26] - 1,
  //        Ent[sizEnt * i + 27] - 1,
  //        Ent[sizEnt * i + 28] - 1,
  //        Ent[sizEnt * i + 29] - 1,
  //        Ent[sizEnt * i + 30] - 1,
  //        Ent[sizEnt * i + 31] - 1,
  //        Ent[sizEnt * i + 32] - 1,
  //        Ent[sizEnt * i + 33] - 1,
  //        Ent[sizEnt * i + 34] - 1,
  //        Ent[sizEnt * i + 35] - 1,
  //        Ent[sizEnt * i + 36] - 1,
  //        Ent[sizEnt * i + 37] - 1,
  //        Ent[sizEnt * i + 38] - 1,
  //        Ent[sizEnt * i + 39] - 1,
  //        Ent[sizEnt * i + 40] - 1,
  //        Ent[sizEnt * i + 41] - 1,
  //        Ent[sizEnt * i + 42] - 1,
  //        Ent[sizEnt * i + 43] - 1,
  //        Ent[sizEnt * i + 44] - 1,
  //        Ent[sizEnt * i + 45] - 1,
  //        Ent[sizEnt * i + 46] - 1,
  //        Ent[sizEnt * i + 47] - 1,
  //        Ent[sizEnt * i + 48] - 1,
  //        Ent[sizEnt * i + 49] - 1,
  //        Ent[sizEnt * i + 50] - 1,
  //        Ent[sizEnt * i + 51] - 1,
  //        Ent[sizEnt * i + 52] - 1,
  //        Ent[sizEnt * i + 53] - 1,
  //        Ent[sizEnt * i + 54] - 1};
  //    cells->InsertNextCell(55, pyrp4);

  //    types[curCell] = VTK_LAGRANGE_PYRAMID;
  //    refArray->SetTuple1(curCell, EntRef[i]);
  //    curCell++;
  //  }
  //}

  //-- PriP4
  if (nbrP4Pri > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizP4Pri, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizP4Pri, &status);

    for (int i = 1; i <= nbrP4Pri; i++) {
      vtkIdType prip4[75] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1,
        Ent[sizEnt * i + 9] - 1,
        Ent[sizEnt * i + 10] - 1,
        Ent[sizEnt * i + 11] - 1,
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 13] - 1,
        Ent[sizEnt * i + 14] - 1,
        Ent[sizEnt * i + 15] - 1,
        Ent[sizEnt * i + 16] - 1,
        Ent[sizEnt * i + 17] - 1,
        Ent[sizEnt * i + 18] - 1,
        Ent[sizEnt * i + 19] - 1,
        Ent[sizEnt * i + 20] - 1,
        Ent[sizEnt * i + 21] - 1,
        Ent[sizEnt * i + 22] - 1,
        Ent[sizEnt * i + 23] - 1,
        Ent[sizEnt * i + 24] - 1,
        Ent[sizEnt * i + 25] - 1,
        Ent[sizEnt * i + 26] - 1,
        Ent[sizEnt * i + 27] - 1,
        Ent[sizEnt * i + 28] - 1,
        Ent[sizEnt * i + 29] - 1,
        Ent[sizEnt * i + 30] - 1,
        Ent[sizEnt * i + 31] - 1,
        Ent[sizEnt * i + 32] - 1,
        Ent[sizEnt * i + 33] - 1, //-- First triangle
        Ent[sizEnt * i + 34] - 1,
        Ent[sizEnt * i + 35] - 1,
        Ent[sizEnt * i + 36] - 1, //-- Second triangle
        Ent[sizEnt * i + 37] - 1,
        Ent[sizEnt * i + 38] - 1,
        Ent[sizEnt * i + 39] - 1, //-- First quad 39-47
        Ent[sizEnt * i + 43] - 1,
        Ent[sizEnt * i + 40] - 1,
        Ent[sizEnt * i + 46] - 1,
        Ent[sizEnt * i + 47] - 1,
        Ent[sizEnt * i + 44] - 1,
        Ent[sizEnt * i + 42] - 1,
        Ent[sizEnt * i + 45] - 1,
        Ent[sizEnt * i + 41] - 1,
        Ent[sizEnt * i + 48] - 1, //-- Second quad 48-56
        Ent[sizEnt * i + 52] - 1,
        Ent[sizEnt * i + 49] - 1,
        Ent[sizEnt * i + 55] - 1,
        Ent[sizEnt * i + 56] - 1,
        Ent[sizEnt * i + 53] - 1,
        Ent[sizEnt * i + 51] - 1,
        Ent[sizEnt * i + 54] - 1,
        Ent[sizEnt * i + 50] - 1,
        Ent[sizEnt * i + 58] - 1, //-- Thrid quad 57-65
        Ent[sizEnt * i + 61] - 1,
        Ent[sizEnt * i + 57] - 1,
        Ent[sizEnt * i + 62] - 1,
        Ent[sizEnt * i + 65] - 1,
        Ent[sizEnt * i + 64] - 1,
        Ent[sizEnt * i + 59] - 1,
        Ent[sizEnt * i + 63] - 1,
        Ent[sizEnt * i + 60] - 1,
        Ent[sizEnt * i + 66] - 1, //-- Volume 66
        Ent[sizEnt * i + 67] - 1,
        Ent[sizEnt * i + 68] - 1,
        Ent[sizEnt * i + 72] - 1,
        Ent[sizEnt * i + 73] - 1,
        Ent[sizEnt * i + 74] - 1,
        Ent[sizEnt * i + 69] - 1,
        Ent[sizEnt * i + 70] - 1,
        Ent[sizEnt * i + 71] - 1
      };
      cells->InsertNextCell(75, prip4);
      //-- ordering here  https://www.kitware.com/modeling-arbitrary-order-lagrange-finite-elements-in-the-visualization-toolkit/
      //-- and here are wrong for last face of prism https://coreform.com/papers/implementation-of-rational-bezier-cells-into-VTK-report.pdf
      //-- the Z in the last face follow the same ordering than for hexahedra

      types[curCell] = VTK_LAGRANGE_WEDGE;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  //-- HexQ4
  if (nbrQ4Hex > 0) {
    Ent    = viz_GetIndicesArray(iObj, VizQ4Hex, &nbrEnt, &sizEnt, &status);
    EntRef = viz_GetReferenceArray(iObj, VizQ4Hex, &status);

    for (int i = 1; i <= nbrQ4Hex; i++) {
      vtkIdType hexq4[125] = {
        Ent[sizEnt * i] - 1,
        Ent[sizEnt * i + 1] - 1,
        Ent[sizEnt * i + 2] - 1,
        Ent[sizEnt * i + 3] - 1,
        Ent[sizEnt * i + 4] - 1,
        Ent[sizEnt * i + 5] - 1,
        Ent[sizEnt * i + 6] - 1,
        Ent[sizEnt * i + 7] - 1,
        Ent[sizEnt * i + 8] - 1, //-- First edge
        Ent[sizEnt * i + 9] - 1,
        Ent[sizEnt * i + 10] - 1,
        Ent[sizEnt * i + 11] - 1, //-- Second edge
        Ent[sizEnt * i + 12] - 1,
        Ent[sizEnt * i + 13] - 1,
        Ent[sizEnt * i + 16] - 1, //-- 3rd edge
        Ent[sizEnt * i + 15] - 1,
        Ent[sizEnt * i + 14] - 1,
        Ent[sizEnt * i + 19] - 1, //-- 4th edge
        Ent[sizEnt * i + 18] - 1,
        Ent[sizEnt * i + 17] - 1,
        Ent[sizEnt * i + 20] - 1, //-- 5th edge
        Ent[sizEnt * i + 21] - 1,
        Ent[sizEnt * i + 22] - 1,
        Ent[sizEnt * i + 23] - 1, //-- 6th edge
        Ent[sizEnt * i + 24] - 1,
        Ent[sizEnt * i + 25] - 1,
        Ent[sizEnt * i + 28] - 1, //-- 7th edge
        Ent[sizEnt * i + 27] - 1,
        Ent[sizEnt * i + 26] - 1,
        Ent[sizEnt * i + 31] - 1, //-- 8th edge
        Ent[sizEnt * i + 30] - 1,
        Ent[sizEnt * i + 29] - 1,
        Ent[sizEnt * i + 32] - 1, //-- 9th edge
        Ent[sizEnt * i + 33] - 1,
        Ent[sizEnt * i + 34] - 1,
        Ent[sizEnt * i + 35] - 1, //-- 10th edge
        Ent[sizEnt * i + 36] - 1,
        Ent[sizEnt * i + 37] - 1,
        Ent[sizEnt * i + 41] - 1, //-- 11th edge
        Ent[sizEnt * i + 42] - 1,
        Ent[sizEnt * i + 43] - 1,
        Ent[sizEnt * i + 38] - 1, //-- 12th edge
        Ent[sizEnt * i + 39] - 1,
        Ent[sizEnt * i + 40] - 1,
        Ent[sizEnt * i + 90] - 1, //-- First face
        Ent[sizEnt * i + 93] - 1,
        Ent[sizEnt * i + 89] - 1,
        Ent[sizEnt * i + 94] - 1,
        Ent[sizEnt * i + 97] - 1,
        Ent[sizEnt * i + 96] - 1,
        Ent[sizEnt * i + 91] - 1,
        Ent[sizEnt * i + 95] - 1,
        Ent[sizEnt * i + 92] - 1,
        Ent[sizEnt * i + 71] - 1, //-- Second face
        Ent[sizEnt * i + 75] - 1,
        Ent[sizEnt * i + 72] - 1,
        Ent[sizEnt * i + 78] - 1,
        Ent[sizEnt * i + 79] - 1,
        Ent[sizEnt * i + 76] - 1,
        Ent[sizEnt * i + 74] - 1,
        Ent[sizEnt * i + 77] - 1,
        Ent[sizEnt * i + 73] - 1,
        Ent[sizEnt * i + 62] - 1, //-- 3rd face
        Ent[sizEnt * i + 66] - 1,
        Ent[sizEnt * i + 63] - 1,
        Ent[sizEnt * i + 69] - 1,
        Ent[sizEnt * i + 70] - 1,
        Ent[sizEnt * i + 67] - 1,
        Ent[sizEnt * i + 65] - 1,
        Ent[sizEnt * i + 68] - 1,
        Ent[sizEnt * i + 64] - 1,
        Ent[sizEnt * i + 81] - 1, //-- 4th face
        Ent[sizEnt * i + 84] - 1,
        Ent[sizEnt * i + 80] - 1,
        Ent[sizEnt * i + 85] - 1,
        Ent[sizEnt * i + 88] - 1,
        Ent[sizEnt * i + 87] - 1,
        Ent[sizEnt * i + 82] - 1,
        Ent[sizEnt * i + 86] - 1,
        Ent[sizEnt * i + 83] - 1,
        Ent[sizEnt * i + 44] - 1, //-- 5th face
        Ent[sizEnt * i + 48] - 1,
        Ent[sizEnt * i + 45] - 1,
        Ent[sizEnt * i + 51] - 1,
        Ent[sizEnt * i + 52] - 1,
        Ent[sizEnt * i + 49] - 1,
        Ent[sizEnt * i + 47] - 1,
        Ent[sizEnt * i + 50] - 1,
        Ent[sizEnt * i + 46] - 1,
        Ent[sizEnt * i + 53] - 1, //-- 6th face
        Ent[sizEnt * i + 57] - 1,
        Ent[sizEnt * i + 54] - 1,
        Ent[sizEnt * i + 60] - 1,
        Ent[sizEnt * i + 61] - 1,
        Ent[sizEnt * i + 58] - 1,
        Ent[sizEnt * i + 56] - 1,
        Ent[sizEnt * i + 59] - 1,
        Ent[sizEnt * i + 55] - 1,
        Ent[sizEnt * i + 98] - 1, //-- volume //-- bottom
        Ent[sizEnt * i + 106] - 1,
        Ent[sizEnt * i + 99] - 1,
        Ent[sizEnt * i + 109] - 1,
        Ent[sizEnt * i + 118] - 1,
        Ent[sizEnt * i + 107] - 1,
        Ent[sizEnt * i + 101] - 1,
        Ent[sizEnt * i + 108] - 1,
        Ent[sizEnt * i + 100] - 1,
        Ent[sizEnt * i + 114] - 1, //-- middle
        Ent[sizEnt * i + 120] - 1,
        Ent[sizEnt * i + 115] - 1,
        Ent[sizEnt * i + 123] - 1,
        Ent[sizEnt * i + 124] - 1,
        Ent[sizEnt * i + 121] - 1,
        Ent[sizEnt * i + 117] - 1,
        Ent[sizEnt * i + 122] - 1,
        Ent[sizEnt * i + 116] - 1,
        Ent[sizEnt * i + 102] - 1, //-- top
        Ent[sizEnt * i + 110] - 1,
        Ent[sizEnt * i + 103] - 1,
        Ent[sizEnt * i + 113] - 1,
        Ent[sizEnt * i + 119] - 1,
        Ent[sizEnt * i + 111] - 1,
        Ent[sizEnt * i + 105] - 1,
        Ent[sizEnt * i + 112] - 1,
        Ent[sizEnt * i + 104] - 1
      };
      cells->InsertNextCell(125, hexq4);

      types[curCell] = VTK_LAGRANGE_HEXAHEDRON;
      refArray->SetTuple1(curCell, EntRef[i]);
      curCell++;
    }
  }

  if (ncells != cells->GetNumberOfCells()) {
    cout << "Major error when reading mesh file. Grid is not complete. " << endl;
    return 0;
  }

  //-- Let's go low VTK level
  vtkIdTypeArray* cellLocations = vtkIdTypeArray::New();
  cellLocations->Allocate(ncells);
  vtkUnsignedCharArray* cellTypes = vtkUnsignedCharArray::New();
  cellTypes->Allocate(ncells);

  vtkIdType  npts = 0;
  vtkIdType* pts  = nullptr;
  cells->InitTraversal();
  for (vtkIdType i = 0; i < ncells; i++) {
    cellTypes->InsertNextValue(static_cast<unsigned char>(types[i]));
    cellLocations->InsertNextValue(cells->GetTraversalLocation(npts));
  }

  // All the cells at the same time - otherwise we lose part
  vtkGrid->SetCells(cellTypes, cellLocations, cells, nullptr, nullptr);

  // add the reference
  vtkDataSetAttributes* reffield;
  reffield = vtkGrid->GetCellData();

  //-- see https://vtk.org/Wiki/VTK/Tutorials/DataStorage#CellData

  reffield->AddArray(refArray);
  refArray->Delete();
  refArray = nullptr;

  // Try to add solutions
  if (ierrsol == 0) {
    vtkDataSetAttributes* field;
    field = vtkGrid->GetPointData();

    ithSol = 1;
    nbrSol = viz_GetSolutionFieldMax(iObj);

    printf("  Number of solution fields = %d \n", nbrSol);

    if ((sol = viz_GetSolutionsArray(iObj, VizVerSol, ithSol, &nbrtmp, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status))) {
      printf("  The solution is defined at vertices \n");

      vtkDataArray** const dataArraylst = new vtkDataArray*[nbrSol];
      for (int iSol = 0; iSol < nbrSol; iSol++) {
        dataArraylst[iSol] = vtkDoubleArray::New();
        if ((sol = viz_GetSolutionsArray(iObj, VizVerSol, iSol + 1, &nbrtmp, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status))) {
          char valField[256], nameField[256];

          int siz;
          sprintf(nameField, "%s", " ");

          switch (SolAtEntTyp) {
            case GmfSca:
              siz = 1;
              strcat(nameField, "Scalar");
              break;

            case GmfVec:
              siz = (dim == 2) ? 2 : 3;
              strcat(nameField, "Vector");
              break;

            case GmfSymMat:
              siz = (dim == 2) ? 3 : 6;
              strcat(nameField, "Metric");
              break;

            default:
              siz = 1;
              break;
          }

          sprintf(valField, "%d", iSol + 1);
          strcat(nameField, valField);

          int noOfDatas = nbrVer * siz;

          // for (int i = 0; i < noOfDatas; i++)
          // {
          //   dataArraylst[iSol]->SetTuple1(i, 1.0 * i); //test value...
          // }

          dataArraylst[iSol]->SetName(nameField);
          dataArraylst[iSol]->SetNumberOfComponents(siz); // 1 for scalar
          dataArraylst[iSol]->SetNumberOfTuples(noOfDatas);

          for (int i = 1; i <= nbrVer; i++) {
            switch (SolAtEntTyp) {
              case GmfSca:
                dataArraylst[iSol]->SetTuple1(i - 1, sol[i]);
                break;

              case GmfVec:
                if (siz == 2)
                  dataArraylst[iSol]->SetTuple2(i - 1, sol[i * siz + 0], sol[i * siz + 1]);
                else if (siz == 3)
                  dataArraylst[iSol]->SetTuple3(i - 1, sol[i * siz + 0], sol[i * siz + 1], sol[i * siz + 2]);
                break;

              case GmfSymMat:
                dataArraylst[iSol]->SetTuple6(i - 1, sol[i * siz + 0], sol[i * siz + 1], sol[i * siz + 2], sol[i * siz + 3], sol[i * siz + 4], sol[i * siz + 5]);
                break;

              default:
                dataArraylst[iSol]->SetTuple1(i - 1, sol[i]);
                break;
            }
          }
        }
      }
      for (int iSol = 0; iSol < nbrSol; iSol++) {
        field->AddArray(dataArraylst[iSol]);
        dataArraylst[iSol]->Delete();
        dataArraylst[iSol] = nullptr;
      }
    }
    else {
      printf("It is not a solution at vertices \n");

      printf("NOT IMPLEMENTED YET \n");

      /*

      int noOfDatas = nbrVer;

      // for (int i = 0; i < noOfDatas; i++)
      // {
      //   dataArray->SetTuple1(i, 1.0 * i); //test value...
      // }

      // printf("noOfDatas = %d \n", noOfDatas);

      dataArray->SetName("Solution Field");
      dataArray->SetNumberOfComponents(1); // 1 for scalar
      dataArray->SetNumberOfTuples(noOfDatas);
      // We write the data solution at each point.
      // // Edges
      // if (iObj->NbrEdg > 0)
      // {
      //   if ((sol = viz_GetSolutionsArray(iObj, VizEdgSol, ithSol, &nbrSol, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status)))
      //   {
      //     printf("Edges : deg = %d \n", deg);
      //     printf("Edges : nbrNodSol = %d \n", nbrNodSol);
      //     printf("Edges : nbrSol = %d \n", nbrSol);

      //     for (int i = 1; i <= iObj->NbrEdg; i++)
      //     {
      //       for (e = 0; e < 2; e++)
      //       {
      //         dataArray->SetTuple1(iObj->Edg[i][e] - 1, sol[i * nbrNodSol + e]);
      //       }
      //     }
      //   }
      //   else
      //   {
      //     printf("No solution at edges. \n");
      //   }
      // }

      // Triangles
      if (iObj->NbrTri > 0)
      {
          if ((sol = viz_GetSolutionsArray(iObj, VizTriSol, ithSol, &nbrSol, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status)))
          {
              printf("Triangles : degree of the solution     = %d \n", deg);
              printf("Triangles : number of nodes by element = %d \n", nbrNodSol);
              printf("Triangles : number of solutions        = %d \n", nbrSol);

              if (deg == 0)
                  printf("Be careful, it is written in a finite element format (whereas it is a constant solution)\n");

              for (int i = 1; i <= iObj->NbrTri; i++)
              {
                  for (e = 0; e < 3; e++)
                  {
                      switch (deg)
                      {
                      case 0:
                          dataArray->SetTuple1(iObj->Tri[i][e] - 1, sol[i * nbrNodSol]);
                          break;

                      default:
                          dataArray->SetTuple1(iObj->Tri[i][e] - 1, sol[i * nbrNodSol + e]);
                          break;
                      }
                  }
              }
          }
          else
          {
              printf("No solution at triangles. \n");
          }
      }

      // Quads
      if (iObj->NbrQua > 0)
      {
          if ((sol = viz_GetSolutionsArray(iObj, VizQuaSol, ithSol, &nbrSol, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status)))
          {
              printf("Quadrilaterials : degree of the solution     = %d \n", deg);
              printf("Quadrilaterials : number of nodes by element = %d \n", nbrNodSol);
              printf("Quadrilaterials : number of solutions        = %d \n", nbrSol);

              if (deg == 0)
                  printf("Be careful, it is written in a finite element format (whereas it is a constant solution)\n");

              for (int i = 1; i <= iObj->NbrQua; i++)
              {
                  for (e = 0; e < 4; e++)
                  {
                      switch (deg)
                      {
                      case 0:
                          dataArray->SetTuple1(iObj->Qua[i][e] - 1, sol[i * nbrNodSol]);
                          break;

                      default:
                          dataArray->SetTuple1(iObj->Qua[i][e] - 1, sol[i * nbrNodSol + e]);
                          break;
                      }
                  }
              }
          }
          else
          {
              printf("No solution at quadrilaterals. \n");
          }
      }

      // Tets
      if (iObj->NbrTet > 0)
      {
          if ((sol = viz_GetSolutionsArray(iObj, VizTetSol, ithSol, &nbrSol, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status)))
          {
              printf("Tetrahedra : degree of the solution     = %d \n", deg);
              printf("Tetrahedra : number of nodes by element = %d \n", nbrNodSol);
              printf("Tetrahedra : number of solutions        = %d \n", nbrSol);

              if (deg == 0)
                  printf("Be careful, it is written in a finite element format (whereas it is a constant solution)\n");

              for (int i = 1; i <= iObj->NbrTet; i++)
              {
                  for (e = 0; e < 4; e++)
                  {
                      switch (deg)
                      {
                      case 0:
                          dataArray->SetTuple1(iObj->Tet[i][e] - 1, sol[i * nbrNodSol]);
                          break;

                      default:
                          dataArray->SetTuple1(iObj->Tet[i][e] - 1, sol[i * nbrNodSol + e]);
                          break;
                      }
                  }
              }
          }
          else
          {
              printf("No solution at tetrahedra. \n");
          }
      }

      // Pyr
      if (iObj->NbrPyr > 0)
      {
          if ((sol = viz_GetSolutionsArray(iObj, VizPyrSol, ithSol, &nbrSol, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status)))
          {
              printf("Pyramids : degree of the solution     = %d \n", deg);
              printf("Pyramids : number of nodes by element = %d \n", nbrNodSol);
              printf("Pyramids : number of solutions        = %d \n", nbrSol);

              if (deg == 0)
                  printf("Be careful, it is written in a finite element format (whereas it is a constant solution)\n");

              for (int i = 1; i <= iObj->NbrPyr; i++)
              {
                  for (e = 0; e < 5; e++)
                  {
                      switch (deg)
                      {
                      case 0:
                          dataArray->SetTuple1(iObj->Pyr[i][e] - 1, sol[i * nbrNodSol]);
                          break;

                      default:
                          dataArray->SetTuple1(iObj->Pyr[i][e] - 1, sol[i * nbrNodSol + e]);
                          break;
                      }
                  }
              }
          }
          else
          {
              printf("No solution at pyramids. \n");
          }
      }

      // Pri
      if (iObj->NbrPri > 0)
      {
          if ((sol = viz_GetSolutionsArray(iObj, VizPriSol, ithSol, &nbrSol, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status)))
          {
              printf("Prims : degree of the solution     = %d \n", deg);
              printf("Prims : number of nodes by element = %d \n", nbrNodSol);
              printf("Prims : number of solutions        = %d \n", nbrSol);

              if (deg == 0)
                  printf("Be careful, it is written in a finite element format (whereas it is a constant solution)\n");

              for (int i = 1; i <= iObj->NbrPri; i++)
              {
                  for (e = 0; e < 6; e++)
                  {
                      switch (deg)
                      {
                      case 0:
                          dataArray->SetTuple1(iObj->Pri[i][e] - 1, sol[i * nbrNodSol]);
                          break;

                      default:
                          dataArray->SetTuple1(iObj->Pri[i][e] - 1, sol[i * nbrNodSol + e]);
                          break;
                      }
                  }
              }
          }
          else
          {
              printf("No solution at prisms. \n");
          }
      }

      // Hex
      if (nbrHex > 0)
      {
          if ((sol = viz_GetSolutionsArray(iObj, VizHexSol, ithSol, &nbrSol, &SolAtEntTyp, &iter, &tim, &deg, &nbrNodSol, &status)))
          {
              printf("Hexahedra : degree of the solution     = %d \n", deg);
              printf("Hexahedra : number of nodes by element = %d \n", nbrNodSol);
              printf("Hexahedra : number of solutions        = %d \n", nbrSol);

              if (deg == 0)
                  printf("Be careful, it is written in a finite element format (whereas it is a constant solution)\n");

              for (int i = 1; i <= nbrHex; i++)
              {
                  for (e = 0; e < 8; e++)
                  {
                      switch (deg)
                      {
                      case 0:
                          dataArray->SetTuple1(iObj->Hex[i][e] - 1, sol[i * nbrNodSol]);
                          break;

                      default:
                          dataArray->SetTuple1(iObj->Hex[i][e] - 1, sol[i * nbrNodSol + e]);
                          break;
                      }
                  }
              }
          }
          else
          {
              printf("No solution at hexahedra. \n");
          }
      }
  */
    }
  }

  printf("  Now write with VTK libraries \n");
  string filename = OutMshNam;
  // Write file
  double                                        t1     = GetWallClock();
  vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
  writer->SetFileName(filename.c_str());
  writer->SetInputData(vtkGrid);
  if (ascii)
    writer->SetDataModeToAscii();
  else
    writer->SetDataModeToBinary();
  writer->Write();
  double t2 = GetWallClock();
  printf(" cpu time write with VTK libraries %lg (s) \n", t2 - t1);
  printf("\n  Write Output file: %s \n", OutMshNam);

  if (sol) {
    free(sol);
    sol = NULL;
  }

  return VIZINT_SUCCESS;
}

int viz_ReadVTK(VizObject* iObj, const char* OutMshNam, bool* solexist)
{
  vtkSmartPointer<vtkUnstructuredGrid> vtkGrid;

  string filename = OutMshNam;

  if (strstr(OutMshNam, ".vtk") != NULL) {
    printf("  Reading a .vtk file \n");
    vtkNew<vtkUnstructuredGridReader> reader;
    reader->SetFileName(filename.c_str());
    reader->Update();
    vtkGrid = reader->GetOutput();
  }
  else if (strstr(OutMshNam, ".vtu") != NULL) {
    printf("  Reading a .vtu file \n");
    vtkNew<vtkXMLUnstructuredGridReader> reader;
    reader->SetFileName(filename.c_str());
    reader->Update();
    vtkGrid = reader->GetOutput();
  }
  else {
    printf("  Unkonwn format, should be .vtu or .vtk \n");
    return VIZINT_SUCCESS;
  }

  //-- vertices
  if (vtkGrid->GetNumberOfPoints() <= 0) {
    printf("  No point found in the file \n");
    return VIZINT_SUCCESS;
  }

  iObj->NbrVer = vtkGrid->GetNumberOfPoints();

  vtkPoints* vtk_points = vtkPoints::New();

  vtk_points = vtkGrid->GetPoints();

  double bds[6];
  vtkGrid->GetBounds(bds);

  if (abs(bds[5] - bds[4]) < 1.e-14)
    iObj->Dim = 2;
  else
    iObj->Dim = 3;

  iObj->Crd    = (double3*)viz_reallocate(sizeof(double3) * (iObj->NbrVer + 1), iObj->Crd);
  iObj->CrdRef = (int*)viz_reallocate(sizeof(int) * (iObj->NbrVer + 1), iObj->CrdRef);

  for (int i = 1; i <= iObj->NbrVer; i++) {
    double x[3];
    vtk_points->GetPoint(i - 1, x);

    iObj->Crd[i][0] = x[0];
    iObj->Crd[i][1] = x[1];
    if (iObj->Dim == 3)
      iObj->Crd[i][2] = x[2];
    iObj->CrdRef[i] = 0;
  }

  //-- cells
  vtkUnsignedCharArray* cellTypes = vtkUnsignedCharArray::New();
  cellTypes                       = vtkGrid->GetCellTypesArray();

  vtkCellArray* cells = vtkCellArray::New();
  cells               = vtkGrid->GetCells();

  vtkIdType        numPts;
  const vtkIdType* pts;

  int nbrP1Edg = 0, nbrP1Tri = 0, nbrQ1Qua = 0, nbrP1Tet = 0, nbrP1Pyr = 0;
  int nbrP1Pri = 0, nbrQ1Hex = 0, nbrP2Edg = 0, nbrP2Tri = 0, nbrQ2Qua = 0;
  int nbrP2Tet = 0, nbrP2Pyr = 0, nbrP2Pri = 0, nbrQ2Hex = 0, nbrUnknown = 0;

  //-- count all entities
  for (int i = 0; i < vtkGrid->GetNumberOfCells(); i++) {
    if (vtkGrid->GetCellType(i) == VTK_LINE)
      nbrP1Edg++;
    else if (vtkGrid->GetCellType(i) == VTK_TRIANGLE)
      nbrP1Tri++;
    else if (vtkGrid->GetCellType(i) == VTK_QUAD)
      nbrQ1Qua++;
    else if (vtkGrid->GetCellType(i) == VTK_TETRA)
      nbrP1Tet++;
    else if (vtkGrid->GetCellType(i) == VTK_PYRAMID)
      nbrP1Pyr++;
    else if (vtkGrid->GetCellType(i) == VTK_WEDGE)
      nbrP1Pri++;
    else if (vtkGrid->GetCellType(i) == VTK_HEXAHEDRON)
      nbrQ1Hex++;
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_EDGE)
      nbrP2Edg++;
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_TRIANGLE)
      nbrP2Tri++;
    else if (vtkGrid->GetCellType(i) == VTK_BIQUADRATIC_QUAD)
      nbrQ2Qua++;
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_TETRA)
      nbrP2Tet++;
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_PYRAMID)
      nbrP2Pyr++;
    else if (vtkGrid->GetCellType(i) == VTK_BIQUADRATIC_QUADRATIC_WEDGE)
      nbrP2Pri++;
    else if (vtkGrid->GetCellType(i) == VTK_TRIQUADRATIC_HEXAHEDRON)
      nbrQ2Hex++;
    else
      nbrUnknown++;
  }

  if (nbrUnknown > 0)
    printf("%d cells with unknown type\n", nbrUnknown);

  iObj->Edg    = (int2*)viz_reallocate(sizeof(int2) * (nbrP1Edg + 1), iObj->Edg);
  iObj->EdgRef = (int*)viz_reallocate(sizeof(int) * (nbrP1Edg + 1), iObj->EdgRef);

  iObj->Tri    = (int3*)viz_reallocate(sizeof(int3) * (nbrP1Tri + 1), iObj->Tri);
  iObj->TriRef = (int*)viz_reallocate(sizeof(int) * (nbrP1Tri + 1), iObj->TriRef);

  iObj->Qua    = (int4*)viz_reallocate(sizeof(int4) * (nbrQ1Qua + 1), iObj->Qua);
  iObj->QuaRef = (int*)viz_reallocate(sizeof(int) * (nbrQ1Qua + 1), iObj->QuaRef);

  iObj->Tet    = (int4*)viz_reallocate(sizeof(int4) * (nbrP1Tet + 1), iObj->Tet);
  iObj->TetRef = (int*)viz_reallocate(sizeof(int) * (nbrP1Tet + 1), iObj->TetRef);

  iObj->Pyr    = (int5*)viz_reallocate(sizeof(int5) * (nbrP1Pyr + 1), iObj->Pyr);
  iObj->PyrRef = (int*)viz_reallocate(sizeof(int) * (nbrP1Pyr + 1), iObj->PyrRef);

  iObj->Pri    = (int6*)viz_reallocate(sizeof(int6) * (nbrP1Pri + 1), iObj->Pri);
  iObj->PriRef = (int*)viz_reallocate(sizeof(int) * (nbrP1Pri + 1), iObj->PriRef);

  iObj->Hex    = (int8*)viz_reallocate(sizeof(int8) * (nbrQ1Hex + 1), iObj->Hex);
  iObj->HexRef = (int*)viz_reallocate(sizeof(int) * (nbrQ1Hex + 1), iObj->HexRef);

  iObj->P2Edg    = (int3*)viz_reallocate(sizeof(int3) * (nbrP2Edg + 1), iObj->P2Edg);
  iObj->P2EdgRef = (int*)viz_reallocate(sizeof(int) * (nbrP2Edg + 1), iObj->P2EdgRef);

  iObj->P2Tri    = (int6*)viz_reallocate(sizeof(int6) * (nbrP2Tri + 1), iObj->P2Tri);
  iObj->P2TriRef = (int*)viz_reallocate(sizeof(int) * (nbrP2Tri + 1), iObj->P2TriRef);

  iObj->Q2Qua    = (int9*)viz_reallocate(sizeof(int9) * (nbrQ2Qua + 1), iObj->Q2Qua);
  iObj->Q2QuaRef = (int*)viz_reallocate(sizeof(int) * (nbrQ2Qua + 1), iObj->Q2QuaRef);

  iObj->P2Tet    = (int10*)viz_reallocate(sizeof(int10) * (nbrP2Tet + 1), iObj->P2Tet);
  iObj->P2TetRef = (int*)viz_reallocate(sizeof(int) * (nbrP2Tet + 1), iObj->P2TetRef);

  iObj->P2Pyr    = (int14*)viz_reallocate(sizeof(int14) * (nbrP2Pyr + 1), iObj->P2Pyr);
  iObj->P2PyrRef = (int*)viz_reallocate(sizeof(int) * (nbrP2Pyr + 1), iObj->P2PyrRef);

  iObj->P2Pri    = (int18*)viz_reallocate(sizeof(int18) * (nbrP2Pri + 1), iObj->P2Pri);
  iObj->P2PriRef = (int*)viz_reallocate(sizeof(int) * (nbrP2Pri + 1), iObj->P2PriRef);

  iObj->Q2Hex    = (int27*)viz_reallocate(sizeof(int27) * (nbrQ2Hex + 1), iObj->Q2Hex);
  iObj->Q2HexRef = (int*)viz_reallocate(sizeof(int) * (nbrQ2Hex + 1), iObj->Q2HexRef);

  cells->InitTraversal();
  for (int i = 0; i < cells->GetNumberOfCells(); i++) {
    cells->GetCellAtId(i, numPts, pts);

    if (vtkGrid->GetCellType(i) == VTK_LINE) {
      iObj->NbrEdg++;
      assert(numPts == 2);
      for (int j = 0; j < numPts; j++)
        iObj->Edg[iObj->NbrEdg][j] = pts[j] + 1;
      iObj->EdgRef[iObj->NbrEdg] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_TRIANGLE) {
      iObj->NbrTri++;
      assert(numPts == 3);
      for (int j = 0; j < numPts; j++)
        iObj->Tri[iObj->NbrTri][j] = pts[j] + 1;
      iObj->TriRef[iObj->NbrTri] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_QUAD) {
      iObj->NbrQua++;
      assert(numPts == 4);
      for (int j = 0; j < numPts; j++)
        iObj->Qua[iObj->NbrQua][j] = pts[j] + 1;
      iObj->QuaRef[iObj->NbrQua] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_TETRA) {
      iObj->NbrTet++;
      assert(numPts == 4);
      for (int j = 0; j < numPts; j++)
        iObj->Tet[iObj->NbrTet][j] = pts[j] + 1;
      iObj->TetRef[iObj->NbrTet] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_PYRAMID) {
      iObj->NbrPyr++;
      assert(numPts == 5);
      for (int j = 0; j < numPts; j++)
        iObj->Pyr[iObj->NbrPyr][j] = pts[j] + 1;
      iObj->PyrRef[iObj->NbrPyr] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_WEDGE) {
      iObj->NbrPri++;
      assert(numPts == 6);
      for (int j = 0; j < numPts; j++)
        iObj->Pri[iObj->NbrPri][j] = pts[j] + 1;
      iObj->PriRef[iObj->NbrPri] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_HEXAHEDRON) {
      iObj->NbrHex++;
      assert(numPts == 8);
      for (int j = 0; j < numPts; j++)
        iObj->Hex[iObj->NbrHex][j] = pts[j] + 1;
      iObj->HexRef[iObj->NbrHex] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_EDGE) {
      iObj->NbrP2Edg++;
      assert(numPts == 3);
      for (int j = 0; j < numPts; j++)
        iObj->P2Edg[iObj->NbrP2Edg][j] = pts[j] + 1;
      iObj->P2EdgRef[iObj->NbrP2Edg] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_TRIANGLE) {
      iObj->NbrP2Tri++;
      assert(numPts == 6);
      for (int j = 0; j < numPts; j++)
        iObj->P2Tri[iObj->NbrP2Tri][j] = pts[j] + 1;
      iObj->P2TriRef[iObj->NbrP2Tri] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_BIQUADRATIC_QUAD) {
      iObj->NbrQ2Qua++;
      assert(numPts == 9);
      for (int j = 0; j < numPts; j++)
        iObj->Q2Qua[iObj->NbrQ2Qua][j] = pts[j] + 1;
      iObj->Q2QuaRef[iObj->NbrQ2Qua] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_TETRA) {
      iObj->NbrP2Tet++;
      assert(numPts == 10);
      for (int j = 0; j < numPts; j++)
        iObj->P2Tet[iObj->NbrP2Tet][j] = pts[j] + 1;
      iObj->P2TetRef[iObj->NbrP2Tet] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_PYRAMID) {
      iObj->NbrP2Pyr++;
      assert(numPts == 14);
      for (int j = 0; j < numPts; j++)
        iObj->P2Pyr[iObj->NbrP2Pyr][j] = pts[j] + 1;
      iObj->P2PyrRef[iObj->NbrP2Pyr] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_BIQUADRATIC_QUADRATIC_WEDGE) {
      iObj->NbrP2Pri++;
      assert(numPts == 18);
      for (int j = 0; j < numPts; j++)
        iObj->P2Pri[iObj->NbrP2Pri][j] = pts[j] + 1;
      iObj->P2PriRef[iObj->NbrP2Pri] = 1;
    }
    else if (vtkGrid->GetCellType(i) == VTK_TRIQUADRATIC_HEXAHEDRON) {
      iObj->NbrQ2Hex++;
      assert(numPts == 27);
      for (int j = 0; j < numPts; j++)
        iObj->Q2Hex[iObj->NbrQ2Hex][j] = pts[j] + 1;
      iObj->Q2HexRef[iObj->NbrQ2Hex] = 1;
    }
  }

  nbrP1Edg = 0, nbrP1Tri = 0, nbrQ1Qua = 0, nbrP1Tet = 0, nbrP1Pyr = 0;
  nbrP1Pri = 0, nbrQ1Hex = 0, nbrP2Edg = 0, nbrP2Tri = 0, nbrQ2Qua = 0;
  nbrP2Tet = 0, nbrP2Pyr = 0, nbrP2Pri = 0, nbrQ2Hex = 0, nbrUnknown = 0;

  //-- reference
  cells->InitTraversal();
  vtkCellData* cellData = vtkGrid->GetCellData();
  for (int j = 0; j < cellData->GetNumberOfArrays(); j++) {
    vtkDataArray* data = cellData->GetArray(j);
    cout << "  Found an array: " << data->GetName() << endl;

    if (strstr(data->GetName(), "Reference") != NULL || strstr(data->GetName(), "reference") != NULL || strstr(data->GetName(), "REFERENCE") != NULL) {
      cout << "  Found a reference array: " << data->GetName() << endl;
      for (int i = 0; i < data->GetNumberOfTuples(); i++) {
        int value = data->GetTuple1(i);

        if (vtkGrid->GetCellType(i) == VTK_LINE) {
          nbrP1Edg++;
          iObj->EdgRef[nbrP1Edg] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_TRIANGLE) {
          nbrP1Tri++;
          iObj->TriRef[nbrP1Tri] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_QUAD) {
          nbrQ1Qua++;
          iObj->QuaRef[nbrQ1Qua] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_TETRA) {
          nbrP1Tet++;
          iObj->TetRef[nbrP1Tet] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_PYRAMID) {
          nbrP1Pyr++;
          iObj->PyrRef[nbrP1Pyr] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_WEDGE) {
          nbrP1Pri++;
          iObj->PriRef[nbrP1Pri] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_HEXAHEDRON) {
          nbrQ1Hex++;
          iObj->HexRef[nbrQ1Hex] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_EDGE) {
          nbrP2Edg++;
          iObj->P2EdgRef[nbrP2Edg] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_TRIANGLE) {
          nbrP2Tri++;
          iObj->P2TriRef[nbrP2Tri] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_BIQUADRATIC_QUAD) {
          nbrQ2Qua++;
          iObj->Q2QuaRef[nbrQ2Qua] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_TETRA) {
          nbrP2Tet++;
          iObj->P2TetRef[nbrP2Tet] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_QUADRATIC_PYRAMID) {
          nbrP2Pyr++;
          iObj->P2PyrRef[nbrP2Pyr] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_BIQUADRATIC_QUADRATIC_WEDGE) {
          nbrP2Pri++;
          iObj->P2PriRef[nbrP2Pri] = value;
        }
        else if (vtkGrid->GetCellType(i) == VTK_TRIQUADRATIC_HEXAHEDRON) {
          nbrP2Pri++;
          iObj->Q2HexRef[nbrQ2Hex] = value;
        }
      }
    }
  }

  *solexist = false;

  //-- Solutions at vertices
  vtkDataSetAttributes* field;
  field = vtkGrid->GetPointData();

  int nbrSol = field->GetNumberOfArrays();

  if (nbrSol > 0)
    *solexist = true;

  vtkDataArray** const dataArraylst = new vtkDataArray*[nbrSol];

  char(*FldVerNam)[1024];

  FldVerNam = (char(*)[1024])malloc(nbrSol * 1024 * sizeof(char));

  solution** Sol = (solution**)malloc(sizeof(solution*) * nbrSol);
  for (int iSol = 0; iSol < nbrSol; iSol++) {
    Sol[iSol] = NULL;
  }

  for (int iSol = 0; iSol < nbrSol; iSol++) {
    dataArraylst[iSol] = field->GetArray(iSol);

    int siz, noOfDatas, SolTyp;
    sprintf(FldVerNam[iSol], "%s", dataArraylst[iSol]->GetName());
    siz       = dataArraylst[iSol]->GetNumberOfComponents();
    noOfDatas = dataArraylst[iSol]->GetNumberOfTuples();

    switch (siz) {
      case 1:
        SolTyp = GmfSca;
        break;
      case 2:
        SolTyp = GmfVec;
        assert(iObj->Dim == 2);
        break;
      case 3:
        if (iObj->Dim == 2)
          SolTyp = GmfSymMat;
        else if (iObj->Dim == 3)
          SolTyp = GmfVec;
        break;
      case 4:
        SolTyp = GmfMat;
        assert(iObj->Dim == 2);
        break;
      case 6:
        SolTyp = GmfSymMat;
        assert(iObj->Dim == 3);
        break;
      case 9:
        SolTyp = GmfMat;
        assert(iObj->Dim == 3);
        break;
      default:
        printf("Warning: the following solution size is unkown   %d \n", siz);
        break;
    }

    //-- for each field, alloc one solution
    Sol[iSol]         = viz_addsol(iObj->CrdSol);
    Sol[iSol]->Dim    = iObj->Dim;
    Sol[iSol]->NbrSol = noOfDatas;
    Sol[iSol]->SolTyp = SolTyp;
    sprintf(Sol[iSol]->Nam, "%s", dataArraylst[iSol]->GetName());
    Sol[iSol]->Sol      = (double*)malloc(1 * siz * (noOfDatas + 1) * sizeof(double));
    Sol[iSol]->Ite      = 0;
    Sol[iSol]->Tim      = 0;
    Sol[iSol]->Deg      = 0;
    Sol[iSol]->NbrNod   = 1;
    Sol[iSol]->vizAlloc = 1;
    iObj->CrdSol        = Sol[iSol];

    for (int iEnt = 0; iEnt < noOfDatas; iEnt++) {
      double* solBuf = (double*)malloc(sizeof(double) * siz);

      switch (siz) {
        case 1:
          solBuf[0] = dataArraylst[iSol]->GetTuple1(iEnt);
          break;
        case 2:
          solBuf = dataArraylst[iSol]->GetTuple2(iEnt);
          break;
        case 3:
          solBuf = dataArraylst[iSol]->GetTuple3(iEnt);
          break;
        case 4:
          solBuf = dataArraylst[iSol]->GetTuple4(iEnt);
          break;
        case 6:
          solBuf = dataArraylst[iSol]->GetTuple6(iEnt);
          break;
        case 9:
          solBuf = dataArraylst[iSol]->GetTuple9(iEnt);
          break;
        default:
          printf("Warning: the following solution size is unkown   %d \n", siz);
          break;
      }

      for (int j = 0; j < siz; j++)
        Sol[iSol]->Sol[(iEnt + 1) * siz + j] = solBuf[j];
    }
  }
  iObj->CrdSol = Sol[0];

  //-- For solution at cells
  // vtkCellData *cellData = vtkGrid->GetCellData();
  // for (int i = 0; i < cellData->GetNumberOfArrays(); i++)
  // {
  //  vtkDataArray *data = cellData->GetArray(i);
  //  cout << "name " << data->GetName() << endl;
  //  for (int j = 0; j < data->GetNumberOfTuples(); j++)
  //  {
  //    double value = data->GetTuple1(j);
  //    cout << "  value " << j << "th is " << value << endl;
  //  }
  //}

  return VIZINT_SUCCESS;
}

/*
    //   typedef enum {
    //   // Linear cells
    //   VTK_EMPTY_CELL       = 0,
    //   VTK_VERTEX           = 1,
    //   VTK_POLY_VERTEX      = 2,
    //   VTK_LINE             = 3,
    //   VTK_POLY_LINE        = 4,
    //   VTK_TRIANGLE         = 5,
    //   VTK_TRIANGLE_STRIP   = 6,
    //   VTK_POLYGON          = 7,
    //   VTK_PIXEL            = 8,
    //   VTK_QUAD             = 9,
    //   VTK_TETRA            = 10,
    //   VTK_VOXEL            = 11,
    //   VTK_HEXAHEDRON       = 12,
    //   VTK_WEDGE            = 13,
    //   VTK_PYRAMID          = 14,
    //   VTK_PENTAGONAL_PRISM = 15,
    //   VTK_HEXAGONAL_PRISM  = 16,

    //   // Quadratic, isoparametric cells
    //   VTK_QUADRATIC_EDGE                   = 21,
    //   VTK_QUADRATIC_TRIANGLE               = 22,
    //   VTK_QUADRATIC_QUAD                   = 23,
    //   VTK_QUADRATIC_POLYGON                = 36,
    //   VTK_QUADRATIC_TETRA                  = 24,
    //   VTK_QUADRATIC_HEXAHEDRON             = 25,
    //   VTK_QUADRATIC_WEDGE                  = 26,
    //   VTK_QUADRATIC_PYRAMID                = 27,
    //   VTK_BIQUADRATIC_QUAD                 = 28,
    //   VTK_TRIQUADRATIC_HEXAHEDRON          = 29,
    //   VTK_QUADRATIC_LINEAR_QUAD            = 30,
    //   VTK_QUADRATIC_LINEAR_WEDGE           = 31,
    //   VTK_BIQUADRATIC_QUADRATIC_WEDGE      = 32,
    //   VTK_BIQUADRATIC_QUADRATIC_HEXAHEDRON = 33,
    //   VTK_BIQUADRATIC_TRIANGLE             = 34,

    //   // Cubic, isoparametric cell
    //   VTK_CUBIC_LINE                       = 35,

    //   // Special class of cells formed by convex group of points
    //   VTK_CONVEX_POINT_SET = 41,

    //   // Polyhedron cell (consisting of polygonal faces)
    //   VTK_POLYHEDRON = 42,

    //   // Higher order cells in parametric form
    //   VTK_PARAMETRIC_CURVE        = 51,
    //   VTK_PARAMETRIC_SURFACE      = 52,
    //   VTK_PARAMETRIC_TRI_SURFACE  = 53,
    //   VTK_PARAMETRIC_QUAD_SURFACE = 54,
    //   VTK_PARAMETRIC_TETRA_REGION = 55,
    //   VTK_PARAMETRIC_HEX_REGION   = 56,

    //   // Higher order cells
    //   VTK_HIGHER_ORDER_EDGE        = 60,
    //   VTK_HIGHER_ORDER_TRIANGLE    = 61,
    //   VTK_HIGHER_ORDER_QUAD        = 62,
    //   VTK_HIGHER_ORDER_POLYGON     = 63,
    //   VTK_HIGHER_ORDER_TETRAHEDRON = 64,
    //   VTK_HIGHER_ORDER_WEDGE       = 65,
    //   VTK_HIGHER_ORDER_PYRAMID     = 66,
    //   VTK_HIGHER_ORDER_HEXAHEDRON  = 67,

    //   // Arbitrary order Lagrange elements (formulated separated from generic higher order cells)
    //   VTK_LAGRANGE_CURVE           = 68,
    //   VTK_LAGRANGE_TRIANGLE        = 69,
    //   VTK_LAGRANGE_QUADRILATERAL   = 70,
    //   VTK_LAGRANGE_TETRAHEDRON     = 71,
    //   VTK_LAGRANGE_HEXAHEDRON      = 72,
    //   VTK_LAGRANGE_WEDGE           = 73,
    //   VTK_LAGRANGE_PYRAMID         = 74,

    //   VTK_NUMBER_OF_CELL_TYPES
    // } VTKCellType;
    */