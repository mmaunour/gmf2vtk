GMF2VTK is a tool to do conversions between Gamma Mesh Format (see [libMeshb](https://github.com/LoicMarechal/libMeshb)) files and VTK files for [ParaView](https://www.paraview.org)

[ViZiR 4](https://pyamg.saclay.inria.fr/vizir4.html) can be used to visualise meshes / solutions in the Gamma Mesh Format (libMeshb).

At the moment, all elements of degree 1, 2, 3 and 4 (triangles, quadrilaterals, tetrahedra, pyramids, prisms, hexahedra) can be converted. 
The Tessellate Filter should be used in ParaView for elements that are not simplicial (i.e. not linear, all elements except linear triangles and linear tetrahedra) to have a better approximation of the elements. 
Note that it has a cost (in time and memory).
For solutions, only solutions at vertices are supported for the conversion. Solutions at elements are not supported.

# Build for *Linux* or *macOS*
[libMeshb](https://github.com/LoicMarechal/libMeshb) and VTK librairies are needed to be installed.

Simply follow these steps:
- Clone the repo
- Go to gmf2vtk
- `mkdir build`
- `cd build`
- `cmake ..`
- `make -j`
- `make install`

If CMake cannot find libMeshb library (with find_package), you may need to add the path where it has been set, for instance:
-  `cmake .. -DCMAKE_PREFIX_PATH=~/cmakebuilds/`

# Usage
Once you have done make install, GMF2VTK can be launched from a terminal with gmf2vtk

```sh
  The format is: gmf2vtk -in xxx.mesh[b]/.vtu (-sol xxx.sol[b] -out xxx -ascii)
  where -in    is required to define the input mesh files
        -sol   is optional to define the input solution files
        -out   is optional to define the output mesh/sol files
        -ascii is optional to define ascii output (binary by default otherwise)
        
```

It is mandatory to put the extension (.mesh or .meshb or .vtk or .vtu) for the input mesh file.

If there is no output mesh file given, the input mesh file name will be used with the correct extension.

# Contact 
[Matthieu Maunoury](https://pages.saclay.inria.fr/matthieu.maunoury/) (matthieu.maunoury@inria.fr)
